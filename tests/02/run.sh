if [ ! -d dist ]; then
	mkdir dist
fi

# TODO: Make multiple recursion case files, eg. 1v1, 2v2, 3v1, 1x1
# NvM: N-chain structure against an M-chain structure
# NxM: N-chain structure mixed with an M-chain structure
# 1-chain: T=record<null<T>>
# 2-chain: T=record<null<record<null<T>>>>

# The current test file only tests some of these cases

#gdb nugget -ex "run aulang.v3 -L dist main.au dist/main"
nugget aulang.v3 -L dist main.au dist/main ||
exit 1

export LD_LIBRARY_PATH=$PWD/../../:$LD_LIBRARY_PATH
../../nugget --dir dist main
#gdb ../../nugget -ex "run --dir dist main"
