#/bin/bash

make || exit 1

if [ ! -f example/out -o example/example.au -nt example/out ]; then
    auro aulang example/example.au example/out
    echo compiled example
fi

function format_output {
    LEVEL=0
    while read line; do
        if [[ $line = "ERROR"* ]]; then
            LEVEL=0
        fi

        if [[ $line = "[gc]"* ]]; then
            continue
        fi


        INDENT=""
        for i in `seq $LEVEL`; do
            INDENT="$INDENT  "
        done
        echo "$INDENT$line"

        if [[ $line = "ENTER FRAME"* ]]; then
            LEVEL=$((LEVEL+1))
        elif [[ $line = "EXIT FRAME"* ]]; then
            LEVEL=$((LEVEL-1))
        fi
    done
}

if [[ "$1" == "test" ]]; then
    for dir in `ls tests`; do
        dir=tests/$dir
        if [ ! -f $dir/out -o $dir/main.au -nt $dir/out ]; then
            nugget aulang $dir/main.au $dir/out
        fi
    done

    export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH

    for dir in `ls tests`; do
        ./nugget tests/$dir/out
        #gdb ./nugget -ex "run tests/$dir/out"
    done
elif [[ $# -eq 0 ]]; then
    export LD_LIBRARY_PATH=$PWD:$LD_LIBRARY_PATH

    #nuggetargs="--dir ../aulang/dist aulang example/example.au example/out"
    nuggetargs="--monitor-namespace example/out --profile profile.csv example/out"
    #nuggetargs="--dir ../aulang.dist aulang"

    ./nugget $nuggetargs
    #./nugget --log $nuggetargs > outlog
    #gdb ./nugget -ex "run --log $nuggetargs"
    #valgrind ./nugget $nuggetargs
else
    ./nugget "$@"
fi