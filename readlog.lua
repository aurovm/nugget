
root_frame = {
	name = "ROOT",
	level = 0,
	lines = {},
}

frame_map = {ROOT=root_frame}
frame_seq = {root_frame}
frame = root_frame

counters = {
	code = 0,
	gc = 0,
	other = 0,
}

function level_indent (level)
	local s = ""
	for i = 1, level do
		s = s .. "  "
	end
	return s
end

function process (line, line_i)
	line_frame = string.match(line, "%[F%d+%]")
	if line_frame then
		if line_frame ~= frame.name then
			local last_line = frame.lines[#frame.lines]

			if frame.parent
			and line_frame == frame.parent.name
			or last_line and last_line.ends
			then
				frame = frame.parent
			else
				new_frame = {
					name = line_frame,
					parent = frame,
					level = 1,
					lines = {},
				}
				frame = new_frame

				frame_map[frame.name] = frame
				table.insert(frame_seq, frame)

				if frame.parent then
					frame.level = frame.parent.level + 1

					if last_line then
						table.insert(last_line.child_frames, frame)
						frame.call_line = last_line
					end
				end

				--print(level_indent(frame.level) .. frame.name)
			end
		end

		line = string.sub(line, #line_frame + 2)

		pc_start, pc_end = string.find(line, "%d+:")
		pc = tonumber(string.sub(line, pc_start, pc_end))

		line = string.sub(line, pc_end + 2)
		--print(level_indent(frame.level) .. line)

		line_data = {
			pc=pc,
			line=line,
			index=line_i,
			frame=frame,
			child_frames={},
			gc_effects={},
			other_effects={},
		}

		if string.sub(line, 1, 3) == "end" then
			line_data.ends = true
		end

		arrow_start, arrow_end = string.find(line, " <- ", 1, true)
		if arrow_start then
			left = string.sub(line, 1, arrow_start - 1)
			right = string.sub(line, arrow_end + 1)

			line_data.left = left
			line_data.right = right
		end

		table.insert(frame.lines, line_data)

		counters.code = counters.code + 1
	elseif string.find(line, "%[gc%]") == 1 then
		local last_line = frame.lines[#frame.lines]
		if last_line then
			table.insert(last_line.gc_effects, line)
		else
			print("ERROR(" .. line_i .. ") " .. line)
		end
		counters.gc = counters.gc + 1
	else
		do -- trim spaces
			local space_start, space_end
			space_start, space_end = string.find(line, "%s+")
			if space_start == 1 then
				line = string.sub(line, space_end + 1)
			end
		end

		local last_line = frame.lines[#frame.lines]
		if last_line then
			if last_line.ends then
				last_line = last_line.frame.call_line
			end
			if last_line then
				table.insert(last_line.other_effects, line)
			end
		end
		counters.other = counters.other + 1
	end
end



file = io.open("outlog", "r")
line = file:read("*line")
line_i = 1
while line do
	process(line, line_i)
	line = file:read("*line")
	line_i = line_i + 1
end

-- print("code", counters.code, "gc", counters.gc, "other", counters.other)

function traverse_lines (f)
	function traverse (frame)
		for _, line in ipairs(frame.lines) do
			f(line)
			for _, child in ipairs(line.child_frames) do
				traverse(child)
			end
		end
	end
	traverse(root_frame)
end

function traverse_lines_back (f)
	function traverse (frame)
		for i = #frame.lines, 1, -1 do
			local line = frame.lines[i]
			f(line)
			for _, child in ipairs(line.child_frames) do
				traverse(child)
			end
		end
	end
	traverse(root_frame)
end



local top_group = {
	frame=root_frame,
	children={}
}

local line_group = top_group


function find_group (frame, start_group)
	local group = start_group

	-- take outer groups until the levels match
	while group.frame.level > frame.level do
		group = group.parent
	end

	if frame.level > group.frame.level then
		local parent = find_group(frame.parent, group)
		local siblings = parent.children
		local last_group = siblings[#siblings]
		if last_group and last_group.line == frame.call_line then
			-- Combine groups
			group = last_group
			if not group.frame then
				group.frame = frame
			end
		else
			group = {
				frame = frame,
				children = {},
				parent = parent,
			}
			table.insert(siblings, group)
		end
	elseif group.frame ~= frame then
		local prev_group = group
		group = {
			frame = frame,
			children = {},
			parent = find_group(frame.parent, group.parent),
		}
		table.insert(group.parent.children, prev_group)
		table.insert(group.parent.children, group)
	end

	if not group.line then
		group.line = frame.call_line
	end

	return group
end

function add_line (line, effects)
	if line_group.children[#line_group.children] == line then return end
	line_group = find_group(line.frame, line_group)

	local siblings = line_group.children
	local last_line = siblings[#siblings]

	if not last_line or last_line.line.index < line.index then
		table.insert(line_group.children, {
			line = line,
			parent = line_group,
			children = {},
			effects = effects,
		})
	else
		local first = 1
		local last = #siblings

		-- Binary search line
		while last >= first do
			local mid = (first + last) // 2
			local mid_index = siblings[mid].line.index

			if mid_index == line.index then
				-- Line already there.
				-- TODO: Add effects
				return
			elseif mid_index > line.index then
				last = mid-1
			elseif mid_index < line.index then
				first = mid+1
			end
		end

		-- last < first
		-- the correct line is not in the list, and it goes in between
		-- first and last, that is, it takes the place of first

		assert(last == first-1)

		table.insert(line_group.children, first, {
			line = line,
			parent = line_group,
			children = {},
			effects = effects,
		})
	end
end

function print_all ()
	function print_groups (groups, indent)
		local _print = print

		for i, group in ipairs(groups) do
			local is_last = i == #groups

			local prefix =
				indent .. (is_last and "  " or "│ ")
			local prefix_connect = 
				indent .. (is_last and "└╴" or "├╴")

			if group.line then
				print(prefix_connect .. group.line.line)
			else
				print(prefix_connect .. "???")
			end

			if group.effects then
				for _, effect in ipairs(group.effects) do
					print(prefix .. "\x1B[1m" .. effect .. "\x1B[0m")
				end
			end

			-- Lines
			if group.children then
				-- Frame
				if group.frame then
					print(prefix .. group.frame.name)
				end
				print_groups(group.children, prefix)
			end
		end
	end

	ancestry = {}
	do
		local frame = top_group.frame
		while frame do
			table.insert(ancestry, frame)
			frame = frame.parent
		end
	end

	for i = #ancestry, 1, -1 do
		local frame = ancestry[i]
		local indent = level_indent(frame.level)

		if frame.call_line then
			print(
				level_indent(frame.level-1) ..
				"└╴" ..
				frame.call_line.line
			)
		end
		print(indent .. frame.name)
	end

	print_groups(top_group.children, level_indent(top_group.frame.level))
end


function forward_track (params)
	local gc_queries = params.gc or {}
	local reg_queries = params.reg or {}

	local track_frames = {}
	function track (frame_name, reg)
		-- print("track", frame_name, reg)
		local track_frame = track_frames[frame_name]
		if not track_frame then
			track_frame = {
				name=frame_name,
				regs={}
			}
			track_frames[frame_name] = track_frame
		end
		track_frame.regs[reg] = true
	end

	for _, query in ipairs(reg_queries) do
		local dot = string.find(query, ".", 1, true)
		local frame = string.sub(query, 1, dot-1)
		local reg = string.sub(query, dot+1)
		track(frame, reg)
	end

	traverse_lines(function (line)
		local matches = false
		local effects = {}

		local track_frame = track_frames[line.frame.name]
		if track_frame then
			for track_reg, _ in pairs(track_frame.regs) do
				if string.find(line.line, track_reg, 1, true) then
					matches = true

					if line.right == track_reg then
						track(line.frame.name, line.left)
					elseif line.ends then
						local parent_line = line.frame.call_line
						if parent_line.left then
							track(parent_line.frame.name, parent_line.left)
						end
					end
				end
			end
		end

		for _, effect in ipairs(line.gc_effects) do
			for _, gc_query in pairs(gc_queries) do
				if string.find(effect, gc_query .. " ") then
					table.insert(effects, effect)
					break -- query loop, continues to next effect
				end
			end
		end

		if matches or #effects > 0 then
			add_line(line, effects)
		end
	end)
end

function back_track (params)
	local reg_queries = params.reg

	local track_frames = {}
	function track (frame_name, reg)
		local track_frame = track_frames[frame_name]
		if not track_frame then
			track_frame = {
				name=frame_name,
				regs={}
			}
			track_frames[frame_name] = track_frame
		end
		track_frame.regs[reg] = true
	end

	for _, query in ipairs(reg_queries) do
		local dot = string.find(query, ".", 1, true)
		local frame = string.sub(query, 1, dot-1)
		local reg = string.sub(query, dot+1)
		track(frame, reg)
	end

	traverse_lines_back(function (line)
		local matches = false
		local effects = {}

		local track_frame = track_frames[line.frame.name]
		if track_frame then
			for track_reg, _ in pairs(track_frame.regs) do
				if string.find(line.line, track_reg, 1, true) then
					matches = true

					if line.left == track_reg then
						track(line.frame.name, line.right)
					elseif line == line.frame.lines[1] then
						local parent_line = line.frame.call_line
						if parent_line.right then
							track(parent_line.frame.name, parent_line.right)
						end
					end
				end
			end
		end

		if matches or #effects > 0 then
			add_line(line, effects)
		end
	end)
end

--[==[
forward_track {
	--gc = {"#2128", "#2127"},
	--reg = {"[F3663].r#17", "[F3663].r#9"},
	gc = {},
	reg = {"[F29982].r#64"},
}

]==]

forward_track {
	gc = { "#2418" },
	reg = { "[F4404].r#59" }
}

--back_track {reg = { "[F4894].r#127", "[F4894].r#109", "[F4894].r#49" }}

print_all()
