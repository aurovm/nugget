#include <stdio.h>
#include <string.h>

#include "monitor.h"
#include "collections/array.h"

DEFINE_ARRAY(PrefixList, char*);

int prefixMatch (char *prefix, char *modname) {
    int prefixLen = strlen(prefix);
    if (strlen(modname) < prefixLen) return 0;
    return memcmp(modname, prefix, prefixLen) == 0;
}

DEFINE_ARRAY_SEARCH(PrefixList, prefix, modname, prefixMatch(prefix, modname));

void MonitorInit(Monitor *monitor) {
    monitor->prefixes = PrefixListNew(8);
}


void MonitorSetFlagValue(Monitor *monitor, int flag, int value) {
	if (value) MonitorSetFlag(monitor, flag);
	else MonitorUnsetFlag(monitor, flag);
}

void MonitorSetFlag(Monitor *monitor, int flag) {
    monitor->flags |= flag;
}

void MonitorUnsetFlag(Monitor *monitor, int flag) {
    monitor->flags &= ~flag;
}

int MonitorGetFlag(Monitor *monitor, int flag) {
    return monitor->flags & flag ? 1 : 0;
}

void MonitorAddPrefix(Monitor *monitor, char *prefix) {
    PrefixListPush(monitor->prefixes, prefix);
}

int MonitorMatchesModule(Monitor *monitor, char *modname) {
    if (!modname) return 0;
    int index = PrefixListFindIndex(monitor->prefixes, modname);
    return index >= 0;
}

