#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "headers.h"
#include "gc.h"
#include "value.h"

int nuggetValPtrSize (nuggetType *type, nuggetValPtr valptr) {}

void nuggetValPtrDrop (nuggetType *type, nuggetValPtr valptr) {
    if (type->flags & NUGGET_TYPE_FLAG_GC) {
        int size = sizeof(void*);
        void *value;
        memcpy(&value, valptr, size);
        gc_decref(value);
        //value = 0;
        //memcpy(dst, &value, size);
    } else if (type->drop) {
    	type->drop(0, valptr, type->data);
    }
}

void nuggetValPtrReadData (nuggetType *type, nuggetValPtr valptr, void *dst);

int nuggetValPtrCopy (nuggetType *type, nuggetValPtr dst, nuggetValPtr src) {
    if (type->flags & NUGGET_TYPE_FLAG_GC) {
        int size = sizeof(void*);
        assert(size == type->size);

        void *value;
        memcpy(&value, src, size);
        gc_incref(value);

        if (dst != src) memcpy(dst, &value, size);
    } else {
    	if (type->clone) {
	    	type->clone(0, src, type->data);
	    }
        if (dst != src) memcpy(dst, src, type->size);
    }
    return type->size;
}