#ifndef __NUGGET_FUNCTION__
#define __NUGGET_FUNCTION__

#include <stdio.h>
#include "headers.h"

nuggetFunction *nuggetFunctionAlloc (nuggetMachine *machine, size_t size);

#endif //__NUGGET_FUNCTION__