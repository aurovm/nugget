#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>

#include "headers.h"
#include "gc.h"

#include "value.h"
#include "stackFrame.h"
#include "monitor.h"

//#define DEBUG(...) printf(__VA_ARGS__)
#define DEBUG(...)

// Read from unaligned address
void *readPointer (char *addr) {
    void *val;
    memcpy(&val, addr, sizeof(void*));
    return val;
}
// TODO: Should flip these return codes, to match c convention
// returns 1 for success and 0 for failure
int decref (stackFrame *frame, int addr) {
    if (!stackFrameGetRegFlag(frame, addr)) return 1;

    nuggetType *type = frame->types[addr];

    void* valptr = &frame->data[addr];
    nuggetValPtrDrop(type, valptr);
    return 1;
}

// returns 1 for success and 0 for failure
int incref (stackFrame *frame, int addr) {
    if (!stackFrameGetRegFlag(frame, addr)) {
        nuggetPushErrorF(frame->machine, 39+12, "Uninitialized value, at stack address %d", addr);
        return 0;
    }

    nuggetType *type = frame->types[addr];
    void* valptr = &frame->data[addr];

    if (type->flags & NUGGET_TYPE_FLAG_GC) {
        if (!gc_isValid(readPointer(valptr))) {
            nuggetPushErrorF(frame->machine, 33+12, "Invalid value, at stack address %d", addr);
            return 0;
        }
    }

    // Overwrite itself to increment the ref counts
    nuggetValPtrOverwrite(type, valptr, valptr);

    stackFrameSetRegFlag(frame, addr, 1);

    return 1;
}

void executionLoop (nuggetMachine *machine);

nuggetMachine* nuggetMachineNew (nuggetModuleLoader loader, void *data) {
    nuggetMachine* this = calloc(sizeof(nuggetMachine), 1);
    this->frame = NULL;
    this->err = NULL;
    this->state = machineStateStopped;

    this->moduleMap = HashmapStringNew();
    ArenaInit(&this->functionArena);
    MonitorInit(&this->monitor);

    this->loader = loader;
    this->data = data;
    return this;
}

int frame_counter = 1;

void pushCompiledFrame (nuggetMachine *machine, compiledFunction *fn, int head, char *resultAddr) {
    DEBUG("ENTER FRAME %d: %s\n", frame_counter, fn->_fn.name);
    stackFrame *frame = calloc(offsetof(stackFrame, data) + fn->frameSize, 1);
    frame->machine = machine;
    frame->parent = machine->frame;
    frame->fn = (nuggetFunction*) fn;
    frame->id = frame_counter++;
    frame->size = fn->frameSize;
    frame->program = fn->program;
    frame->types = fn->types;
    frame->error = 0;
    frame->ipc = frame->pc = 0;

    frame->head = head;
    frame->resultAddr = resultAddr;

    int groupSize = sizeof(int) * 4;
    int groupCount = fn->frameSize / groupSize + 1;
    frame->dataset = calloc(groupCount * groupSize, 1);

    frame->startClock = clock();
    frame->innerElapsed = 0;

    machine->frame = frame;
}

void popStackFrame (nuggetMachine *machine) {
    DEBUG("EXIT FRAME %d\n", machine->frame->id);
    stackFrame *oldFrame = machine->frame;
    if (oldFrame == NULL) {
        printf("Tried to pop the frame of an empty machine\n");
        return;
    }

    machine->frame = oldFrame->parent;

    // Unreference all the GC values
    for (int i = 0; i < oldFrame->size; ++i) {
        decref(oldFrame, i);
    }

    if (oldFrame->dataset) {
        free(oldFrame->dataset);
    }

    if (MonitorGetFlag(&machine->monitor, MONITOR_PROFILE)
        && oldFrame->fn->flags & NUGGET_FUNCTION_MONITOR
        && oldFrame->startClock > 0
    ) {
        int elapsed = clock() - oldFrame->startClock;
        oldFrame->fn->callCount++;
        oldFrame->fn->time += elapsed;
        oldFrame->fn->innerTime += oldFrame->innerElapsed;

        if (oldFrame->parent) {
            oldFrame->parent->innerElapsed += elapsed;
        }
    }

    free(oldFrame);
}

void executeConstant (nuggetMachine *machine, Constant *fn) {
    nuggetFunction *base = fn->base;

    int dataSize = 0;

    int mainCallOffset = (INT_PER_PTR + 2) * base->inCount;
    int mainArgOffset = mainCallOffset + 1 + INT_PER_PTR;
    int *program = calloc(sizeof(int), mainArgOffset + base->inCount + 3);


    int pc = 0;


    // Args
    for (int i = 0; i < base->inCount; i++) {
        // Call instruction
        program[pc++] = InstCall;
        memcpy(&program[pc], &fn->args[i], sizeof(void*));
        pc += INT_PER_PTR;

        // current value index is the dataSize

        // no arguments for calls
        // return Address
        program[pc++] = dataSize;

        // Argument to main call
        program[mainArgOffset + i] = dataSize;

        // both increases the size of the data buffer, and gives the index of the next argument
        dataSize += base->ins[i]->size;
    }

    // pc should == mainCallOffset
    program[pc++] = InstCall;
    memcpy(&program[pc], &base, sizeof(base));

    // Jump all the arguments, they were written in the loop above
    pc += INT_PER_PTR + base->inCount;

    // Return address
    int resultIndex = dataSize;
    program[pc++] = resultIndex;

    // Allocate enough space for the output
    for (int i = 0; i < base->outCount; i++) {
        dataSize += base->outs[i]->size;
    }

    program[pc++] = InstEnd;
    program[pc++] = resultIndex;

    // Collect the types of the stack, for cleanup
    nuggetType **types = calloc(sizeof(void*), dataSize);
    int index = 0;
    for (int i = 0; i < base->inCount; i++) {
        types[index] = base->ins[i];
        index += base->ins[i]->size;
    }
    for (int i = 0; i < base->outCount; i++) {
        types[index] = base->outs[i];
        index += base->outs[i]->size;
    }

    DEBUG("ENTER CONST FRAME %d: %s(%s)\n", frame_counter, fn->_fn.name, base->name);
    

    stackFrame *frame = calloc(offsetof(stackFrame, data) + dataSize, 1);
    frame->machine = machine;
    frame->parent = machine->frame;
    frame->fn = (nuggetFunction*) fn;
    frame->id = frame_counter++;
    frame->size = dataSize;
    frame->program = program;
    frame->types = types;
    frame->error = 0;
    frame->ipc = frame->pc = 0;
    frame->head = 1;
    // The machine will write the constant value here, no need to write anything ourselves
    frame->resultAddr = fn->result;

    int groupSize = sizeof(int) * 4;
    int groupCount = dataSize / groupSize + 1;
    frame->dataset = calloc(groupCount * groupSize, 1);

    // Run the generated instructions and wait for it to finish
    machine->frame = frame;
    executionLoop(machine);

    if (machine->state != machineStateStopped) {
        // Something went wrong
    }

    fn->executed = 1;
    machine->state = machineStateRunning;

    // Cleanup
    //free(frame->types);
    //free(frame->dataset);
    free(program);
}

void nextInstruction (nuggetMachine *machine) {
    stackFrame *frame = machine->frame;

    // DEBUG("frame %p : %d\n", frame, frame->pc);

    int prev_ipc = frame->ipc;
    int ipc = frame->ipc = frame->pc++;

    int dolog = 0;
    if (MonitorGetFlag(&machine->monitor, MONITOR_LOG)) {
        char *repr;
        printf("[F%d] %d:", frame->id, ipc);
        dolog = 1;
    }

    if (frame->fn->kind == nuggetFunctionCompiled) {
        int size = ((compiledFunction*) frame->fn)->programSize;
        if (ipc >= size || ipc < 0) {
            frame->ipc = prev_ipc;

            if (dolog) printf(" OUT OF BOUNDS\n");
            nuggetPushError(machine, "Instruction out of program bounds");
            return;
        }
    }

    int modified[24];
    int modifiedCount = 0;
    #define MOD(ADDR) modified[modifiedCount++]=ADDR

    switch (frame->program[ipc]) {
        case InstMov: {
            int size = frame->program[frame->pc++];
            int dst = frame->program[frame->pc++];
            int src = frame->program[frame->pc++];

            if (dolog) printf(" r#%d <- r#%d\n", dst, src);

            decref(frame, dst);
            if (!stackFrameReadReg(frame, src, &frame->data[dst])) return;
            stackFrameSetRegFlag(frame, dst, 1);

            MOD(dst);
            break;
        }
        case InstCall: {
            nuggetFunction *fn;
            memcpy(&fn, &frame->program[frame->pc], sizeof(fn));
            frame->pc += INT_PER_PTR;

            if (dolog) {
                int _pc = frame->pc;
                if (fn->outCount) {
                    int off = _pc + fn->inCount;
                    for (int i = 0; i < fn->outCount; i++) {
                        printf(" r#%d", frame->program[off + i]);
                    }
                    printf(" <-");
                }

                printf(" %s (", fn->name, fn->inCount);
                for (int i = 0; i < fn->inCount; i++) {
                    printf(" r#%d", frame->program[_pc + i]);
                }
                printf(" )\n");

                fflush(stdout);
            }


            // pc is pointing at argument list now
            // after the arguments, goes the return address

            reapply:

            switch (fn->kind) {
                case nuggetFunctionConst: {
                    RECAST(fn, Constant)

                    if (!fn->executed) {
                        executeConstant(machine, fn);
                    }

                    int addr = frame->program[frame->pc++];
                    void *src = fn->result;
                    stackFrameWriteReg(frame, addr, src);
                    MOD(addr);
                    break;
                }
                case nuggetFunctionBuiltin: {
                    // For monitoring
                    int outaddr = frame->program[frame->pc + fn->inCount];
                    for (int i = 0; i < fn->outCount; ++i) {
                        MOD(outaddr);
                        outaddr += fn->outs[i]->size;
                    }

                    RECAST(fn, BuiltinFunction)

                    fn->impl(frame, fn->data);

                    if (frame->error) {
                        machine->state = machineStateError;
                    }
                    break;
                }
                case nuggetFunctionCustom: {
                    CustomFunction *custom = (CustomFunction*) fn;

                    nuggetFunctionArgs *args = nuggetFunctionArgsNew(fn);

                    for (int i = 0; i < fn->inCount; ++i) {
                        int addr = frame->program[frame->pc++];
                        //incref(frame, addr);
                        nuggetFunctionArgsWriteInput(args, i, &frame->data[addr]);
                    }

                    custom->impl(args, custom->data);
                    if (machine->state == machineStateError) break;

                    if (fn->outCount > 0) {
                        int addr = frame->program[frame->pc++];
                        for (int i = 0; i < fn->outCount; ++i) {
                            // TODO: Dry?

                            decref(frame, addr);
                            nuggetFunctionArgsReadOutput(args, i, &frame->data[addr]);
                            stackFrameSetRegFlag(frame, addr, 1);
                            MOD(addr);
                            addr += fn->outs[i]->size;
                        }
                    }
                    break;
                }
                case nuggetFunctionCompiled: {
                    pushCompiledFrame(machine, (compiledFunction*) fn, 0, 0);

                    // printf("PREIN: %d\n", frame->pc);
                    int offset = 0;
                    for (int i = 0; i < fn->inCount; ++i) {
                        int addr = frame->program[frame->pc++];
                        int written = stackFrameTransferReg(
                            // dst
                            machine->frame, offset,
                            // src
                            frame, addr
                        );
                        if (!written) return;
                        offset += written;
                    }

                    // printf("PREOUT: %d\n", frame->pc);
                    if (fn->outCount > 0) {
                        // printf("O1: %d\n", frame->pc);
                        int addr = frame->program[frame->pc++];
                        // printf("O2: %d\n", frame->pc);

                        //decref(frame, addr);

                        // printf("O3: %d\n", frame->pc);
                        //machine->frame->resultAddr = &frame->data[addr];
                        // printf("O4: %d\n", frame->pc);
                    }
                    // printf("POSTOUT: %d\n", frame->pc);
                    break;
                }
                case nuggetFunctionApply: {
                    // Replace the active funcion with the argument, and process the call again
                    memcpy(&fn, &frame->data[frame->program[frame->pc++]], sizeof(void*));
                    goto reapply;
                }
                default:
                    printf("Cannot execute function kind: %d\n", fn->kind);
                    machine->state = machineStateError;
            }
            break;
        }
        case InstEnd: {
            // TODO: Organizedly choose how to write the results:
            // either a frame register transfer or siple write.
            char *ptr = frame->resultAddr;

            if (dolog) {
                printf(" end");
                for (int i = 0; i < frame->fn->outCount; i++) {
                    printf(" r#%d", frame->program[ipc+1+i]);
                }
                printf("\n");
            }

            stackFrame *parent = frame->parent;
            int paddr = -1;
            if (parent) {
                // Hack the return address out of the parent stack frame
                paddr = parent->program[parent->pc-1];

                // paddr takes priority over ptr, so make sure it
                // points to the same place as ptr if it's defined
                if (ptr && ptr != &parent->data[paddr]) {
                    paddr = -1;
                }
            }

            for (int i = 0; i < frame->fn->outCount; i++) {
                int addr = frame->program[frame->pc++];
                nuggetType *type = frame->fn->outs[i];
                // TODO: Assert same type as register

                if (paddr >= 0) {
                    int written = stackFrameTransferReg(
                        // dst
                        parent, paddr,
                        // src
                        frame, addr                    
                    );
                    if (!written) return;
                    MOD(paddr);
                    paddr += written;
                } else {
                    int written = stackFrameReadReg(frame, addr, ptr);
                    if (!written) return;
                    ptr += written;
                }
            }

            int head = frame->head;
            popStackFrame(machine);

            if (head) {
                machine->state = machineStateStopped;
                modifiedCount = 0;
            } else {
                frame = parent;
            }
            break;
        }
        case InstJmp: {
            if (dolog) printf(" jmp %d\n", frame->pc+1);

            frame->pc = frame->program[frame->pc++];
            break;
        }
        case InstJif: {
            if (dolog) printf(" jif %d r#%d\n", frame->pc+1, frame->pc+2);

            int dst = frame->program[frame->pc++];
            char cond = frame->data[frame->program[frame->pc++]];
            if (cond) frame->pc = dst;
            break;
        }
        case InstNif: {
            if (dolog) printf(" nif %d r#%d\n", frame->pc+1, frame->pc+2);

            int dst = frame->program[frame->pc++];
            char cond = frame->data[frame->program[frame->pc++]];
            if (!cond) frame->pc = dst;
            break;
        }
        default:
            if (dolog) printf(" ???\n");
            nuggetPushErrorF(machine, 31, "Unknown instruction %d", frame->program[frame->ipc]);
            break;
    }

    for (int i = 0; i < modifiedCount; i++) {
        int addr = modified[i];
        nuggetType *type = frame->types[addr];

        if (dolog) {
            char *valstr = nuggetValueString(0, &frame->data[addr], type);
            printf("\tr#%d: %s\n", addr, valstr);
            free(valstr);
        }

        if (type->flags & NUGGET_TYPE_FLAG_GC) {
            void *value;
            memcpy(&value, &frame->data[addr], sizeof(void*));
            if (!gc_isValid(value)) {
                char *tname = 0;
                if (frame->types[addr]) {
                    tname = frame->types[addr]->name;
                }
                nuggetPushErrorF(
                    machine, 34 + strlen(tname),
                    "Register #%d (%s) checked invalid",
                    addr, tname
                );
                return;
            }
        }
    }

    // Make sure the log always shows
    if (dolog) fflush(stdout);
}

void executionLoop (nuggetMachine *machine) {
    while (machine->state == machineStateRunning) {
        nextInstruction(machine);
    }
}

void nuggetFunctionCall (nuggetMachine *machine, nuggetFunction *fn, nuggetFunctionArgs *args) {
    int prevstate = machine->state;

    switch (fn->kind) {
        case nuggetFunctionCompiled:
            pushCompiledFrame(machine, (compiledFunction*) fn, 1, &args->data[args->insize]);

            // This copies all the arguments at once, but doesn't do bookkeeping
            // memcpy(machine->frame->data, args->data, args->insize);
            machine->state = machineStateRunning;

            int addr = 0;
            for (int i = 0; i < fn->inCount; ++i) {
                nuggetType *type = fn->ins[i];
                void *srcvalptr = &args->data[addr];
                int written = stackFrameWriteReg(machine->frame, addr, srcvalptr);
                assert(written > 0);
                addr += written;
            }

            executionLoop(machine);
            break;
        case nuggetFunctionCustom: {
            // TODO: Bookkeeping

            CustomFunction *custom = (CustomFunction*) fn;
            custom->impl(args, custom->data);
            break;
        }
        case nuggetFunctionBuiltin: {
            // TODO: Bookkeeping
            int count = fn->inCount + (fn->outCount > 0 ? 1 : 0);

            int *program = malloc(sizeof(int) * count);

            int size = 0;

            for (int i = 0; i < fn->inCount; ++i) {
                program[i] = size;
                size += fn->ins[i]->size;
            }

            if (fn->outCount > 0) {
                program[fn->inCount] = size;
                for (int i = 0; i < fn->outCount; ++i) {
                    size += fn->outs[i]->size;
                }
            }

            stackFrame *frame = calloc(offsetof(stackFrame, data) + size, 1);
            frame->machine = machine;
            frame->parent = machine->frame;
            frame->fn = fn;
            frame->size = size;
            frame->program = program;
            frame->error = 0;
            frame->pc = 0;
            frame->head = 1;

            int insize = args->insize;

            memcpy(frame->data, args->data, insize);

            BuiltinFunction *_fn = (BuiltinFunction*) fn;
            _fn->impl(frame, _fn->data);

            if (frame->error) {
                if (machine->flags & NUGGET_FLAG_SHELL_ON_ERROR) {
                    nuggetStartShell(machine);

                    // disable the flag so that outer frames in the stack don't trigger
                    machine->flags &= ~NUGGET_FLAG_SHELL_ON_ERROR;
                }
                machine->state = machineStateError;
                free(program);
                free(frame);
                break;
            }

            memcpy(&args->data[insize], &frame->data[insize], size - insize);

            free(program);
            free(frame);

            break;
        }
        case nuggetFunctionConst: {
            // TODO: Bookkeeping

            RECAST(fn, Constant)

            if (!fn->executed) {
                int prevstate = machine->state;
                machine->state = machineStateRunning;
                executeConstant(machine, fn);
                machine->state = prevstate;
            }

            memcpy(args->data, fn->result, fn->type->size);
            break;
        }
        case nuggetFunctionApply: {
            printf("TODO: Apply function calls\n", fn->kind);
            break;
        }
        default:
            printf("Unknown call strategy for function kind: %d\n", fn->kind);
    }

    // TODO: Don't have redundant error states.
    if (machine->err || machine->error) {
        machine->state = machineStateError;
    } else {
        machine->state = prevstate;
    }
}

nuggetModule *nuggetMachineGetModule (nuggetMachine *machine, char *name) {
    nuggetModule **entry = (nuggetModule**) HashmapGetPtr(machine->moduleMap, name);
    if (*entry == 0) {
        *entry = machine->loader(machine, name);
    }
    return *entry;
}


void *nuggetMachineGetData (nuggetMachine *machine) { return machine->data; }