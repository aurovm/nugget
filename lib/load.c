#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include "headers.h"
#include "load.h"
#include "sourcemap.h"
#include "function.h"


//#define DO_DEBUG

#ifdef DO_DEBUG
    #define DEBUG(...) printf(__VA_ARGS__)
#else
    #define DEBUG(...)
#endif

#define NO_CONTEXT (-2)
#define MAIN_CONTEXT (-1)
#define COMPUTING_CONTEXT (-3)

nuggetType *loadType (loadedModule* mod, int index);
nuggetModule *loadModule (loadedModule* mod, int index);
nuggetFunction *loadFunction (loadedModule* mod, int index);

typedef struct {
    nuggetModule _mod;

    loadedModule *base;

    int itemCount;
    itemEntry *items;
} DefineModule;

typedef struct ContextModule {
    nuggetModule _mod;
    loadedModule *parent;
    int contextIndex;
} ContextModule;


nuggetItem defineGetItem (nuggetModule *_mod, char *query) {
    DefineModule *mod = (DefineModule*) _mod;

    int matchIndex;
    int matchCount = 0;

    for (int i = 0; i < mod->itemCount; i++) {
        int nmatch = nameMatch(query, mod->items[i].name);

        if (nmatch == EXACT_MATCH) {
            matchIndex = i;
            matchCount = 1;
            break;
        } else if (nmatch == INEXACT_MATCH) {
            matchIndex = i;
            matchCount++;
        }
    }

    if (matchCount == 0) {
        return EMPTY_ITEM;
    }

    if (matchCount > 1) {
        pushErr(mod->base->machine, "Multiple matches for query '%s'", query);
        return EMPTY_ITEM;
    }

    itemEntry *entry = &mod->items[matchIndex];
    nuggetItem item = entry->item;

    if (item.kind == nuggetItemEmpty) {
        nuggetDefineItemDesc desc = entry->desc;

        if (desc.kind == nuggetDefineFunctionItem) {
            item.kind = nuggetItemFunction;
            item.item = loadFunction(mod->base, desc.index);
        } else if (desc.kind == nuggetDefineTypeItem) {
            item.kind = nuggetItemType;
            item.item = loadType(mod->base, desc.index);
        } else if (desc.kind == nuggetDefineModuleItem) {
            item.kind = nuggetItemModule;
            item.item = loadModule(mod->base, desc.index);
        } else {
            pushErr(mod->base->machine, "Invalid define entry kind: %d", desc.kind);
            item.item = 0;
        }

        mod->items[matchIndex].item = item;
    }

    if (item.item == 0) {
        item.kind = nuggetItemError;
    }

    return item;
}

char *kindNames[6] = {"Empty", "Module", "Type", "Function", "", ""};

nuggetModule *contextBuild (nuggetModule *_mod, nuggetModule *argument) {
    //printf("context build %d\n", );
    ContextModule *ctxmod = (ContextModule*) _mod;

    // Clone all the memory, but reallocate the item caches
    loadedModule *mod = malloc(sizeof(loadedModule));
    memcpy(mod, ctxmod->parent, sizeof(loadedModule));

    mod->modules = calloc(mod->moduleCount, sizeof(void*));
    mod->functions = calloc(mod->functionCount, sizeof(void*));
    mod->types = calloc(mod->typeCount, sizeof(void*));

    mod->parent = ctxmod->parent;
    mod->contextIndex = ctxmod->contextIndex;
    assert(argument != 0);
    mod->argument = argument;

    return loadModule(mod, ctxmod->contextIndex + 2);
}


nuggetModuleItem *getModuleItemDesc (nuggetModuleDesc *desc, int index, int *isarg) {
    if (index < 1) return 0;
    index--;

    for (int i = 0; i < desc->moduleCount; i++) {
        if (index == i) {
            *isarg = 0;
            return &desc->modules[index];
        }
        if (desc->modules[i].kind == nuggetModuleItemContext) {
            if (index == i+1) {
                *isarg = 1;
                return &desc->modules[i];
            } else {
                index--;
            }
        }
    }

    return 0;
}

int inContext (contextInfo *cinfo, int context) {
    for (int i = 0; i < cinfo->activeCount; ++i) {
        if (context == cinfo->active[i]) return 1;
    }
    return 0;
}

int isChild (contextInfo *cinfo, int child, int parent) {
    int next = child;
    while (next != NO_CONTEXT) {
        if (next < 0) {
            return 0;
        }
        next = cinfo->items[next];
        if (next == parent) {
            return 1;
        }
    }
    return 0;
}

int lowestContext (contextInfo *cinfo, int a, int b) {
    if (a == NO_CONTEXT || b == NO_CONTEXT) return NO_CONTEXT;
    if (a == b) return a;
    if (inContext(cinfo, a)) return b;
    if (inContext(cinfo, b)) return a;
    if (isChild(cinfo, a, b)) return a;
    if (isChild(cinfo, b, a)) return b;
    if (a == MAIN_CONTEXT) return b;
    if (b == MAIN_CONTEXT) return a;
    return NO_CONTEXT;
}

int ctx_level = 0;

int computeContext (loadedModule *mod, int itemKind, int index) {
    if (index < 0) return NO_CONTEXT;

    // Use fallthrough
    int cindex = index;
    switch (itemKind) {
        case nuggetItemFunction: cindex += mod->typeCount;
        case nuggetItemType: cindex += mod->moduleCount;
    }

    #ifdef DO_DEBUG
        char ident[128];
        if (ctx_level*2 >= 128) {
            printf("STACK OVERFLOW! context too deep! (ctx_level = %d)\n", ctx_level);
            abort();
        }

        for (int i = 0; i < ctx_level; i++) {
            ident[i*2] = '|';
            ident[i*2+1] = ' ';
        }
        ident[ctx_level*2] = 0;
    #endif

    int context = mod->contextInfo->items[cindex];

    if (context == COMPUTING_CONTEXT) {
        mod->contextInfo->recursions[cindex]++;
        mod->contextInfo->totalRecursion++;
        DEBUG("%sContext %s[%d] recursive: %d\n", ident, kindNames[itemKind], index, MAIN_CONTEXT);
        return MAIN_CONTEXT;
    }

    // The second conditional prevents previously cached items to affect the context calculation
    // when a context is active
    if (context != NO_CONTEXT && !inContext(mod->contextInfo, context)) {
        DEBUG("%sContext %s[%d] cached: %d\n", ident, kindNames[itemKind], index, context);
        return context;
    };

    ctx_level++;

    DEBUG("%sCompute Context %s[%d]\n", ident, kindNames[itemKind], index);

    int origRecursedCount = mod->contextInfo->totalRecursion;

    int *origItems = mod->contextInfo->items;
    int origContext = origItems[cindex];
    origItems[cindex] = COMPUTING_CONTEXT;

    switch (itemKind) {
        case nuggetItemModule: {
            int isarg;
            nuggetModuleItem *desc = getModuleItemDesc(mod->desc, index, &isarg);
            if (desc == 0) {
                pushErr(mod->machine, "Invalid module index %d (context)", kindNames[itemKind], index);
                ctx_level--;
                return NO_CONTEXT;
            }

            switch (desc->kind) {
                case nuggetModuleItemImport: {
                    context = MAIN_CONTEXT;
                    break;
                }
                case nuggetModuleItemDefine: {
                    context = MAIN_CONTEXT;
                    for (int i = 0; i < desc->data.define.count; i++) {

                        nuggetDefineItemDesc idesc = desc->data.define.items[i];
                        int kind =
                            (idesc.kind == nuggetDefineFunctionItem) ? nuggetItemFunction :
                            (idesc.kind == nuggetDefineTypeItem) ? nuggetItemType :
                            (idesc.kind == nuggetDefineModuleItem) ? nuggetItemModule :
                            -1;

                        if (kind == -1) {
                            pushErr(mod->machine, "Invalid define entry kind: %d", idesc.kind);
                            return NO_CONTEXT;
                        }

                        int ictx = computeContext(mod, kind, idesc.index);
                        if (ictx == NO_CONTEXT) {
                            pushErr(mod->machine, "Failed to get context of define item %s", idesc.name);
                            ctx_level--;
                            return NO_CONTEXT;
                        }
                        context = lowestContext(mod->contextInfo, context, ictx);
                    }
                    break;
                }
                case nuggetModuleItemBuild: {
                    context = lowestContext(
                        mod->contextInfo,
                        computeContext(mod, nuggetItemModule, desc->data.build.base),
                        computeContext(mod, nuggetItemModule, desc->data.build.argument)
                    );
                    break;
                }
                case nuggetModuleItemUse: {
                    context = computeContext(mod, nuggetItemModule, desc->data.use.base);
                    break;
                }
                case nuggetModuleItemContext: {
                    contextInfo *ci = mod->contextInfo;

                    if (isarg) {
                        context = index - 1;
                        // This block of code will be executed only once per context, after this the result will be cached.
                        if (inContext(mod->contextInfo, context)) {
                            // Duplicate cache first so that item contexts don't get saved incorrectly.
                            size_t size = sizeof(int) * ci->itemCount;
                            int *items = malloc(size);
                            memcpy(items, ci->items, size);
                            ci->items = items;

                            // Then replace the context result
                            context = MAIN_CONTEXT;
                        }
                    } else {
                        DEBUG("%sENTER CONTEXT: %d\n", ident, index);

                        int *items = ci->items;
                        ci->active[ci->activeCount++] = index;
                        context = computeContext(mod, nuggetItemModule, index + 2);
                        ci->activeCount--;

                        // If the item cache was changed, it means the context argument was used at some point.
                        // Restore the items for caching all items outside this context
                        if (ci->items != items) {
                            free(ci->items);
                            ci->items = items;
                        }
                    }
                    break;
                }
                default:
                    pushErr(mod->machine, "Unknown module kind for context");
                    return NO_CONTEXT;
            }
            break;
        }
        case nuggetItemType: {
            nuggetTypeItem *desc = &mod->desc->types[index];
            DEBUG("%sType[%d] { Module[%d].%s }\n", ident, index, desc->module, desc->name);
            context = computeContext(mod, nuggetItemModule, desc->module);
            break;
        }
        case nuggetItemFunction: {
            nuggetFunctionItem *desc = &mod->desc->functions[index];

            int inCount = desc->inCount;
            int outCount = desc->outCount;

            int tcontext = MAIN_CONTEXT;

            for (int i = 0; i < inCount; i++) {
                int tctx = computeContext(mod, nuggetItemType, desc->ins[i]);
                if (tctx == NO_CONTEXT) {
                    pushErr(mod->machine, "Could not load input type context %d for Function[%d]", i, index);
                    return NO_CONTEXT;
                }
                tcontext = lowestContext(mod->contextInfo, tcontext, tctx);
            }

            for (int i = 0; i < outCount; i++) {
                int tix = desc->outs[i];

                // it's -1 for constant functions: const calls, int and bin
                if (tix != -1) {
                    int tctx = computeContext(mod, nuggetItemType, tix);
                    if (tctx == NO_CONTEXT) {
                        pushErr(mod->machine, "Could not load output type context %d for Function[%d]", i, index);
                        return NO_CONTEXT;
                    }
                    tcontext = lowestContext(mod->contextInfo, tcontext, tctx);
                }
            }

            int fcontext = MAIN_CONTEXT;
            switch (desc->kind) {
                case nuggetFunctionCode:
                    int count = desc->data.code.count;
                    nuggetInstDesc *code = desc->data.code.code;

                    for (int i = 0; i < count; i++) {
                        nuggetInstDesc inst = code[i];
                        if (inst.kind == nuggetInstCall) {
                            int icontext = computeContext(mod, nuggetItemFunction, inst.data.call.fn);
                            fcontext = lowestContext(mod->contextInfo, fcontext, icontext);
                        }
                    }

                    break;
                case nuggetFunctionImport: {
                    fcontext = computeContext(mod, nuggetItemModule, desc->data.import.module);
                    break;
                }
                case nuggetFunctionConstCall: {
                    fcontext = computeContext(mod, nuggetItemFunction, desc->data.call.fn);
                    for (int i = 0; i < desc->data.call.paramCount; i++) {
                        fcontext = lowestContext(
                            mod->contextInfo,
                            fcontext,
                            computeContext(mod, nuggetItemFunction, desc->data.call.params[i])
                        );
                    }
                    break;
                }
                case nuggetFunctionEmpty:
                case nuggetFunctionBin:
                case nuggetFunctionInt: {
                    fcontext = MAIN_CONTEXT;
                    break;
                }
                default:
                    pushErr(mod->machine, "Unknown function kind for context: %d", desc->kind);
                    return 0;
            }
            context = lowestContext(mod->contextInfo, tcontext, fcontext);
            DEBUG("%st %d, f %d, c %d\n", ident, tcontext, fcontext, context);
            break;
        }
        default:
            pushErr(mod->machine, "Unknown item kind for context: %d", itemKind);
            return NO_CONTEXT;
    }

    ctx_level--;
    DEBUG("%sCONTEXT: %d\n", ident, context);

    if (inContext(mod->contextInfo, context)) {
        pushErr(mod->machine, "ERROR: Context results cannot be active (%d)", context);
        return NO_CONTEXT;
    }

    // If this item was computed recursively
    if (mod->contextInfo->recursions[cindex]) {
        mod->contextInfo->totalRecursion -= mod->contextInfo->recursions[cindex];
        mod->contextInfo->recursions[cindex] = 0;
    }

    // TODO: does the count not changing after removing appearances guarantees that it's safe to save this context?
    // If there are still recursively used items, do not save the context.
    mod->contextInfo->items[cindex] = (mod->contextInfo->totalRecursion > origRecursedCount) ? origContext : context;

    // If the cache item list changed, it means an active context was used.
    // Restore the context item entry in the original list
    if (origItems != mod->contextInfo->items) {
        if (strcmp(mod->_mod.name, "utils.alist") == 0 && itemKind == 1 && index == 5) {
            printf("OFFENDING ITEM\n");
        }
        origItems[cindex] = origContext;
    }

    return context;
}

loadedModule *getContextModule (loadedModule *mod, int itemKind, int index) {
    int context = computeContext(mod, itemKind, index);
    if (context == NO_CONTEXT) {
        pushErr(
            mod->machine,
            "Context for %s[%d] cannot be computed (at %s)",
            kindNames[itemKind], index, mod->_mod.name
        );
        return 0;
    }

    for (loadedModule *next = mod; next; next = next->parent) {
        if (next->contextIndex == context) {
            if (context != mod->contextIndex) {
                DEBUG("load module from %d's ancestor: %d\n", mod->contextIndex, context);
            }
            return next;
        }
    }

    pushErr(
        mod->machine,
        "%s[%d] is part of context %d, which is not a parent of context %d (at %s)",
        kindNames[itemKind], index, context, mod->contextIndex, mod->_mod.name
    );
    return 0;
}

nuggetModule *loadModule (loadedModule *mod, int index) {
    DEBUG("Load Module %d (%s)\n", index, mod->_mod.name);
    // TODO: Check index boundary
    if (index < 1 || index >= mod->moduleCount) {
        pushErr(mod->machine, "Invalid module index %d (load)", index);
        return 0;
    }

    loadedModule *ctxmod = getContextModule(mod, nuggetItemModule, index);
    if (!ctxmod) return 0;
    if (ctxmod != mod) return loadModule(ctxmod, index);


    nuggetModule *item = mod->modules[index];
    if (item != 0) return item;

    int isarg;
    nuggetModuleItem *desc = getModuleItemDesc(mod->desc, index, &isarg);

    if (desc == 0) {
        pushErr(mod->machine, "Invalid module index");
        return 0;
    }

    switch (desc->kind) {
        case nuggetModuleItemImport: {
            item = nuggetMachineGetModule(mod->machine, desc->data.import);
            if (item == 0) {
                pushErr(mod->machine, "Module '%s' not found", desc->data.import);
                return 0;
            }
            break;
        }
        case nuggetModuleItemDefine: {
            item = malloc(sizeof(DefineModule));
            RECAST(item, DefineModule);

            item->_mod.getItem = defineGetItem;
            item->_mod.build = 0;
            item->_mod.name = malloc(strlen(mod->_mod.name) + 16);
            sprintf(item->_mod.name, "%s[define.%d]", mod->_mod.name, index);

            item->base = mod;
            item->itemCount = desc->data.define.count;
            item->items = malloc(sizeof(itemEntry) * item->itemCount);

            for (int i = 0; i < item->itemCount; i++) {
                item->items[i] = (itemEntry) {
                    desc: desc->data.define.items[i],
                    name: desc->data.define.items[i].name,
                    item: (nuggetItem) {
                        kind: nuggetItemEmpty,
                        item: 0,
                    },
                };
            }

            break;
        }
        case nuggetModuleItemBuild: {
            nuggetModule *base = loadModule(mod, desc->data.build.base);
            if (!base) {
                pushErr(mod->machine, "Could not load base module for build (%d)", index);
                return 0;
            }

            if (base->build == 0) {
                pushErr(mod->machine, "Module can't be built");
                return 0;
            }

            nuggetModule *argument = loadModule(mod, desc->data.build.argument);
            if (!argument) {
                pushErr(mod->machine, "Could not load argument module for build (%d)", index);
                return 0;
            }

            item = base->build(base, argument);
            break;
        }
        case nuggetModuleItemUse: {
            nuggetModule *base = loadModule(mod, desc->data.use.base);
            if (!base) {
                pushErr(mod->machine, "Could not load base module for use (%d)", index);
                return 0;
            }

            nuggetItem import = nuggetModuleGetItem(base, desc->data.use.name);
            if (import.kind != nuggetItemModule) {
                pushErr(mod->machine, "Item '%s' of module '%s' is not a module", desc->data.use.name, base->name);
                return 0;
            }

            item = import.item;
            break;

            nuggetModule *argument = loadModule(mod, desc->data.build.argument);
            if (!argument) {
                pushErr(mod->machine, "Could not load argument module");
                return 0;
            }

            item = base->build(base, argument);
            break;
        }
        case nuggetModuleItemContext: {
            if (isarg) {
                int context = index-1;
                loadedModule *contextMod = mod;

                // TODO: Transform this into
                // while (contextMod->contextIndex != context) {...}
                // item = contextMod->argument;
                while (!item) {
                    if (contextMod->contextIndex == context) {
                        item = contextMod->argument;
                        if (!item) {
                            pushErr(mod->machine, "Context %d has no applied argument", context);
                            return 0;
                        }
                    } else {
                        contextMod = contextMod->parent;
                        if (!contextMod->parent) {
                            pushErr(mod->machine, "Context %d is not contained in context %d", context, mod->contextIndex);
                            return 0;
                        }
                    }
                }
            } else {
                item = malloc(sizeof(ContextModule));
                RECAST(item, ContextModule);

                item->_mod.getItem = 0;
                item->_mod.build = contextBuild;
                item->_mod.name = malloc(strlen(mod->_mod.name) + 16);
                sprintf(item->_mod.name, "%s[context.%d]", mod->_mod.name, index);

                item->parent = mod;
                item->contextIndex = index;
            }
            break;
        }
        default:
            pushErr(mod->machine, "Unknown module kind: %d (%d)", desc->kind, index);
            return 0;
    }

    mod->modules[index] = item;
    return item;
}

char *typeLoc (loadedModule *mod, int index) {
    char *loc = malloc(11 + strlen(mod->_mod.name) + 4);
    sprintf(loc, "%s Type[%d]", mod->_mod.name, index);
    return loc;
}

nuggetType *loadType (loadedModule *mod, int index) {
    DEBUG("Load Type %d\n", index);

    // TODO: Check index boundary

    loadedModule *ctxmod = getContextModule(mod, nuggetItemType, index);
    if (!ctxmod) return 0;
    if (ctxmod != mod) return loadType(ctxmod, index);

    nuggetType *item = mod->types[index];
    if (item != 0) return item;

    nuggetTypeItem *desc = &mod->desc->types[index];

    nuggetModule *parent = loadModule(mod, desc->module);
    if (parent == 0) {
        pushErr(mod->machine, "Could not load module %d for type %d, in %s", desc->module, index, mod->_mod.name);
        return 0;
    }

    nuggetItem imported = nuggetModuleGetItem(parent, desc->name);
    if (imported.kind != nuggetItemType) {
        char *msg = malloc(33 + strlen(desc->name));
        sprintf(msg, "Item '%s' in module is not a type", desc->name);
        pushLoadError(mod->machine, msg, typeLoc(mod, index));
        return 0;
    }

    item = imported.item;
    if (item == 0) {
        pushErr(mod->machine, "Item '%s' in module is NULL", desc->name);
        return 0;
    }

    if (item->flags & NUGGET_TYPE_FLAG_UNNAMED) {
        nuggetMeta *sourceMap = sourceMapFindType(mod->sourceMap, index);
        if (sourceMap) {
            char *name = sourceMapItemName(sourceMap);
            if (name) {
                // Should this be set regardless of the branch?
                // If this module didn't name it, nobody else should
                item->flags &= ~NUGGET_TYPE_FLAG_UNNAMED;

                char *fullname = malloc(strlen(name) + strlen(mod->_mod.name) + 2);
                sprintf(fullname, "%s.%s", mod->_mod.name, name);
                item->name = fullname;
            }
        }
    }

    mod->types[index] = item;
    return item;
}

compiledFunction *createCode (loadedModule* mod, nuggetFunctionItem *desc, int index) {
    compiledFunction *fn = nuggetFunctionAlloc(mod->machine, sizeof(compiledFunction));

    fn->_fn.kind = nuggetFunctionCompiled;

    fn->module = mod;
    fn->sourceMap = sourceMapFindFunction(mod->sourceMap, index);

    char nbuf[15];
    char *name = sourceMapFunctionName(fn->sourceMap);
    if (!name) {
        sprintf(nbuf, "Function[%d]", index);
        name = nbuf;
    }

    fn->_fn.name = malloc(strlen(mod->_mod.name) + strlen(name) + 2);
    sprintf(fn->_fn.name, "%s.%s", mod->_mod.name, name);

    int inCount = desc->inCount;
    int outCount = desc->outCount;

    fn->_fn.inCount = inCount;
    fn->_fn.outCount = outCount;

    int tsize = inCount + outCount;
    if (tsize > 0) {
        nuggetType **types = calloc(sizeof(nuggetType), tsize);

        for (int i = 0; i < inCount; i++) {
            types[i] = loadType(mod, desc->ins[i]);
            if (types[i] == 0) {
                //pushErr(mod->machine, "Could not load input type %d for %s", i, fn->_fn.name);
                return 0;
            }
        }

        for (int i = 0; i < outCount; i++) {
            types[i + inCount] = loadType(mod, desc->outs[i]);
            if (types[i + inCount] == 0) {
                //pushErr(mod->machine, "Could not load output type %d for %s", i, fn->_fn.name);
                return 0;
            }
        }

        fn->_fn.ins = types;
        fn->_fn.outs = types + inCount;
    }

    fn->frameSize = 0;
    fn->program = 0;

    if (MonitorMatchesModule(&mod->machine->monitor, mod->_mod.name)) {
        fn->_fn.flags |= NUGGET_FUNCTION_MONITOR;
    }

    return fn;
}

char linenob[32];

// max size: 17
char* linenos (compiledFunction *fn, int index) {
    int line = sourceMapFunctionFindLine(fn->sourceMap, index);
    if (line < 0) {
        sprintf(linenob, "%d", index);
    } else {
        sprintf(linenob, "%d (line %d)", index, line);
    }
    return linenob;
}

int compileCode (compiledFunction *fn, loadedModule* mod, nuggetFunctionItem *desc) {
    // TODO: Free all buffers

    DEBUG("Compile Code %s\n", fn->_fn.name);

    int count = desc->data.code.count;
    nuggetInstDesc *code = desc->data.code.code;

    // --- Register assignment phase --- //

    int regCount = fn->_fn.inCount;

    int *instRegs = calloc(sizeof(int), count);
    nuggetFunction **instFns = calloc(sizeof(void*), count);

    for (int i = 0; i < count; i++) {
        nuggetInstDesc inst = code[i];
        switch (inst.kind) {
            case nuggetInstEnd:
            case nuggetInstHlt:
            case nuggetInstSet:
            case nuggetInstJmp:
            case nuggetInstJif:
            case nuggetInstNif:
                instRegs[i] = -1;
                break;
            case nuggetInstVar:
            case nuggetInstDup: {
                instRegs[i] = regCount++;
                break;
            }
            case nuggetInstCall: {
                nuggetFunction *cfn = loadFunction(mod, inst.data.call.fn);
                if (cfn == 0) {
                    char *fname = fn->_fn.name;
                    char *loc = malloc(14 + strlen(fname) + 17);
                    sprintf(loc, "instruction %s:%s", fname, linenos(fn, i));
                    pushLoadError(mod->machine, 0, loc);
                    return 0;
                }

                instFns[i] = cfn;
                instRegs[i] = regCount;
                regCount += cfn->outCount;
                break;
            }
            default:
                pushErr(mod->machine, "Unsupported instruction (register assignment phase): %d\n", inst.kind);
                return 0;
        }
    }

    // --- Type resolution phase --- //

    nuggetType **regTypes = calloc(sizeof(void*), regCount);

    for (int i = 0; i < fn->_fn.inCount; i++) {
        regTypes[i] = fn->_fn.ins[i];
    }

    char *instResolved = calloc(1, count);
    int resolveCount = 0;

    while (resolveCount < count) {
        int anyResolved = 0;

        for (int i = 0; i < count; i++) {
            if (instResolved[i]) continue;

            nuggetInstDesc inst = code[i];
            int resolved = 0;

            switch (inst.kind) {
                case nuggetInstEnd:
                case nuggetInstHlt:
                case nuggetInstVar:
                case nuggetInstJmp:
                case nuggetInstJif:
                case nuggetInstNif:
                    resolved = 1;
                    break;
                case nuggetInstSet: {
                    int a = inst.data.args[0];
                    int b = inst.data.args[1];
                    if (regTypes[b]) {
                        if (regTypes[a] && regTypes[a] != regTypes[b]) {
                            pushErr(mod->machine, "Tried to assign %s to a %s, at %s:%s",
                                regTypes[b]->name, regTypes[a]->name, fn->_fn.name, linenos(fn, i)
                            );
                            return 0;
                        }

                        regTypes[a] = regTypes[b];
                        resolved = 1;
                    }
                    break;
                }
                case nuggetInstDup: {
                    nuggetType *tp = regTypes[inst.data.args[0]];
                    if (tp) {
                        regTypes[instRegs[i]] = tp;
                        resolved = 1;
                    }
                    break;
                }
                case nuggetInstCall: {
                    nuggetFunction *fn = instFns[i];

                    for (int j = 0; j < fn->outCount; j++) {
                        regTypes[instRegs[i] + j] = fn->outs[j];
                    }

                    resolved = 1;
                    break;
                }
                default:
                    pushErr(mod->machine, "Unsupported instruction (type resolution phase): %d", inst.kind);
                    return 0;
            }

            if (resolved) {
                resolveCount++;
                instResolved[i] = 1;
                anyResolved = 1;
            }
        }

        if (!anyResolved) {
            pushLoadError(mod->machine, "Cannot resolve all register types", fn->_fn.name);
            return 0;
        }
    }

    // --- Typecheck phase --- //

    for (int i = 0; i < count; i++) {
        nuggetInstDesc inst = code[i];

        switch (inst.kind) {
            case nuggetInstHlt:
            case nuggetInstVar:
            case nuggetInstJmp:
            case nuggetInstDup:
            case nuggetInstSet: // already typechecked
                break;
            case nuggetInstJif:
            case nuggetInstNif:
                // TODO
                break;
            case nuggetInstEnd:
                if (fn->_fn.outCount != inst.data.call.argCount) {
                    // TODO: Add a proper pushLoadError if this actually happens
                    printf("End instruction count doesn't match function out count\n");
                    abort();
                }
                
                for (int j = 0; j < fn->_fn.outCount; j++) {

                    nuggetType *reg = regTypes[inst.data.call.args[j]];
                    nuggetType *ret = fn->_fn.outs[j];

                    if (!nuggetTypeEq(reg, ret)) {
                        char *fname = fn->_fn.name;
                        char *tname = ret->name;
                        char *rname = reg->name;
                        char *msg = malloc(38 + strlen(fname) + strlen(tname) + strlen(rname));
                        sprintf(
                            msg,
                            "%s %dth result is a %s, but %s was passed",
                            fname, j, tname, rname
                        );

                        char *loc = malloc(14 + strlen(fname) + 17);
                        sprintf(loc, "instruction %s:%s", fname, linenos(fn, i));
                        pushLoadError(mod->machine, msg, loc);
                        return 0;
                    }
                }
                break;
            case nuggetInstCall: {
                nuggetFunction *cfn = instFns[i];

                if (cfn->inCount != inst.data.call.argCount) {
                    pushErr(
                        mod->machine, "%s expects %d arguments, but was called with %d, at %s:%s",
                        cfn->name, cfn->inCount, inst.data.call.argCount, fn->_fn.name, linenos(fn, i)
                    );
                    return 0;
                }

                for (int j = 0; j < cfn->inCount; j++) {
                    int regIndex = inst.data.call.args[j];
                    nuggetType *reg = regTypes[regIndex];
                    nuggetType *param = cfn->ins[j];

                    if (!reg) {
                        pushErr(
                            mod->machine, "register %d is never set with a value, at %s:%s",
                            regIndex, fn->_fn.name, linenos(fn, i)
                        );
                        return 0;
                    }

                    if (!nuggetTypeEq(reg, param)) {
                        char *fname = cfn->name;
                        char *pname = param->name;
                        char *rname = reg->name;
                        char *msg = malloc(57 + strlen(fname) + strlen(pname) + strlen(rname));
                        sprintf(
                            msg,
                            "%s expects %s as it's %d argument, but was called with %s",
                            fname, pname, j, rname
                        );

                        fname = fn->_fn.name;
                        char *loc = malloc(14 + strlen(fname) + 17);
                        sprintf(loc, "instruction %s:%s", fname, linenos(fn, i));
                        pushLoadError(mod->machine, msg, loc);
                        return 0;
                    }
                }

                // No need to check output types because they are created based on the called function

                break;
            }
            default:
                pushErr(mod->machine, "Unsupported instruction (type checking phase): %d", inst.kind);
                return 0;
        }
    }


    // --- Register allocation phase --- //

    int *registers = calloc(sizeof(int), regCount);

    fn->frameSize = 0;
    for (int i = 0; i < regCount; i++) {
        registers[i] = fn->frameSize;
        fn->frameSize += regTypes[i]->size;
    }

    // Store the types of each register
    //   this will hog like crazy in big functions
    //   necessary for stack cleanup
    fn->types = calloc(sizeof(void*), fn->frameSize);
    int index = 0;
    for (int i = 0; i < regCount; i++) {
        fn->types[index] = regTypes[i];
        index += regTypes[i]->size;
    }

    // --- Instruction emission phase --- //

    int *instOffsets = calloc(sizeof(int), count);
    int *jumps = calloc(sizeof(int), count);
    int jumpCount = 0;

    int capacity = 32;
    int pc = 0;

    fn->codeCount = count;
    fn->program = malloc(sizeof(int) * capacity);
    fn->mapping = malloc(sizeof(int) * count);

    //#define IDEBUG(...) printf(__VA_ARGS__)
    #define IDEBUG(...)

    #define ENSURE(N) if (pc+(N) < capacity) { capacity += 32; fn->program = realloc(fn->program, sizeof(int) * capacity); }
    #define PUSH(EXPR) { IDEBUG(" %d", EXPR); fn->program[pc++] = EXPR; }
    #define PUSHI(EXPR) { IDEBUG("\n    %d (%d): " #EXPR, pc, i); fn->program[pc++] = EXPR; }


    IDEBUG("COMPILING: %s", fn->_fn.name);

    for (int i = 0; i < count; i++) {
        instOffsets[i] = pc;
        nuggetInstDesc inst = code[i];
        fn->mapping[i] = pc;
        sch:
        switch (inst.kind) {
            case nuggetInstEnd: {
                ENSURE(1 + inst.data.call.argCount);
                PUSHI(InstEnd);
                for (int j = 0; j < inst.data.call.argCount; j++) {
                    PUSH(registers[inst.data.call.args[j]]);
                }
                break;
            }
            case nuggetInstVar:
                break;
            case nuggetInstDup: {
                int a = inst.data.args[0];
                ENSURE(4);
                PUSHI(InstMov);
                PUSH(regTypes[a]->size);
                PUSH(registers[instRegs[i]]);
                PUSH(registers[a]);
                break;
            }
            case nuggetInstSet: {
                ENSURE(4);
                PUSHI(InstMov);
                PUSH(regTypes[inst.data.args[0]]->size);
                PUSH(registers[inst.data.args[0]]);
                PUSH(registers[inst.data.args[1]]);
                break;
            }
            case nuggetInstCall: {
                nuggetFunction *cfn = instFns[i];

                ENSURE(1 + INT_PER_PTR + cfn->inCount);

                PUSHI(InstCall);
                IDEBUG(" %s", cfn->name);

                // WRITE UNALIGNED
                memcpy(&fn->program[pc], &cfn, sizeof(void*));
                pc += INT_PER_PTR;

                // assert cfn->inCount == inst.data.call.argCount
                for (int j = 0; j < inst.data.call.argCount; j++) {
                    PUSH(registers[inst.data.call.args[j]]);
                }

                if (cfn->outCount > 0) {
                    ENSURE(1);
                    PUSH(registers[instRegs[i]]);
                }

                break;
            }
            case nuggetInstJmp: {
                ENSURE(2);
                PUSHI(InstJmp);
                jumps[jumpCount++] = pc;
                PUSH(inst.data.args[0]);
                break;
            }
            case nuggetInstJif: {
                ENSURE(3);
                PUSHI(InstJif);
                jumps[jumpCount++] = pc;
                PUSH(inst.data.args[0]);
                PUSH(registers[inst.data.args[1]]);
                break;
            }
            case nuggetInstNif: {
                ENSURE(3);
                PUSHI(InstNif);
                jumps[jumpCount++] = pc;
                PUSH(inst.data.args[0]);
                PUSH(registers[inst.data.args[1]]);
                break;
            }
            default:
                pushErr(mod->machine, "Unsupported instruction (code emission phase): %d\n", inst.kind);
                return 0;
        }
    }

    IDEBUG("\n");
    fn->programSize = pc;


    // --- Jump translation phase --- //

    for (int i = 0; i < jumpCount; ++i) {
        int p = jumps[i];
        int dst = fn->program[p];
        if (dst < 0 || dst >= count) {
            // TODO: this should be illegal, but the aulang compiler generates a lot of these
            // fprintf(stderr, "WARNING: Jump outside function code bounds, at %s:%d\n", fn->_fn.name, i);

            // Try to provoque a deliberate error if it reaches this point.
            fn->program[p] = -1;
        } else {
            fn->program[p] = instOffsets[fn->program[p]];
        }
    }

    // TODO: Free all

    free(registers);

    DEBUG("End Compile Code %s\n", fn->_fn.name);

    #undef ENSURE
    #undef PUSH
    #undef PUSHI
    #undef IDEBUG

    return 1;
}

char *functionLoc (loadedModule *mod, int index) {
    char *loc = malloc(15 + strlen(mod->_mod.name) + 4);
    sprintf(loc, "%s Function[%d]", mod->_mod.name, index);
    return loc;
}

nuggetFunction *loadFunction (loadedModule* mod, int index) {
    DEBUG("Load Function %d\n", index);

    // TODO: Check index boundary

    loadedModule *ctxmod = getContextModule(mod, nuggetItemFunction, index);
    if (!ctxmod) return 0;
    if (ctxmod != mod) return loadFunction(ctxmod, index);

    nuggetFunction *item = mod->functions[index];
    if (item != 0) return item;

    nuggetFunctionItem *desc = &mod->desc->functions[index];

    compiledFunction *toCompile = 0;

    switch (desc->kind) {
        case nuggetFunctionCode:
            toCompile = createCode(mod, desc, index);
            item = (nuggetFunction*) toCompile;
            break;
        case nuggetFunctionImport: {
            nuggetModule *parent = loadModule(mod, desc->data.import.module);
            if (parent == 0) {
                pushErr(mod->machine, "Could not load module %d for function %d, at %s", desc->data.import.module, index, mod->_mod.name);
                return 0;
            }

            nuggetItem imported = nuggetModuleGetItem(parent, desc->data.import.name);
            if (imported.kind == nuggetItemError) { return 0; }
            if (imported.kind != nuggetItemFunction) {
                char *iname = desc->data.import.name;
                char *pname = parent->name;

                char *msg = malloc(33 + strlen(iname) + (pname ? strlen(pname) : 16));
                sprintf(msg, "Item '%s' in %s is not a function", iname, pname ? pname : "<unnamed module>");

                pushLoadError(mod->machine, msg, functionLoc(mod, index));
                return 0;
            }

            item = imported.item;
            if (item == 0) {
                pushErr(mod->machine, "Item '%s' in module is NULL", desc->data.import.name);
                return 0;
            }

            // Type check the function
            nuggetFunction *fn = (nuggetFunction*) item;

            if (fn->inCount != desc->inCount) {
                char *msg = malloc(60 + strlen(fn->name));
                sprintf(msg, "Function '%s' was imported with %d inputs, but it has %d", fn->name, desc->inCount, fn->inCount);
                pushLoadError(mod->machine, msg, functionLoc(mod, index));
                return 0;
            }

            if (fn->outCount != desc->outCount) {
                char *msg = malloc(60 + strlen(fn->name));
                sprintf(msg, "Function '%s' was imported with %d outputs, but it has %d", fn->name, desc->outCount, fn->outCount);
                pushLoadError(mod->machine, msg, functionLoc(mod, index));
                return 0;
            }

            // TODO: Typecheck the actual types
            break;
        }
        case nuggetFunctionConstCall: {
            nuggetFunction *base = loadFunction(mod, desc->data.call.fn);
            if (base == 0) {
                pushErr(mod->machine, "Could not load function for call constant");
                return 0;
            }
            
            if (base->outCount < 1) {
                pushErr(mod->machine, "Function used for constant has no return values.");
                return 0;
            }

            nuggetType *type = base->outs[0];
            Constant *cns = calloc(offsetof(Constant, result) + type->size, 1);

            cns->base = base;
            cns->type = type;
            cns->executed = 0;

            cns->args = calloc(sizeof(void*), base->inCount);
            // TODO: Check desc->data.call.paramCount == base->inCount
            for (int i = 0; i < base->inCount; i++) {
                cns->args[i] = loadFunction(mod, desc->data.call.params[i]);
                if (cns->args[i] == 0) {
                    pushErr(mod->machine, "Could not load argument function %d for const call", i);
                    return 0;
                }
            }

            cns->_fn.kind = nuggetFunctionConst;
            cns->_fn.name = "<call constant>";
            cns->_fn.inCount = 0;
            cns->_fn.outCount = 1;
            cns->_fn.outs = &cns->type;

            item = cns;

            break;
        }
        case nuggetFunctionBin: {
            nuggetType *type = &buffer_t;

            int size = desc->data.bin.size;

            nuggetBuffer *bufInfo = nuggetBufferAlloc(size);

            bufInfo->size = size;
            memcpy(&bufInfo->data, desc->data.bin.bytes, size);

            Constant *cns = calloc(offsetof(Constant, result) + type->size, 1);

            cns->base = 0;
            cns->type = type;
            cns->executed = 1;
            memcpy(&cns->result, &bufInfo, type->size);

            cns->_fn.kind = nuggetFunctionConst;
            cns->_fn.name = "<bin constant>";
            cns->_fn.inCount = 0;
            cns->_fn.outCount = 1;
            cns->_fn.outs = &cns->type;

            item = cns;

            break;
        }
        case nuggetFunctionInt: {
            nuggetType *type = &int_t;

            Constant *cns = calloc(offsetof(Constant, result) + type->size, 1);

            cns->base = 0;
            cns->type = type;
            cns->executed = 1;
            memcpy(&cns->result, &desc->data.num, type->size);

            cns->_fn.kind = nuggetFunctionConst;
            cns->_fn.name = "<int constant>";
            cns->_fn.inCount = 0;
            cns->_fn.outCount = 1;
            cns->_fn.outs = &cns->type;

            item = cns;

            break;
        }
        default:
            pushErr(mod->machine, "Can only load code functions. Found: %d", desc->kind);
            return 0;
    }

    // Declare, and store reference to the function
    mod->functions[index] = item;

    // Compile if it's a code function
    if (toCompile) {
        int result = compileCode(toCompile, mod, desc);
        if (!result) return 0;
    }

    return item;
}



// Public API

nuggetModule *nuggetModuleDescLoad (nuggetModuleDesc *desc, nuggetMachine *machine, char *name) {
    loadedModule *mod = malloc(sizeof(loadedModule));
    mod->_mod.name = name;

    mod->_mod.getItem = 0;
    mod->_mod.build = 0;

    mod->desc = desc;
    mod->machine = machine;

    mod->parent = 0;
    mod->contextIndex = -1;
    mod->argument = 0;

    mod->sourceMap = sourceMapFindRoot(&desc->meta);

    if (desc->moduleCount < 1) {
        pushErr(mod->machine, "Module needs to define at least one module item");
        free(mod);
        return 0;
    }

    mod->moduleCount = desc->moduleCount + 1;
    for (int i = 0; i < desc->moduleCount; i++) {
        // Count the implicit context argument modules
        if (desc->modules[i].kind == nuggetModuleItemContext) {
            mod->moduleCount++;
        }
    }
    mod->modules = calloc(mod->moduleCount, sizeof(void*));

    mod->functionCount = desc->functionCount;
    mod->functions = calloc(mod->functionCount, sizeof(void*));

    mod->typeCount = desc->typeCount;
    mod->types = calloc(mod->typeCount, sizeof(void*));

    int count = mod->moduleCount + mod->typeCount + mod->functionCount;
    mod->contextInfo = malloc(sizeof(contextInfo));
    mod->contextInfo->activeCount = 0;

    mod->contextInfo->totalRecursion = 0;
    mod->contextInfo->itemCount = count;
    mod->contextInfo->items = malloc(count * sizeof(int));
    mod->contextInfo->recursions = malloc(count * sizeof(int));
    for (int i = 0; i < count; ++i) {
        mod->contextInfo->items[i] = NO_CONTEXT;
        mod->contextInfo->recursions[i] = 0;
    }

    return loadModule(mod, 1);
}

nuggetFunction *nuggetCompiledModuleGetFunction (nuggetModule *_m, int index) {
    return loadFunction((loadedModule*) _m, index);
}