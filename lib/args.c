#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include "headers.h"
#include "value.h"

nuggetFunctionArgs *nuggetFunctionArgsNew (nuggetFunction *fn) {
    int size = 0;

    for (int i = 0; i < fn->inCount; ++i) {
        size += fn->ins[i]->size;
    }

    int insize = size;

    for (int i = 0; i < fn->outCount; ++i) {
        size += fn->outs[i]->size;
    }

    nuggetFunctionArgs *args = calloc(offsetof(nuggetFunctionArgs, data) + size, 1);
    args->fn = fn;
    args->insize = insize;

    return args;
}

void nuggetFunctionArgsFree (nuggetFunctionArgs *args) { free(args); }

char *inAddr (nuggetFunctionArgs *args, int i) {
    if (i < 0 || i >= args->fn->inCount) return 0;

    int p = 0;
    for (int j = 0; j < i; ++j) {
        p += args->fn->ins[j]->size;
    }

    return &args->data[p];
}

char *outAddr (nuggetFunctionArgs *args, int i) {
    if (i < 0 || i >= args->fn->outCount) return 0;

    int p = args->insize;
    for (int j = 0; j < i; ++j) {
        p += args->fn->outs[j]->size;
    }

    return &args->data[p];
}

void nuggetFunctionArgsReadInput (nuggetFunctionArgs *args, int i, void *addr) {
    void *src = inAddr(args, i);
    if (src == 0) return;
    memcpy(addr, src, args->fn->ins[i]->size);
}

void nuggetFunctionArgsReadOutput (nuggetFunctionArgs *args, int i, void *addr) {
    void *src = outAddr(args, i);
    if (src == 0) return;
    memcpy(addr, src, args->fn->outs[i]->size);
}

void nuggetFunctionArgsWriteInput (nuggetFunctionArgs *args, int i, void *data) {
    void *dst = inAddr(args, i);
    if (dst == 0) return;
    memcpy(dst, data, args->fn->ins[i]->size);
}

void nuggetFunctionArgsWriteOutput (nuggetFunctionArgs *args, int i, void *data) {
    void *dst = outAddr(args, i);
    if (dst == 0) return;
    memcpy(dst, data, args->fn->outs[i]->size);
}

