#ifndef __NUGGET_ARENA__
#define __NUGGET_ARENA__

#include <stdlib.h>
#include "collections/array.h"

typedef struct {
	col_Array *entries;
} Arena;

// valueSize: size in bytes (sizeof) of the values to be stored
// bucketCapacity: number of values each allocated bucket can hold
void ArenaInit (Arena *arena);
void *ArenaAlloc (Arena *arena, size_t size);
void ArenaReduce (Arena *arena, void (*fn)(void *value, void *acc), void *acc);

#endif //__NUGGET_ARENA__