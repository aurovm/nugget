#ifndef __NUGGET_MONITOR__
#define __NUGGET_MONITOR__

#include "collections/array.h"

// Flags

// Log every instruction
#define MONITOR_LOG 1
// Open shell after every instruction
#define MONITOR_PAUSE 2
// Profile function execution times
#define MONITOR_PROFILE 4

typedef struct {
    int flags;
    // File where the output (log and profiling) will be written.
    // If NULL, then everything is logget to stderr
    // FILE*, but requires stdio
    void *reportFile;

    // List of module prefixes to be monitored
    col_Array *prefixes;
} Monitor;

void MonitorInit(Monitor *monitor);

void MonitorSetFlagValue(Monitor *monitor, int flag, int value);
void MonitorSetFlag(Monitor *monitor, int flag);
void MonitorUnsetFlag(Monitor *monitor, int flag);
int MonitorGetFlag(Monitor *monitor, int flag);

void MonitorAddPrefix(Monitor *monitor, char *prefix);
int MonitorMatchesModule(Monitor *monitor, char *modname);

#endif