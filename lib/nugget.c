#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"
#include "gc.h"

char *_err = 0;
int typeid = 64;

char *nuggetError () { return _err; }
void setErr (const char* format, ...) {
    char *buf = malloc(strlen(format) + 256);

    va_list args;
    va_start( args, format );
    vsprintf( buf, format, args );
    va_end( args );

    _err = buf;
}

void pushErr (nuggetMachine *machine, const char* format, ...) {
    char *buf = malloc(strlen(format) + 256);

    va_list args;
    va_start( args, format );
    vsprintf( buf, format, args );
    va_end( args );

    pushLoadError(machine, buf, "<unknown>");
}

int nextTypeId () { return typeid++; }

typedef struct {
    char main[256];

    int count;
    struct { char* str; int matched; } segments[32];
} Name;

void parseName (Name *name, char *orig) {
    int len = strlen(orig);
    if (len > 255) {
        printf("Name exceeds capacity! %d\n", strlen);
        exit(1);
    }

    name->count = 0;
    strcpy(name->main, orig);

    // Replaces each separator with a NULL (c string ending character),
    // and stores the address of each segment in a list
    for (int i = 0; i < len; i++) {
        if (name->main[i] == '\x1d') {
            name->main[i] = 0;

            name->segments[name->count].str = &name->main[i + 1];
            name->segments[name->count].matched = 0;
            name->count++;

            if (name->count > 32) {
                printf("Name segment count exceeds capacity! %d\n", name->count);
                exit(1);
            }
        }
    }
}

void printName (Name *name) {
    printf("%s", name->main);
    for (int i = 0; i < name->count; ++i) {
        printf(".%s", name->segments[i]);
    }
}

int nameMatch (char *_query, char *_name) {
    Name query, name;
    parseName(&query, _query);
    parseName(&name, _name);

    if (strcmp(query.main, name.main) != 0) {
        return 0;
    }

    // Impossible to succeed if the query has more segments than name can match
    if (query.count > name.count) {
        return 0;
    }

    // Go over each segment of the query
    for (int i = 0; i < query.count; i++) {
        // Compare with each segment of the name that has not matched a previous segment of the query
        for (int j = 0; j < name.count; j++) {
            if (name.segments[j].matched) continue;

            int match = strcmp(query.segments[i].str, name.segments[j].str) == 0;
            if (match) {
                name.segments[j].matched = 1;
                goto cont;
            }
        }

        // If the loop ends, the query segment had no match, which fails the whole match
        return 0;

        cont:
    }

    return query.count == name.count ? EXACT_MATCH : INEXACT_MATCH;
}

nuggetItem nuggetModuleGetItem (nuggetModule* module, char *name) {
    if (!module->getItem) {
        printf("Module %s does not get items\n", module->name);
        return EMPTY_ITEM;
    }
    return (module->getItem)(module, name);
}

int nuggetFunctionGetInputCount (nuggetFunction *fn) { return fn->inCount; }
nuggetType *nuggetFunctionGetInput (nuggetFunction *fn, int i) {
    if (i < 0 || i >= fn->inCount) return 0;
    return fn->ins[i];
}

int nuggetFunctionGetOutputCount (nuggetFunction *fn) { return fn->outCount; }
nuggetType *nuggetFunctionGetOutput (nuggetFunction *fn, int i) {
    if (i < 0 || i >= fn->outCount) return 0;
    return fn->outs[i];
}


void nuggetPrintModule (nuggetModuleDesc *mod) {
    if (mod == 0) {
        printf("NULL\n");
        return;
    }

    printf("Modules:\n");
    for (int i = 0; i < mod->moduleCount; i++) {
        nuggetModuleItem *item = &mod->modules[i];
        switch (item->kind) {
            case nuggetModuleItemImport:
                printf("\t%d: Import(%s)\n", i+1, item->data.import);
                break;
            case nuggetModuleItemDefine:
                printf("\t%d: Define:\n", i+1);
                for (int j = 0; j < item->data.define.count; j++) {
                    nuggetDefineItemDesc inner = item->data.define.items[j];
                    char *group;
                    switch (inner.kind) {
                        case nuggetDefineModuleItem: group = "Modules"; break;
                        case nuggetDefineTypeItem: group = "Types"; break;
                        case nuggetDefineFunctionItem: group = "Functions"; break;
                        default: group = "INVALID"; break;
                    }
                    printf("\t\t%s: %s[%d]\n", inner.name, group, inner.index);
                }
                break;
            case nuggetModuleItemBuild:
                printf("\t%d: Build Modules[%d](Modules[%d])\n", i+1, item->data.build.base, item->data.build.argument);
                break;
            case nuggetModuleItemUse:
                printf("\t%d: Use Modules[%d].%s\n", i+1, item->data.use.base, item->data.use.name);
                break;
            default:
                printf("\t%d: INVALID\n", i+1);
        }
    }

    printf("Types:\n");
    for (int i = 0; i < mod->typeCount; i++) {
        nuggetTypeItem *item = &mod->types[i];
        if (item->name == 0) {
            printf("\t%d: NULL\n", i);
        } else {
            printf("\t%d: %d . %s\n", i, item->module, item->name);
        }
    }

    printf("Functions:\n");
    for (int i = 0; i < mod->functionCount; i++) {
        nuggetFunctionItem *item = &mod->functions[i];

        printf("\t%d: ( ", i);
        for (int j = 0; j < item->inCount; j++) {
            printf("%d ", item->ins[j]);
        }
        printf("-> ");
        for (int j = 0; j < item->outCount; j++) {
            printf("%d ", item->outs[j]);
        }
        printf("):");
        switch (item->kind) {
            case nuggetFunctionEmpty:
                printf(" EMPTY\n");
                break;
            case nuggetFunctionCode:
                printf("\n");
                for (int j = 0; j < item->data.code.count; j++) {
                    nuggetInstDesc inst = item->data.code.code[j];
                    switch (inst.kind) {
                        case nuggetInstEnd:
                            printf("\t\tend");
                            for (int k = 0; k < inst.data.call.argCount; k++) {
                                printf(" %d", inst.data.call.args[k]);
                            }
                            printf("\n");
                            break;
                        case nuggetInstHlt:
                            printf("\t\thlt\n");
                            break;
                        case nuggetInstVar:
                            printf("\t\tvar\n");
                            break;
                        case nuggetInstDup:
                            printf("\t\tdup\n");
                            break;
                        case nuggetInstSet:
                            printf("\t\tset\n");
                            break;
                        case nuggetInstJmp:
                            printf("\t\tjmp\n");
                            break;
                        case nuggetInstJif:
                            printf("\t\tjif\n");
                            break;
                        case nuggetInstNif:
                            printf("\t\tnif\n");
                            break;
                        case nuggetInstCall:
                            printf("\t\tFunctions[%d]( ", inst.data.call.fn);
                            for (int k = 0; k < inst.data.call.argCount; k++) {
                                printf("%d ", inst.data.call.args[k]);
                            }
                            printf(")\n");
                            break;
                        default:
                            printf("\t\tINVALID\n");
                    }
                }
                break;
            case nuggetFunctionImport:
                printf(" %d . %s\n", item->data.import.module, item->data.import.name);
                break;
            case nuggetFunctionInt:
                printf(" int %d\n", item->data.num);
                break;
            case nuggetFunctionBin:
                printf(" bin ");
                for (int j = 0; j < item->data.bin.size; j++) {
                    printf("%x", item->data.bin.bytes[j]);
                }
                printf("\n");
                break;
            case nuggetFunctionConstCall:
                printf(" const call Functions[%d]( ", item->data.call.fn);
                for (int j = 0; j < item->data.call.paramCount; j++) {
                    printf("Functions[%d]() ", item->data.call.params[j]);
                }
                printf(")\n");
                break;
            default:
                printf(" INVALID\n");
        }
    }
}

extern void nuggetMachinePrintErrors (nuggetMachine *machine) {
    if (machine->err == 0) {
        printf("No errors.");
    } else {
        ErrorFrame *err = machine->err;

        while (err != 0) {
            printf("%s\n", err->msg);
            err = err->prev;
        }
    }
}

extern int nuggetAllocatedMemory (nuggetMachine *machine) {
    return gc_totalAllocated();
}