#include "function.h"

nuggetFunction *nuggetFunctionAlloc (nuggetMachine *machine, size_t size) {
	return ArenaAlloc(&machine->functionArena, size);
}