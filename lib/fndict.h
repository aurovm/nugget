typedef struct {
    int inCount;
    int outCount;
    nuggetType **ins;
    nuggetType **outs;
} Signature;

void SignaturePrint (Signature a);
int SignatureEq (Signature a, Signature b);
int SignatureFromModule (nuggetModule *mod, Signature *tgt);
Signature SignatureFromFunction (nuggetFunction *fn);

// Buffer must be at least `inCount + outCount` long
Signature SignatureTmp (nuggetType **tbuf, int inCount, int outCount, ...);


typedef struct FnDict FnDict;

FnDict *FnDictNew ();
FnDict *FnDictGlobal ();

nuggetType *FnDictGet (FnDict *dict, Signature sig);