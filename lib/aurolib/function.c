#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "headers.h"

#include "../fndict.h"

typedef struct {
    nuggetModule _mod;
    nuggetModule *argument;
    nuggetType *type;
    Signature sig;
    nuggetModule *closure;
    int ready;
} FnMod;

typedef struct {
    nuggetModule _mod;
    FnMod* base;
    nuggetType *boundType;
    nuggetFunction *fn;
} ClosureMod;

typedef struct {
    ClosureMod *mod;
    void *value;
} ClosureInstance;

typedef struct {
    nuggetModule _mod;
    FnMod *base;
    nuggetFunction *fn;
} NewFnMod;



void newfn_get (stackFrame *frame, void *_m) {
    NewFnMod *mod = (NewFnMod*)_m;
    nuggetFunction *fn = mod->fn;
    //assert(fn != 0);
    stackFrameWriteOut(frame, &fn);
}

void fn_apply (stackFrame *frame, void *_m) {
    frame->error = 1;
}

nuggetItem getNewFnMod (nuggetModule *_m, char *name) {
    NewFnMod *mod = (NewFnMod*)_m;
    SCASE(name, "") return FITEM(BuiltinFunctionNew(newfn_get, "function.new", _m, 0, 1, mod->base->type));
    return EMPTY_ITEM;
}

nuggetModule *buildNewFnMod (nuggetModule *_self, nuggetModule *argument) {

    nuggetItem fItem = nuggetModuleGetItem(argument, "0");
    if (fItem.kind != nuggetItemFunction) {
        printf("function.new argument '0' is not a Function\n");
        return 0;
    }

    NewFnMod *base = _self;
    NewFnMod *mod = malloc(sizeof(NewFnMod));
    mod->_mod.getItem = getNewFnMod;
    mod->_mod.build = 0;
    mod->_mod.name = "function.new<?>";

    mod->base = base->base;
    mod->fn = fItem.item;

    assert(mod->fn != 0);

    return mod;
}

int FnModInit (FnMod *mod) {
    if (mod->ready) return 0;

    if (SignatureFromModule(mod->argument, &mod->sig)) {
        return 1;
    }

    FnDict *dict = FnDictGlobal();
    mod->type = FnDictGet(dict, mod->sig);
    return 0;
}

void closure_wrapper (stackFrame *frame, void *_m) {
    ClosureInstance *instance = (ClosureInstance*) _m;

    nuggetFunction *fn = instance->mod->fn; 

    // TODO: Check error
    nuggetFunctionArgs *args = nuggetFunctionArgsNew(fn);

    // TODO: Try to do this more abstractly. Do NOT copy bytes around.
    // I'm destructuring args because atm there's no way to connect stack frames
    // and args more abstractly.
    char *p = args->data;

    for (int i = 0; i < fn->inCount - 1; i++) {
        stackFrameReadArg(frame, p);
        p += fn->ins[0]->size;
    }

    // Write the bound argument
    nuggetValPtrCopy(
        // type, dst, src
        instance->mod->boundType,
        p, instance->value
    );

    p += instance->mod->boundType->size;

    nuggetFunctionCall(frame->machine, fn, args);
    if (frame->machine->state == machineStateError) {
        printf("ERROR: %s failed in closure.\n", fn->name);
        return;
    }

    assert(p == args->data + args->insize);

    // Same as above: don't traverse the pointer
    for (int i = 0; i < fn->outCount; i++) {
        // stackFrameWriteOut only works for one output. Panic if there's more
        assert(i == 0);
        stackFrameWriteOut(frame, p);
        p += fn->outs[0]->size;
    }
}

void closure_new (stackFrame *frame, void *_m) {
    ClosureMod *mod = (ClosureMod*) _m;

    ClosureInstance *instance = calloc(sizeof(ClosureInstance), 1);
    instance->mod = mod;
    instance->value = calloc(mod->boundType->size, 1);

    stackFrameReadArg(frame, instance->value);

    nuggetFunction *modfn = mod->fn;
    nuggetFunction *fn = BuiltinFunctionNew(closure_wrapper, "function.closure", instance, 0, 0);

    // TODO: Doesn't belong here. This is the same as BuiltinFunctionNew does
    int inCount = modfn->inCount - 1;
    nuggetType **types = calloc(sizeof(void*), inCount + modfn->outCount);

    for (int i = 0; i < inCount; i++) {
        types[i] = modfn->ins[i];
    }

    for (int i = 0; i < modfn->outCount; i++) {
        types[inCount + i] = modfn->outs[i];
    }

    fn->inCount = inCount;
    fn->outCount = modfn->outCount;
    fn->ins = types;
    fn->outs = &types[inCount];

    stackFrameWriteOut(frame, &fn);
}

nuggetItem getClosureMod (nuggetModule *_m, char *name) {
    ClosureMod *mod = (FnMod*)_m;
    SCASE(name, "new") return FITEM(BuiltinFunctionNew(
        closure_new, "function.closure.new", _m, 1, 1, mod->boundType, mod->base->type
    ));

    return EMPTY_ITEM;
}

nuggetModule *buildClosureMod (nuggetModule *_self, nuggetModule *argument) {
    nuggetItem fItem = nuggetModuleGetItem(argument, "0");
    if (fItem.kind != nuggetItemFunction) {
        printf("function.new argument '0' is not a Function\n");
        return 0;
    }

    // TODO: Check signature against base function

    ClosureMod *base = _self;
    ClosureMod *mod = calloc(sizeof(ClosureMod), 1);
    mod->_mod.getItem = getClosureMod;
    mod->_mod.build = 0;
    mod->_mod.name = "function.closure<?>";
    mod->_mod.machine = _self->machine;

    mod->base = base->base;
    mod->fn = fItem.item;
    mod->boundType = mod->fn->ins[mod->fn->inCount - 1];

    return mod;
}

nuggetItem getFnModule (nuggetModule *_m, char *name) {
    FnMod *mod = (FnMod*)_m;

    SCASE(name, "") return (nuggetItem) { kind: nuggetItemType, item: mod->type };
    SCASE(name, "new") {
        NewFnMod *imod = malloc(sizeof(NewFnMod));
        imod->_mod.getItem = 0;
        imod->_mod.build = buildNewFnMod;
        imod->base = mod;
        return (nuggetItem) { kind: nuggetItemModule, item: imod };
    }
    SCASE(name, "apply") {
        Signature sig = mod->sig;

        nuggetType **types = malloc(sizeof(void*) * (sig.inCount + sig.outCount + 1));

        types[0] = mod->type;
        for (int i = 0; i < sig.inCount; ++i) { types[i+1] = sig.ins[i]; }
        for (int i = 0; i < sig.outCount; ++i) { types[sig.inCount+i+1] = sig.outs[i]; }

        nuggetFunction *fn = malloc(sizeof(nuggetFunction));
        fn->kind = nuggetFunctionApply;
        fn->name = "function.apply";
        fn->inCount = sig.inCount + 1;
        fn->outCount = sig.outCount;
        fn->ins = &types[0];
        fn->outs = &types[sig.inCount+1];

        return FITEM(fn);
    }
    SCASE(name, "closure") {
        if (!mod->closure) {
            NewFnMod *imod = calloc(sizeof(NewFnMod), 1);
            imod->_mod.build = buildClosureMod;
            imod->_mod.machine = mod->_mod.machine;
            imod->base = mod;

            mod->closure = imod;
        }
        return (nuggetItem) { kind: nuggetItemModule, item: mod->closure };
    }

    return EMPTY_ITEM;
}

nuggetModule *buildFnModule (nuggetModule *_self, nuggetModule *argument) {
    FnMod *mod = calloc(sizeof(FnMod), 1);
    mod->_mod.getItem = getFnModule;
    mod->_mod.build = 0;

    mod->type = nuggetTypeNew(0, sizeof(void*));
    mod->type->flags |= NUGGET_TYPE_FLAG_COPY;

    mod->_mod.name = malloc(20);
    sprintf(mod->_mod.name, "auro.function<&%d>", mod->type->id);
    mod->type->name = mod->_mod.name + 5;

    mod->_mod.machine = _self->machine;
    mod->argument = argument;
    mod->ready = 0;

    if (FnModInit(mod)) {
        return 0;
    }

    return mod;
}

nuggetModule *newFunctionModule (nuggetMachine *machine /*TODO*/) {
    nuggetModule *mod = calloc(sizeof(nuggetModule), 1);
    mod->getItem = 0;
    mod->build = buildFnModule;
    mod->machine = machine;
    mod->name = "auro.function";
    return mod;
}