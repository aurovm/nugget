#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "headers.h"
#include "../gc.h"
#include "../value.h"
#include "../collections/array.h"

typedef struct {
    nuggetType *a;
    nuggetType *b;
} TypePair;

DEFINE_ARRAY(typeList, nuggetType*)
DEFINE_ARRAY(pairList, TypePair)
DEFINE_ARRAY_SEARCH(typeList, a, b, a == b)
DEFINE_ARRAY_SEARCH(pairList, a, b, a.a == b.a && a.b == b.b)

typedef struct {
    nuggetModule _mod;
    Hashmap *map;
    // Types observed for recursive Mu-equality
    typeList muList;
    // Mu-equality claims
    pairList muClaims;
    // Mu-equality facts
    pairList muFacts;
} RecordBase;

typedef struct {
    nuggetType *type;
    nuggetFunction *get;
    nuggetFunction *set;
    int offset;
    char name[4];
} RecordField;

typedef struct {
    nuggetModule _mod;

    nuggetModule *argument;
    Hashmap *map;
    RecordBase* baseMod;

    nuggetType *base;
    nuggetType *type;

    nuggetFunction *new;

    RecordField *fields;
    int fieldCount;
    int size;

    enum { RECORD_NEW, RECORD_READY, RECORD_NO_FIELDS, RECORD_FIELDS, RECORD_ERROR } state;
} RecordMod;


void record_new (stackFrame *frame, void *_m) {
    RecordMod* mod = _m;
    char *body = gc_alloc(mod->type, mod->size);

    for (int i = 0; i < mod->fieldCount; i++) {
        RecordField field = mod->fields[i];
        nuggetType *type = field.type;
        char *dst = &body[field.offset];

        stackFrameReadArg(frame, dst);
    }

    void *valptr = &body;
    stackFrameWriteOut(frame, valptr);
}

void record_field_get (stackFrame *frame, void *_m) {
    RecordField* field = _m;
    READ(char*, body);

    char *src = &body[field->offset];
    stackFrameWriteOut(frame, src);
}

void record_field_set (stackFrame *frame, void *_m) {
    RecordField* field = _m;
    READ(char*, body);

    nuggetType *tp = field->type;
    char *dst = &body[field->offset];

    nuggetValPtrDrop(tp, dst);
    stackFrameReadArg(frame, dst);
}

void record_drop (nuggetMachine *machine, void *value, void *data) {
    char *body = value;
    RecordMod *mod = data;

    for (int i = 0; i < mod->fieldCount; i++) {
        nuggetType *type = mod->fields[i].type;
        char *valptr = &body[mod->fields[i].offset];

        nuggetValPtrDrop(type, valptr);
    }
}

int RecordModInit (RecordMod *mod) {
    if (mod->state != RECORD_NEW) return 0;

    mod->state = RECORD_NO_FIELDS;

    nuggetType *types[64];
    int count = 0;

    while (1) {

        if (count >= 64) {
            printf("OUT OF BOUNDS! record field count\n");
            return 1;
        }

        char sindex[4];
        sprintf(sindex, "%d", count);
        nuggetItem baseItem = nuggetModuleGetItem(mod->argument, sindex);
        if (baseItem.kind == nuggetItemEmpty) { break; }
        if (baseItem.kind != nuggetItemType) {
            char *msg = 0;
            if (baseItem.kind != nuggetItemError) {
                char *msg = malloc(43);
                sprintf(msg, "Record argument '%d' is not a type", count);
            }
            pushLoadError(mod->_mod.machine, msg, "record module");
            mod->state = RECORD_ERROR;
            return 1;
        }

        types[count++] = baseItem.item;
    }

    // Only create with out type, we build the input types in the field loop
    mod->new = BuiltinFunctionNew(record_new, "record.new", mod, 0, 1, mod->type);
    mod->new->ins = malloc(sizeof(nuggetType*) * count);
    mod->new->inCount = count;

    RecordField *fields = malloc(sizeof(RecordField) * count);
    int size = 0;

    int namesize = 0;

    for (int i = 0; i < count; ++i) {
        nuggetType *type = types[i];

        mod->new->ins[i] = type;

        fields[i].type = type;
        fields[i].get = BuiltinFunctionNew(record_field_get, "record.get", &fields[i], 1, 1, mod->type, type);
        fields[i].set = BuiltinFunctionNew(record_field_set, "record.set", &fields[i], 2, 0, mod->type, type);
        fields[i].offset = size;
        sprintf(fields[i].name, "%d", i);

        size += type->size;
        namesize += type->name ? strlen(type->name) + 1 : 1;
    }

    mod->fields = fields;
    mod->fieldCount = count;
    mod->size = size;

    // Allow hashing
    mod->state = RECORD_FIELDS;

    nuggetType **entry = HashmapGetPtr(mod->map, mod);
    if (*entry != 0) {
        mod->type->id = (*entry)->id;
        mod->type->name = (*entry)->name;
        mod->state = RECORD_READY;
        return 0;
    }

    // Name :D
    free(mod->_mod.name);
    mod->_mod.name = malloc(strlen("auro.record<>") + namesize + 1);
    strcpy(mod->_mod.name, "auro.record<");
    for (int i = 0; i < count; ++i) {
        if (i > 0) strcat(mod->_mod.name, ",");
        if (types[i]->name)
            strcat(mod->_mod.name, types[i]->name);
    }
    strcat(mod->_mod.name, ">");
    mod->type->name = mod->_mod.name + 5;

    *entry = mod->type;

    mod->state = RECORD_READY;
    return 0;
}


nuggetItem recordItemGetter (nuggetModule *_m, char *name) {
    RecordMod *mod = (RecordMod*)_m;

    // Ignore if it doesn't initialize. It's okay to return a half type
    // as long as nobody uses the constructor, getters or setters
    if (RecordModInit(mod)) {
        if (mod->state == RECORD_ERROR) return ERROR_ITEM;
    }

    SCASE(name, "") {
        return (nuggetItem) { kind: nuggetItemType, item: mod->type };
    }
    SCASE(name, "new") {
        if (RecordModInit(mod)) return ERROR_ITEM;
        return FITEM(mod->new);
    }

    int isget = strncmp(name, "get", 3) == 0;
    int isset = strncmp(name, "set", 3) == 0;

    if (isget || isset) {
        if (RecordModInit(mod)) return ERROR_ITEM;
        for (int i = 0; i < mod->fieldCount; ++i) {
            RecordField *field = &mod->fields[i];

            if (strcmp(name+3, field->name) == 0) {
                return FITEM(isget ? field->get : field->set);
            }
        }
    }

    return EMPTY_ITEM;
}

nuggetModule *buildRecordModule (nuggetModule *_self, nuggetModule *argument) {

    // printf("Create record with %d fields\n", count);
    //int tid = nextTypeId();

    RecordMod *mod = malloc(sizeof(RecordMod));
    mod->_mod.getItem = recordItemGetter;
    mod->_mod.build = 0;
    mod->_mod.machine = _self->machine;

    mod->type = nuggetTypeNew(0, sizeof(void*));
    mod->type->flags = NUGGET_TYPE_FLAG_GC | NUGGET_TYPE_FLAG_RECORD;
    mod->type->data = mod;
    mod->type->drop = record_drop;

    mod->_mod.name = malloc(18);
    sprintf(mod->_mod.name, "auro.record<&%d>", mod->type->id);
    mod->type->name = mod->_mod.name + 5;

    mod->baseMod = (RecordBase*) _self;
    mod->map = ((RecordBase*) _self)->map;
    mod->argument = argument;
    mod->state = RECORD_NEW;
}

int RecordHash (RecordMod *mod) {
    if (mod->state != RECORD_READY && mod->state != RECORD_FIELDS) return 0;
    int hash = 0;
    for (int i = 0; i < mod->fieldCount; ++i) {
        hash *= 31;
        hash += mod->fields[i].type->id;
    }
    return hash;
}

typedef struct {
    // if (!pair.a && !pair.b) this is not a recursive pair (or it is a result)
    // if (pair.a && !pair.b) type a is in the muList
    // if (pair.a && pair.b) the claim is in the list
    TypePair pair;
    int isFact;
} MuStatus;

MuStatus searchRecursive (RecordBase *baseMod, nuggetType *a, nuggetType *b) {
    // TODO: Only consider records known to be recursive (Track them)
    // if (!(a->flags & b->flags & NUGGET_TYPE_FLAG_MU)) return;

    // Recursive types get compared multiple times (because they are recursive)
    // Each time the record is compared again, this function will advance to the next step.
    // The steps don't show up in order in the function body

    MuStatus status = (MuStatus){
        .pair = (TypePair){0, 0},
        .isFact = 0
    };

    TypePair pair = (TypePair){a, b};

    // Step 4
    if (pairListFindIndex(baseMod->muFacts, pair) >= 0) {
        status.isFact = 1;
        return;
    }

    // Step 1: mark the type as non recursively compared.
    // TODO: Search B in the muList as well
    // TODO: This step might be unnecessary if the types are already known to be recursive.
    int typeIndex = typeListFindIndex(baseMod->muList, pair.a);

    if (typeIndex >= 0) {
        int claimIndex = pairListFindIndex(baseMod->muClaims, pair);
        if (claimIndex >= 0) {
            pairListPush(baseMod->muFacts, pair);
            status.isFact = 1;
        } else {
            // Step 2: This means this comparison is the back-link in the type graph
            // We store this equality
            pairListPush(baseMod->muClaims, pair);
            status.pair = pair;
        }
    } else {
        typeListPush(baseMod->muList, pair.a);
        status.pair.a = pair.a;
    }

    return status;
}

void clearMuStatus (RecordBase *baseMod, MuStatus status) {
    if (status.pair.a) {
        if (status.pair.b) {
            int index = pairListFindIndex(baseMod->muClaims, status.pair);
            pairListRemove(baseMod->muClaims, index);
        } else {
            int index = typeListFindIndex(baseMod->muList, status.pair.a);
            typeListRemove(baseMod->muList, index);
        }
    }
}

int RecordEq (RecordMod *a, RecordMod *b) {
    // TODO: Is it really necessary to dismiss these cases?
    if (a->state != RECORD_READY && a->state != RECORD_FIELDS) {return 0;}
    if (b->state != RECORD_READY && b->state != RECORD_FIELDS) {return 0;}
    if (a->fieldCount != b->fieldCount) return 0;

    RecordBase *baseMod = a->baseMod;
    // assert(b->baseMod == baseMod);

    int result = 0;

    // MuStatus objects need to be dropped
    // For now lets consider all records are potentially recursive.
    // This should be optimized away
    MuStatus muStatusA = searchRecursive(baseMod, a->type, b->type);

    // TODO: Search B also

    // No need to compare fields if this equality has been seen before (recursive)
    result = muStatusA.isFact;
    if (result) goto cleanup;

    for (int i = 0; i < a->fieldCount; ++i) {
        if (!nuggetTypeEq(a->fields[i].type, b->fields[i].type)) {
            result = 0;
            goto cleanup;
        }
    }
    result = 1;

    cleanup:
    clearMuStatus(baseMod, muStatusA);
    return result;
}

nuggetModule *newRecordModule (nuggetMachine *machine) {
    RecordBase *mod = malloc(sizeof(RecordBase));
    mod->_mod.getItem = 0;
    mod->_mod.build = buildRecordModule;
    mod->_mod.machine = machine;
    mod->map = HashmapNew((HashmapHash) RecordHash, (HashmapEqual) RecordEq);
    mod->muList = typeListNew(32);
    mod->muClaims = pairListNew(4);
    mod->muFacts = pairListNew(4);
    return mod;
}

int recordTypeEquals (nuggetType *a, nuggetType *b) {
    RecordMod *amod = a->data;
    RecordMod *bmod = b->data;
    return RecordEq(amod, bmod);
}