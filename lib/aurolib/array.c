#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"
#include "../gc.h"
#include "../value.h"

typedef struct {
    int count;
    char *data;
} Array;

typedef struct {
    nuggetModule _mod;
    nuggetType *base;
    nuggetType *array;

    nuggetFunction *new;
    nuggetFunction *get;
    nuggetFunction *set;
    nuggetFunction *len;
} ArrayMod;



typedef struct ArrayEntry {
    nuggetType *tp;
    ArrayMod *mod;
    struct ArrayEntry *next;
} ArrayEntry;

typedef struct {
    nuggetModule _mod;
    ArrayEntry *entries;
    nuggetTypeFamily *family;
} ArrayBase;

ArrayMod *findEntry (ArrayEntry *entry, nuggetType *tp) {
    while (entry) {
        if (entry->tp == tp) return entry->mod;
        entry = entry->next;
    }
    return 0;
}

ArrayMod *addEntry (ArrayEntry **entry, nuggetType *tp, ArrayMod *mod) {
    ArrayEntry *next = malloc(sizeof(ArrayEntry));
    next->tp = tp;
    next->mod = mod;
    next->next = *entry;
    *entry = next;
}



void arr_new (stackFrame *frame, void* _mod) {
    ArrayMod *mod = _mod;
    nuggetType *type = mod->base;

    char *init = &frame->data[frame->program[frame->pc++]];
    READ(int, count)

    char *data = malloc(type->size * count);

    for (int i = 0; i < count; i++) {
        void *dst = &data[i * type->size];
        nuggetValPtrCopy(type, dst, init);
    }

    Array *arr = gc_alloc(mod->array, sizeof(Array));
    arr->count = count;
    arr->data = data;

    void *valptr = &arr;
    stackFrameWriteOut(frame, valptr);
}

void arr_get (stackFrame *frame, void *_mod) {
    ArrayMod *mod = _mod;
    nuggetType *type = mod->base;

    READ(Array*, arr)
    READ(int, index)

    if (index < 0 || index >= arr->count) {
        frame->error = 1;
        nuggetPushErrorF(frame->machine, 0, "Index %d is out of bounds for array with size %d", index, arr->count);
        return;
    }

    void *val = &arr->data[index * type->size];
    writeOut(frame, val);
}

void arr_set (stackFrame *frame, void *_mod) {
    ArrayMod *mod = _mod;
    nuggetType *type = mod->base;

    READ(Array*, arr)
    READ(int, index)

    if (index < 0 || index >= arr->count) {
        setErr("Index %d is out of bounds for array with size %d\n", index, arr->count);
        frame->error = 1;
        return;
    }

    void *dst = &arr->data[index * type->size];
    dropVal(dst, type);

    // Reads the passed argument into the array
    readArg(frame, dst);
}

void arr_len (stackFrame *frame, void *_mod) {
    ArrayMod *mod = _mod;
    READ(Array*, arr)
    int len = arr->count;
    WRITE(len)
}

void arr_drop (nuggetMachine *machine, void *value, void *data) {
    ArrayMod *mod = data;
    Array *arr = value;

    nuggetType *type = mod->base;

    for (int i = 0; i < arr->count; i++) {
        char *valptr = &arr->data[i * type->size];
        nuggetValPtrDrop(type, valptr);
    }
}



nuggetItem arrayItemGetter (nuggetModule *_m, char *name) {
    ArrayMod *mod = (ArrayMod*) _m;
    SCASE(name, "") return (nuggetItem) { kind: nuggetItemType, item: mod->array };
    SCASE(name, "get") return FITEM(mod->get);
    SCASE(name, "set") return FITEM(mod->set);
    SCASE(name, "new") return FITEM(mod->new);
    SCASE(name, "len") return FITEM(mod->len);

    return EMPTY_ITEM;
}

nuggetModule *buildArrayModule (nuggetModule *_self, nuggetModule *argument) {
    ArrayBase *baseMod = (ArrayBase*) _self;

    nuggetItem baseItem = nuggetModuleGetItem(argument, "0");
    if (baseItem.kind != nuggetItemType) {
        printf("No type item '0' in array module argument\n");
        return 0;
    }

    nuggetType *base = baseItem.item;

    ArrayMod *mod = findEntry(baseMod->entries, base);
    if (mod) { return (nuggetModule*) mod; }

    mod = malloc(sizeof(ArrayMod));
    mod->_mod.getItem = arrayItemGetter;
    mod->_mod.build = 0;
    mod->_mod.name = "auro.array<?>";

    mod->base = base;

    mod->array = nuggetTypeNew("array<?>", sizeof(void*));
    mod->array->flags = NUGGET_TYPE_FLAG_GC;
    mod->array->drop = arr_drop;
    mod->array->data = mod;
    mod->array->family = 

    mod->new = BuiltinFunctionNew(arr_new, "array.new", mod, 2, 1, base, &int_t, mod->array);
    mod->get = BuiltinFunctionNew(arr_get, "array.get", mod, 2, 1, mod->array, &int_t, base);
    mod->set = BuiltinFunctionNew(arr_set, "array.set", mod, 3, 0, mod->array, &int_t, base);
    mod->len = BuiltinFunctionNew(arr_len, "array.len", mod, 1, 1, mod->array, &int_t);

    addEntry(&baseMod->entries, base, mod);

    return (nuggetModule*) mod;
}

int arrayFamilyEq (nuggetType *a, nuggetType *b, void *data) {
    ArrayBase *mod = data;
    ArrayMod *modA = a->data;
    ArrayMod *modB = b->data;

    return nuggetTypeEq(modA->base, modB->base);
}

nuggetModule *newArrayModule (nuggetMachine *machine) {
    ArrayBase *mod = malloc(sizeof(ArrayBase));
    mod->_mod.getItem = 0;
    mod->_mod.build = buildArrayModule;
    mod->_mod.machine = machine;
    mod->entries = 0;
    mod->family = malloc(sizeof(nuggetTypeFamily));

    mod->family->userData = mod;
    mod->family->eq = arrayFamilyEq;
    return mod;
}