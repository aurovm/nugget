#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"
#include "../gc.h"
#include "../value.h"

typedef struct {
    nuggetModule _mod;

    nuggetModule *argument;

    nuggetType *base;
    nuggetType *type;
} ShellMod;


void shell_new (stackFrame *frame, void *_m) {
    ShellMod *mod = _m;
    nuggetType *type = mod->base;

    void *shell = gc_alloc(mod->type, type->size);
    stackFrameReadArg(frame, shell);

    writeRef(frame, shell);
}

void shell_get (stackFrame *frame, void *_m) {
    ShellMod *mod = _m;
    nuggetType *type = mod->base;

    READ(void*, shell)
    stackFrameWriteOut(frame, shell);
}

void shell_drop (nuggetMachine *machine, void *shellptr, void *data) {
    ShellMod *mod = data;
    nuggetType *type = mod->base;

    void *innerptr = shellptr;
    nuggetValPtrDrop(type, innerptr);
}


nuggetItem shellItemGetter (nuggetModule *_m, char *name) {
    ShellMod *mod = (ShellMod*)_m;

    SCASE(name, "") return (nuggetItem) { kind: nuggetItemType, item: mod->type };

    if (!mod->base) {
        nuggetItem baseItem = nuggetModuleGetItem(mod->argument, "0");
        if (baseItem.kind != nuggetItemType) {
            char *msg = 0;
            if (baseItem.kind == nuggetItemError) {
                msg = "argument item '0' is not a type";
            }
            pushLoadError(mod->_mod.machine, msg, "auro.shell");
            return ERROR_ITEM;
        }

        nuggetType *base = baseItem.item;
        mod->base = base;
    }

    SCASE(name, "new") return FITEM(BuiltinFunctionNew(shell_new, "typeshell.new", mod, 1, 1, mod->base, mod->type));
    SCASE(name, "get") return FITEM(BuiltinFunctionNew(shell_get, "typeshell.get", mod, 1, 1, mod->type, mod->base));
    return EMPTY_ITEM;
}

nuggetModule *buildShellModule (nuggetModule *_self, nuggetModule *argument) {
    ShellMod *mod = malloc(sizeof(ShellMod));
    mod->_mod.getItem = shellItemGetter;
    mod->_mod.build = 0;
    //mod->_mod.name = "auro.shell<?>";

    // If we try to evaluate the base type here, and it is recursive,
    // we will enter an infinite loop.
    // The base type evaluation must be defered
    mod->argument = argument;
    mod->base = 0;

    // I would like if the shell were the same size as the base type,
    // so that it could be stored directly instead of allocating.
    // But that would give an incomplete type and auro would have
    // problems with that.
    // Particularly, I'm having an issue where a record type containing
    // an incomplete shell has the incorrect size because the size of
    // the shell is updated after..
    mod->type = nuggetTypeNew(0, sizeof(void*));
    mod->type->flags = NUGGET_TYPE_FLAG_UNNAMED | NUGGET_TYPE_FLAG_GC;
    mod->type->drop = shell_drop;
    mod->type->data = mod;
    //mod->type->name = "shell<?>";

    mod->_mod.name = malloc(18);
    sprintf(mod->_mod.name, "auro.shell<&%d>", mod->type->id);
    mod->type->name = mod->_mod.name + 5;


    return (nuggetModule*) mod;
}

nuggetModule *newShellModule (nuggetMachine *machine /*TODO*/) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = 0;
    mod->build = buildShellModule;
    mod->name = "auro.shell";
    return mod;
}