#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"
#include "../gc.h"
#include "../value.h"

typedef struct {
    nuggetModule _mod;
    nuggetType *base;
    nuggetType *type;

    nuggetFunction *new;
    nuggetFunction *null;
    nuggetFunction *get;
    nuggetFunction *isnull;
} NullMod;


void null_new (stackFrame *frame, void *_m) {
    NullMod *mod = _m;
    nuggetType *type = mod->base;

    // We read the return address first,
    // so we'll have to increment it later.
    int retaddr = frame->program[frame->pc + 1];

    char *tgt = &frame->data[retaddr];

    if (stackFrameGetRegFlag(frame, retaddr)) {
        if (tgt[0]) {
            // Free the previous value if the flag was set
            nuggetValPtrDrop(type, tgt+1);
        }
    }

    // Flag the null as populated
    tgt[0] = 1;

    // Read the base value into the (stack allocated) null
    readArg(frame, tgt+1);
    stackFrameSetRegFlag(frame, retaddr, 1);

    // Skip the output address as it was already redd
    frame->pc++;
}

void null_get (stackFrame *frame, void *_m) {
    NullMod *mod = _m;
    nuggetType *type = mod->base;

    char *value = &frame->data[frame->program[frame->pc++]];

    if (*value == 0) {
        frame->error = 1;
        nuggetPushErrorF(frame->machine, 34, "Tried to dereference a null value");
        return;
    }

    if (*value != 1) {
        frame->error = 1;
        nuggetPushErrorF(frame->machine, 34, "Null flag is invalid: %d", *value);
        return;
    }

    writeOut(frame, value+1);
}

void null_null (stackFrame *frame, void *_m) {
    NullMod *mod = _m;
    nuggetType *type = mod->base;

    int retaddr = frame->program[frame->pc++];

    frame->data[retaddr] = 0;
    stackFrameSetRegFlag(frame, retaddr, 1);

    // TODO: Write zeros
    //memcpy(&frame->data[retaddr + 1], value, type->size);
}

void null_isnull (stackFrame *frame, void *_m) {
    NullMod *mod = _m;

    READ(char, flag)

    char result;

    switch (flag) {
        case 0: result = 1; break;
        case 1: result = 0; break;
        default:
            frame->error = 1;
            nuggetPushErrorF(frame->machine, 34, "Null flag is invalid: %d", flag);
            return;
    }

    WRITE(result)
}

void null_drop (nuggetMachine *machine, void *valptr, void *data) {
    NullMod *mod = data;
    nuggetType *type = mod->base;

    char *value = valptr;
    if (value[0]) {
        nuggetValPtrDrop(type, value+1);
    }
}

void null_clone (nuggetMachine *machine, void *valptr, void *data) {
    NullMod *mod = data;
    nuggetType *type = mod->base;

    char *value = valptr;
    if (value[0]) {
        // Copy to itself to incref
        nuggetValPtrCopy(type, value+1, value+1);
    }
}



nuggetItem nullItemGetter (nuggetModule *_m, char *name) {
    NullMod *mod = (NullMod*) _m;
    SCASE(name, "") return (nuggetItem) { kind: nuggetItemType, item: mod->type };
    SCASE(name, "get") return FITEM(mod->get);
    SCASE(name, "new") return FITEM(mod->new);
    SCASE(name, "null") return FITEM(mod->null);
    SCASE(name, "isnull") return FITEM(mod->isnull);

    return EMPTY_ITEM;
}

nuggetModule *buildNullModule (nuggetModule *_self, nuggetModule *argument) {
    nuggetItem baseItem = nuggetModuleGetItem(argument, "0");
    if (baseItem.kind != nuggetItemType) {
        printf("No type item '0' in null module argument\n");
        return 0;
    }

    nuggetType *base = baseItem.item;

    DictModule *dict = (DictModule*) _self;
    nuggetModule *raw = InstanceDictGet(&dict->dict, base);
    if (raw) { return raw; }

    NullMod *mod = malloc(sizeof(NullMod));
    mod->_mod.getItem = nullItemGetter;
    mod->_mod.build = 0;

    mod->base = base;

    mod->type = nuggetTypeNew(0, base->size + 1);
    mod->type->name = malloc(7 + strlen(base->name));
    mod->type->flags = NUGGET_TYPE_FLAG_NULL;
    mod->type->drop = null_drop;
    mod->type->clone = null_clone;
    mod->type->data = mod;
    sprintf(mod->type->name, "null<%s>", base->name);


    mod->null = BuiltinFunctionNew(null_null, "null.null", mod, 0, 1, mod->type);
    mod->new = BuiltinFunctionNew(null_new, "null.new", mod, 1, 1, base, mod->type);
    mod->get = BuiltinFunctionNew(null_get, "null.get", mod, 1, 1, mod->type, base);
    mod->isnull = BuiltinFunctionNew(null_isnull, "null.isnull", mod, 1, 1, mod->type, &bool_t);

    raw = (nuggetModule*) mod;
    InstanceDictSet(&dict->dict, base, raw);

    return raw;
}

nuggetModule *newNullModule (nuggetMachine *machine /*TODO*/) {
    nuggetModule *mod = malloc(sizeof(DictModule));
    mod->getItem = 0;
    mod->build = buildNullModule;

    ((DictModule*) mod)->dict = InstanceDictNew();

    return mod;
}

int nullTypeEquals (nuggetType *a, nuggetType *b) {
    NullMod *amod = a->data;
    NullMod *bmod = b->data;
    return nuggetTypeEq(amod->base, bmod->base);
}