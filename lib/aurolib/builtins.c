#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

#include "headers.h"
#include "../gc.h"

typedef nuggetItem (*getter) (char*);


typedef struct {
    nuggetModule _mod;
    getter getter;
} BuiltinMod;

nuggetFunction *BuiltinFunctionNew (void (*impl)(stackFrame*, void*), char *name, void* data, int inCount, int outCount, ...) {
    int tcount = inCount + outCount;
    nuggetType **types = malloc(sizeof(void*) * tcount);

    va_list args;
    va_start( args, outCount );
    for (int i = 0; i < tcount; i++) {
        types[i] = va_arg(args, nuggetType*);
    }
    va_end( args );

    BuiltinFunction *fn = malloc(sizeof(BuiltinFunction));
    fn->_fn.kind = nuggetFunctionBuiltin;
    fn->_fn.name = name;
    fn->_fn.inCount = inCount;
    fn->_fn.outCount = outCount;

    fn->_fn.ins = &types[0];
    fn->_fn.outs = &types[inCount];

    fn->impl = impl;
    fn->data = data;

    return (nuggetFunction*) fn;
}

#define COPY NUGGET_TYPE_FLAG_COPY

// Available ids up to 63
DECL_TYPE(int, 1, int, COPY);
DECL_TYPE(bool, 2, char, COPY);
DECL_TYPE(buffer, 3, void*, COPY);
DECL_TYPE(string, 4, void*, NUGGET_TYPE_FLAG_GC);
DECL_TYPE(flt, 5, float, COPY);
DECL_TYPE(char, 6, int, COPY);
DECL_TYPE(any, 7, void*, NUGGET_TYPE_FLAG_GC);


//DECL_TYPE(aufile, 16, void*, 0);
//DECL_TYPE(aufilemode, 17, void*, 0);
DECL_TYPE(module, 18, void*, COPY);
DECL_TYPE(item, 19, nuggetItem, COPY);
DECL_TYPE(code, 20, void*, COPY);
DECL_TYPE(type, 21, void*, COPY);

// 30 to 41 are for machine primitives

#undef COPY



#define BOOL_OP(NAME, OP) \
    void bool_##NAME (stackFrame *frame, void* data) { \
        READ(char, a) \
        READ(char, b) \
        char r = a OP b; \
        WRITE(r) \
    }

BOOL_OP(and, &&)
BOOL_OP(or, ||)
BOOL_OP(xor, ^)
BOOL_OP(eq, ==)

void bool_true (stackFrame *frame, void* data) {
    char b = 1;
    WRITE(b)
}

void bool_false (stackFrame *frame, void* data) {
    char b = 0;
    WRITE(b)
}

void bool_not (stackFrame *frame, void* data) {
    READ(char, b)
    b = !b;
    WRITE(b)
}

nuggetItem bool_mod (char *name) {
    SCASE(name, "bool") return TITEM(&bool_t);
    SCASE(name, "true") return FITEM(BuiltinFunctionNew(bool_true, "bool.true", 0, 0, 1, &bool_t));
    SCASE(name, "false") return FITEM(BuiltinFunctionNew(bool_false, "bool.false", 0, 0, 1, &bool_t));
    SCASE(name, "not") return FITEM(BuiltinFunctionNew(bool_not, "bool.not", 0, 1, 1, &bool_t, &bool_t));
    SCASE(name, "and") return FITEM(BuiltinFunctionNew(bool_and, "bool.and", 0, 2, 1, &bool_t, &bool_t, &bool_t));
    SCASE(name, "or") return FITEM(BuiltinFunctionNew(bool_or, "bool.or", 0, 2, 1, &bool_t, &bool_t, &bool_t));
    SCASE(name, "xor") return FITEM(BuiltinFunctionNew(bool_xor, "bool.xor", 0, 2, 1, &bool_t, &bool_t, &bool_t));
    SCASE(name, "eq") return FITEM(BuiltinFunctionNew(bool_eq, "bool.eq", 0, 2, 1, &bool_t, &bool_t, &bool_t));

    return EMPTY_ITEM;
}



#define INT_OP(NAME, OP) \
    void int_##NAME (stackFrame *frame, void* data) { \
        READ(int, a) \
        READ(int, b) \
        int r = a OP b; \
        WRITE(r) \
    }

#define INT_BOP(NAME, OP) \
    void int_##NAME (stackFrame *frame, void* data) { \
        READ(int, a) \
        READ(int, b) \
        char r = a OP b; \
        WRITE(r) \
    }

INT_OP(add, +)
INT_OP(sub, -)
INT_OP(mul, *)
INT_OP(div, /)
INT_BOP(lt, <)
INT_BOP(gt, >)
INT_BOP(le, <=)
INT_BOP(ge, >=)
INT_BOP(eq, ==)
INT_BOP(ne, !=)

void int_neg (stackFrame *frame, void* data) {
    READ(int, a)
    int r = -a;
    WRITE(r)
}

nuggetItem int_mod (char *name) {
    SCASE(name, "int") return TITEM(&int_t);
    SCASE(name, "neg") return FITEM(BuiltinFunctionNew(int_neg, "int.neg", 0, 1, 1, &int_t, &int_t));
    SCASE(name, "add") return FITEM(BuiltinFunctionNew(int_add, "int.add", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "sub") return FITEM(BuiltinFunctionNew(int_sub, "int.sub", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "mul") return FITEM(BuiltinFunctionNew(int_mul, "int.mul", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "div") return FITEM(BuiltinFunctionNew(int_div, "int.div", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "lt") return FITEM(BuiltinFunctionNew(int_lt, "int.lt", 0, 2, 1, &int_t, &int_t, &bool_t));
    SCASE(name, "gt") return FITEM(BuiltinFunctionNew(int_gt, "int.gt", 0, 2, 1, &int_t, &int_t, &bool_t));
    SCASE(name, "le") return FITEM(BuiltinFunctionNew(int_le, "int.le", 0, 2, 1, &int_t, &int_t, &bool_t));
    SCASE(name, "ge") return FITEM(BuiltinFunctionNew(int_ge, "int.ge", 0, 2, 1, &int_t, &int_t, &bool_t));
    SCASE(name, "eq") return FITEM(BuiltinFunctionNew(int_eq, "int.eq", 0, 2, 1, &int_t, &int_t, &bool_t));
    SCASE(name, "ne") return FITEM(BuiltinFunctionNew(int_ne, "int.ne", 0, 2, 1, &int_t, &int_t, &bool_t));

    return EMPTY_ITEM;
}

INT_OP(and, &)
INT_OP(or, |)
INT_OP(xor, ^)

INT_OP(shl, <<)
INT_OP(shr, >>)

void int_biteq (stackFrame *frame, void* data) {
    READ(int, a)
    READ(int, b)
    int r = ~(a ^ b);
    WRITE(r)
}

void int_not (stackFrame *frame, void* data) {
    READ(int, a)
    int r = ~a;
    WRITE(r)
}

nuggetItem intbit_mod (char *name) {
    SCASE(name, "not") return FITEM(BuiltinFunctionNew(int_not, "int.not", 0, 1, 1, &int_t, &int_t));
    SCASE(name, "and") return FITEM(BuiltinFunctionNew(int_and, "int.and", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "or") return FITEM(BuiltinFunctionNew(int_or, "int.or", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "xor") return FITEM(BuiltinFunctionNew(int_xor, "int.xor", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "eq") return FITEM(BuiltinFunctionNew(int_biteq, "int.eq", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "shl") return FITEM(BuiltinFunctionNew(int_shl, "int.shl", 0, 2, 1, &int_t, &int_t, &int_t));
    SCASE(name, "shr") return FITEM(BuiltinFunctionNew(int_shr, "int.shr", 0, 2, 1, &int_t, &int_t, &int_t));

    return EMPTY_ITEM;
}






#define FLT_OP(NAME, OP) \
    void flt_##NAME (stackFrame *frame, void* data) { \
        READ(float, a) \
        READ(float, b) \
        float r = a OP b; \
        WRITE(r) \
    }

#define FLT_BOP(NAME, OP) \
    void flt_##NAME (stackFrame *frame, void* data) { \
        READ(float, a) \
        READ(float, b) \
        char r = a OP b; \
        WRITE(r) \
    }

FLT_OP(add, +)
FLT_OP(sub, -)
FLT_OP(mul, *)
FLT_OP(div, /)
FLT_BOP(lt, <)
FLT_BOP(gt, >)
FLT_BOP(le, <=)
FLT_BOP(ge, >=)
FLT_BOP(eq, ==)

void flt_neg (stackFrame *frame, void* data) {
    READ(float, a)
    float r = -a;
    WRITE(r)
}

void flt_itof (stackFrame *frame, void* data) {
    READ(int, a)
    float r = a;
    WRITE(r)
}

void flt_ftoi (stackFrame *frame, void* data) {
    READ(float, a)
    int r = a;
    WRITE(r)
}

void flt_decimal (stackFrame *frame, void* data) {
    READ(int, base)
    READ(int, exp)
    float r = base;
    while (exp > 0) {
        r *= 10.0;
        exp--;
    }
    while (exp < 0) {
        r /= 10.0;
        exp++;
    }
    WRITE(r)
}

void flt_nan (stackFrame *frame, void* data) {
    float r = NAN;
    WRITE(r)
}

void flt_inf (stackFrame *frame, void* data) {
    float r = NAN;
    WRITE(r)
}

void flt_isnan (stackFrame *frame, void* data) {
    READ(float, a)
    char r = isnan(a);
    WRITE(r)
}

void flt_isinf (stackFrame *frame, void* data) {
    READ(float, a)
    char r = isinf(a);
    WRITE(r)
}

nuggetItem flt_mod (char *name) {
    SCASE(name, "float") return TITEM(&flt_t);
    SCASE(name, "neg") return FITEM(BuiltinFunctionNew(flt_neg, "float.neg", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "add") return FITEM(BuiltinFunctionNew(flt_add, "float.add", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    SCASE(name, "sub") return FITEM(BuiltinFunctionNew(flt_sub, "float.sub", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    SCASE(name, "mul") return FITEM(BuiltinFunctionNew(flt_mul, "float.mul", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    SCASE(name, "div") return FITEM(BuiltinFunctionNew(flt_div, "float.div", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    SCASE(name, "lt") return FITEM(BuiltinFunctionNew(flt_lt, "float.lt", 0, 2, 1, &flt_t, &flt_t, &bool_t));
    SCASE(name, "gt") return FITEM(BuiltinFunctionNew(flt_gt, "float.gt", 0, 2, 1, &flt_t, &flt_t, &bool_t));
    SCASE(name, "le") return FITEM(BuiltinFunctionNew(flt_le, "float.le", 0, 2, 1, &flt_t, &flt_t, &bool_t));
    SCASE(name, "ge") return FITEM(BuiltinFunctionNew(flt_ge, "float.ge", 0, 2, 1, &flt_t, &flt_t, &bool_t));
    SCASE(name, "eq") return FITEM(BuiltinFunctionNew(flt_eq, "float.eq", 0, 2, 1, &flt_t, &flt_t, &bool_t));
    SCASE(name, "itof") return FITEM(BuiltinFunctionNew(flt_itof, "float.itof", 0, 1, 1, &int_t, &flt_t));
    SCASE(name, "ftoi") return FITEM(BuiltinFunctionNew(flt_ftoi, "float.ftoi", 0, 1, 1, &flt_t, &int_t));
    SCASE(name, "decimal") return FITEM(BuiltinFunctionNew(flt_decimal, "float.decimal", 0, 2, 1, &int_t, &int_t, &flt_t));

    SCASE(name, "nan") return FITEM(BuiltinFunctionNew(flt_nan, "float.nan", 0, 0, 1, &flt_t));
    SCASE(name, "infinity") return FITEM(BuiltinFunctionNew(flt_inf, "float.inf", 0, 0, 1, &flt_t));
    SCASE(name, "isnan") return FITEM(BuiltinFunctionNew(flt_isnan, "float.isnan", 0, 1, 1, &flt_t, &bool_t));
    SCASE(name, "isinfinity") return FITEM(BuiltinFunctionNew(flt_isinf, "float.isinf", 0, 1, 1, &flt_t, &bool_t));

    return EMPTY_ITEM;
}



void str_new (stackFrame *frame, void* data) {
    READ(nuggetBuffer*, buf)

    char *str = gc_alloc(&string_t, buf->size + 1);
    memcpy(str, &buf->data, buf->size);
    str[buf->size] = 0;

    writeRef(frame, str);
}

void str_newchar (stackFrame *frame, void* data) {
    READ(int, code)
    WRITE(code)
}

void str_itos (stackFrame *frame, void* data) {
    READ(int,n)

    // What's the safe size?
    char* s = gc_alloc(&string_t, 16);
    sprintf(s, "%d", n);

    writeRef(frame, s);
}

void str_ftos (stackFrame *frame, void* data) {
    READ(float,n)

    // What's the safe size?
    char* s = gc_alloc(&string_t, 32);
    sprintf(s, "%f", n);

    writeRef(frame, s);
}

void str_add (stackFrame *frame, void* data) {
    READ(char*, a)
    READ(char, c)

    char *r;
    if (c < 128) {
        int len = strlen(a);
        r = gc_alloc(&string_t, strlen(a) + 2);
        strcpy(r, a);
        r[len] = c;
        r[len+1] = 0;
    } else {
        // Not ascii
        r = a;
    }

    writeRef(frame, r);
}

void str_cat (stackFrame *frame, void* data) {
    READ(char*, a)
    READ(char*, b)

    char* r = gc_alloc(&string_t, strlen(a) + strlen(b) + 1);
    strcpy(r, a); strcat(r, b);

    writeRef(frame, r);
}

void str_slice (stackFrame *frame, void* data) {
    READ(char*, src)
    READ(int, a)
    READ(int, b)

    int len = strlen(src);

    if (a < 0 || a > b || b > len) {
        frame->error = 1;
        nuggetPushErrorF(
            frame->machine, 0,
            "Slice indices (%d, %d) are out of bounds for a string with size %d",
            a, b, len
        );
        return;
    }

    int nlen = b-a;
    char *r = gc_alloc(&string_t, nlen + 1);
    memcpy(r, src + a, nlen);
    r[nlen] = 0;

    writeRef(frame, r);
}

void str_eq (stackFrame *frame, void* data) {
    READ(char*, a)
    READ(char*, b)
    char r = strcmp(a, b) == 0;
    WRITE(r)
}

void str_charat (stackFrame *frame, void* data) {
    READ(char*, s)
    READ(int, i)

    if (i < 0 || i > strlen(s)) {
        frame->error = 1;
        nuggetPushErrorF(
            frame->machine, 0,
            "Character index %d out of bounds for a string with size %d",
            i, strlen(s)
        );
        return;
    }

    int c = s[i++];

    int retaddr = frame->program[frame->pc++];
    memcpy(&frame->data[retaddr], &c, sizeof(int));
    memcpy(&frame->data[retaddr + sizeof(int)], &i, sizeof(int));
}

void str_codeof (stackFrame *frame, void* data) {
    READ(int, code)
    WRITE(code)
}

void str_length (stackFrame *frame, void* data) {
    READ(char*, s)
    int c = strlen(s);
    WRITE(c)
}

void str_tobuffer (stackFrame *frame, void* data) {
    READ(char*, s)

    int len = strlen(s);

    nuggetBuffer *buf = nuggetBufferAlloc(len);
    buf->size = len;
    memcpy(&buf->data, s, len);

    WRITE(buf)
}

nuggetItem str_mod (char *name) {
    SCASE(name, "string") return TITEM(&string_t);
    SCASE(name, "char") return TITEM(&char_t);

    SCASE(name, "new") return FITEM(BuiltinFunctionNew(str_new, "string.new", 0, 1, 1, &buffer_t, &string_t));
    SCASE(name, "newchar") return FITEM(BuiltinFunctionNew(str_newchar, "string.new$char", 0, 1, 1, &int_t, &char_t));
    SCASE(name, "itos") return FITEM(BuiltinFunctionNew(str_itos, "string.itos", 0, 1, 1, &int_t, &string_t));
    SCASE(name, "ftos") return FITEM(BuiltinFunctionNew(str_ftos, "string.ftos", 0, 1, 1, &flt_t, &string_t));

    SCASE(name, "add") return FITEM(BuiltinFunctionNew(str_add, "string.add", 0, 2, 1, &string_t, &char_t, &string_t));
    SCASE(name, "concat") return FITEM(BuiltinFunctionNew(str_cat, "string.concat", 0, 2, 1, &string_t, &string_t, &string_t));
    SCASE(name, "slice") return FITEM(BuiltinFunctionNew(str_slice, "string.slice", 0, 3, 1, &string_t, &int_t, &int_t, &string_t));

    SCASE(name, "eq") return FITEM(BuiltinFunctionNew(str_eq, "string.eq", 0, 2, 1, &string_t, &string_t, &bool_t));
    SCASE(name, "charat") return FITEM(BuiltinFunctionNew(str_charat, "string.charat", 0, 2, 2, &string_t, &int_t, &char_t, &int_t));
    SCASE(name, "codeof") return FITEM(BuiltinFunctionNew(str_codeof, "string.codeof", 0, 1, 1, &char_t, &int_t));
    SCASE(name, "length") return FITEM(BuiltinFunctionNew(str_length, "string.length", 0, 1, 1, &string_t, &int_t));
    SCASE(name, "tobuffer") return FITEM(BuiltinFunctionNew(str_tobuffer, "string.tobuffer", 0, 1, 1, &string_t, &buffer_t));

    return EMPTY_ITEM;
}




void buf_new (stackFrame *frame, void* data) {
    READ(int, size)
    nuggetBuffer *buf = nuggetBufferAlloc(size);
    WRITE(buf)
}

void buf_get (stackFrame *frame, void* data) {
    READ(nuggetBuffer*, buf)
    READ(int, ix)
    if (ix < 0 || ix > buf->size) {
        nuggetPushErrorF(
            frame->machine, 0,
            "Index %d out of bounds for a buffer of size %d",
            ix, buf->size
        );
        frame->error = 1;
        return;
    }
    int val = buf->data[ix] & 0xff;
    WRITE(val)
}

void buf_set (stackFrame *frame, void* data) {
    READ(nuggetBuffer*, buf)
    READ(int, ix)
    READ(int, val)
    if (ix < 0 || ix > buf->size) {
        nuggetPushErrorF(
            frame->machine, 0,
            "Index %d out of bounds for a buffer of size %d",
            ix, buf->size
        );
        frame->error = 1;
        return;
    }
    buf->data[ix] = val;
}

void buf_size (stackFrame *frame, void* data) {
    READ(nuggetBuffer*, buf)
    WRITE(buf->size)
}

void buf_readonly (stackFrame *frame, void* data) {
    READ(nuggetBuffer*, buf)
    char flag = 0;
    WRITE(flag)
}

nuggetItem buf_mod (char *name) {
    SCASE(name, "buffer") return TITEM(&buffer_t);

    SCASE(name, "new") return FITEM(BuiltinFunctionNew(buf_new, "buffer.new", 0, 1, 1, &int_t, &buffer_t));
    SCASE(name, "size") return FITEM(BuiltinFunctionNew(buf_size, "buffer.size", 0, 1, 1, &buffer_t, &int_t));
    SCASE(name, "get") return FITEM(BuiltinFunctionNew(buf_get, "buffer.get", 0, 2, 1, &buffer_t, &int_t, &int_t));
    SCASE(name, "set") return FITEM(BuiltinFunctionNew(buf_set, "buffer.set", 0, 3, 0, &buffer_t, &int_t, &int_t));
    SCASE(name, "readonly") return FITEM(BuiltinFunctionNew(buf_readonly, "buffer.readonly", 0, 1, 1, &buffer_t, &bool_t));

    return EMPTY_ITEM;
}




void sys_println (stackFrame *frame, void* data) {
    READ(char*, line)
    printf("%s\n", line);
}

nuggetItem sys_mod (char *name) {
    SCASE(name, "println") return FITEM(BuiltinFunctionNew(sys_println, "system.println", 0, 1, 0, &string_t));
    return EMPTY_ITEM;
}




#define MATH0(NAME, C) \
    void math_##NAME (stackFrame *frame, void* data) { \
        float r = C; \
        WRITE(r) \
    }

#define MATH1(NAME, FN) \
    void math_##NAME (stackFrame *frame, void* data) { \
        READ(float, a) \
        float r = FN(a); \
        WRITE(r) \
    }

#define MATH2(NAME, FN) \
    void math_##NAME (stackFrame *frame, void* data) { \
        READ(float, a) \
        READ(float, b) \
        float r = FN(a, b); \
        WRITE(r) \
    }

// TODO: Is this well behaved in both signs?
float impl_round (float x) { return floor(x + 0.5); }
float impl_trunc (float x) { return x - floor(x); }
float impl_cbrt (float x) { return pow(x, 1.0/3.0); }
float impl_log (float x, float b) { return log(x)/log(b); }

MATH0(pi, 3.1415926535)
MATH0(e, 2.718281828)
MATH0(sqrt2, 1.41421356)

MATH1(abs, fabs)
MATH1(ceil, ceil)
MATH1(floor, floor)
MATH1(round, impl_round)
MATH1(trunc, impl_trunc)

MATH1(ln, log)
MATH1(exp, exp)
MATH1(sqrt, sqrt)
MATH1(cbrt, impl_cbrt)

MATH1(sin, sin)
MATH1(cos, cos)
MATH1(tan, tan)
MATH1(asin, asin)
MATH1(acos, acos)
MATH1(atan, atan)
MATH1(sinh, sinh)
MATH1(cosh, cosh)
MATH1(tanh, tanh)

MATH2(pow, pow)
MATH2(mod, fmod)
MATH2(log, impl_log)
MATH2(atan2, atan2)

nuggetItem math_module (char *name) {
    SCASE(name, "pi") return FITEM(BuiltinFunctionNew(math_pi, "math.pi", 0, 0, 1, &flt_t));
    SCASE(name, "e") return FITEM(BuiltinFunctionNew(math_e, "math.e", 0, 0, 1, &flt_t));
    SCASE(name, "sqrt2") return FITEM(BuiltinFunctionNew(math_sqrt2, "math.sqrt2", 0, 0, 1, &flt_t));

    SCASE(name, "abs") return FITEM(BuiltinFunctionNew(math_abs, "math.abs", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "ceil") return FITEM(BuiltinFunctionNew(math_ceil, "math.ceil", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "floor") return FITEM(BuiltinFunctionNew(math_floor, "math.floor", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "round") return FITEM(BuiltinFunctionNew(math_round, "math.round", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "trunc") return FITEM(BuiltinFunctionNew(math_trunc, "math.trunc", 0, 1, 1, &flt_t, &flt_t));

    SCASE(name, "ln") return FITEM(BuiltinFunctionNew(math_ln, "math.ln", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "exp") return FITEM(BuiltinFunctionNew(math_exp, "math.exp", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "sqrt") return FITEM(BuiltinFunctionNew(math_sqrt, "math.sqrt", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "cbrt") return FITEM(BuiltinFunctionNew(math_cbrt, "math.cbrt", 0, 1, 1, &flt_t, &flt_t));

    SCASE(name, "sin") return FITEM(BuiltinFunctionNew(math_sin, "math.sin", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "cos") return FITEM(BuiltinFunctionNew(math_cos, "math.cos", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "tan") return FITEM(BuiltinFunctionNew(math_tan, "math.tan", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "asin") return FITEM(BuiltinFunctionNew(math_asin, "math.asin", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "acos") return FITEM(BuiltinFunctionNew(math_acos, "math.acos", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "atan") return FITEM(BuiltinFunctionNew(math_atan, "math.atan", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "sinh") return FITEM(BuiltinFunctionNew(math_sinh, "math.sinh", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "cosh") return FITEM(BuiltinFunctionNew(math_cosh, "math.cosh", 0, 1, 1, &flt_t, &flt_t));
    SCASE(name, "tanh") return FITEM(BuiltinFunctionNew(math_tanh, "math.tanh", 0, 1, 1, &flt_t, &flt_t));

    SCASE(name, "pow") return FITEM(BuiltinFunctionNew(math_pow, "math.pow", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    SCASE(name, "mod") return FITEM(BuiltinFunctionNew(math_mod, "math.mod", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    SCASE(name, "log") return FITEM(BuiltinFunctionNew(math_log, "math.log", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    SCASE(name, "atan2") return FITEM(BuiltinFunctionNew(math_atan2, "math.atan2", 0, 2, 1, &flt_t, &flt_t, &flt_t));
    return EMPTY_ITEM;
}




nuggetItem getItemWrapper (nuggetModule *_mod, char *name) {
    return ((BuiltinMod*) _mod)->getter(name);
}

nuggetModule *newMod (nuggetMachine* machine, char* name, getter g) {
    BuiltinMod *mod = malloc(sizeof(BuiltinMod));
    mod->_mod.machine = machine;
    mod->_mod.name = name;
    mod->_mod.getItem = getItemWrapper;
    mod->_mod.build = 0;
    mod->getter = g;
    return (nuggetModule *) mod;
}

nuggetModule *nuggetModuleBuiltin (nuggetMachine *machine, char *name) {
    SCASE(name, "auro\x1f" "bool") return newMod(machine, "auro.bool", bool_mod);
    SCASE(name, "auro\x1f" "int") return newMod(machine, "auro.int", int_mod);
    SCASE(name, "auro\x1f" "float") return newMod(machine, "auro.float", flt_mod);
    SCASE(name, "auro\x1f" "string") return newMod(machine, "auro.string", str_mod);
    SCASE(name, "auro\x1f" "buffer") return newMod(machine, "auro.buffer", buf_mod);
    SCASE(name, "auro\x1f" "math") return newMod(machine, "auro.math", math_module);

    // Most of auro.system and auro.io should be implemented by the main executable, not by nugget.
    // Nugget tries to not do IO if possible
    //SCASE(name, "auro\x1f" "system") return newMod(machine, "auro.system", sys_mod);

    SCASE(name, "auro\x1f" "int\x1f" "bit") return newMod(machine, "auro.int.bit", intbit_mod);

    SCASE(name, "auro\x1f" "null") return newNullModule(machine);
    SCASE(name, "auro\x1f" "array") return newArrayModule(machine);
    SCASE(name, "auro\x1f" "any") return newAnyModule(machine);

    SCASE(name, "auro\x1f" "typeshell") return newShellModule(machine);
    SCASE(name, "auro\x1f" "record") return newRecordModule(machine);

    // reflect.c
    SCASE(name, "auro\x1f" "type") return newTypeModule(machine);
    SCASE(name, "auro\x1f" "module") return newModuleModule(machine);
    SCASE(name, "auro\x1f" "function") return newFunctionModule(machine);
    SCASE(name, "auro\x1f" "module\x1f" "item") return newItemModule(machine);
    SCASE(name, "auro\x1f" "module\x1f" "code") return newCodeModule(machine);

    // machine.c
    SCASE(name, "auro\x1f" "machine\x1f" "primitive") return newPrimModule(machine);
    SCASE(name, "auro\x1f" "machine\x1f" "memory") return newMemModule(machine);
    SCASE(name, "auro\x1f" "machine\x1f" "mem") return newMemModule(machine); // should deprecate

    // reflect.c
    //SCASE(name, "auro\x1f" "debug") return newDebugModule(machine); // deprecate
    SCASE(name, "nugget\x1f" "debug") return newDebugModule(machine);

    return 0;
}


InstanceDict InstanceDictNew () {
    InstanceDict dict;
    dict.capacity = 32;
    dict.entries = malloc(sizeof(InstanceDictEntry) * dict.capacity);
    dict.count = 0;
    return dict;
}

nuggetModule *InstanceDictGet (InstanceDict *dict, nuggetType *type) {
    for (int i = 0; i < dict->count; ++i) {
        InstanceDictEntry *entry = &dict->entries[i];
        if (nuggetTypeEq(entry->type, type)) {
            return entry->module;
        }
    }
    return 0;
}

void InstanceDictSet (InstanceDict *dict, nuggetType *type, nuggetModule *instance) {
    if (dict->count >= dict->capacity) {
        dict->capacity *= 2;
        dict->entries = realloc(dict->entries, sizeof(InstanceDictEntry) * dict->capacity);
    }

    dict->entries[dict->count++] = (InstanceDictEntry) {
        type: type,
        module: instance,
    };
}


nuggetType *nuggetTypeInt() { return &int_t; }
nuggetType *nuggetTypeFloat() { return &flt_t; }
nuggetType *nuggetTypeBool() { return &bool_t; }
nuggetType *nuggetTypeString() { return &string_t; }
nuggetType *nuggetTypeBuffer() { return &buffer_t; }