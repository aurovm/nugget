#include "../headers.h"
#include "../stackFrame.h"

#define DECL_TYPE(NAME, ID, TP, FLAG) nuggetType NAME##_t = { id: ID, size: sizeof(TP), name: #NAME, flags: FLAG }

#define SCASE(VAR, PATTERN) if (strcmp(VAR, PATTERN) == 0)

#define READ(TYPE, NAME) TYPE NAME; memcpy(&NAME, &frame->data[frame->program[frame->pc++]], sizeof(TYPE));
#define WRITE(NAME) memcpy(&frame->data[frame->program[frame->pc++]], &NAME, sizeof(NAME));

#define FITEM(P) (nuggetItem) { kind: nuggetItemFunction, item: P }
#define TITEM(P) (nuggetItem) { kind: nuggetItemType, item: P }
