#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"

#define COPY NUGGET_TYPE_FLAG_COPY
DECL_TYPE(u8,  30, char, COPY);
DECL_TYPE(u16, 31, short int, COPY);
DECL_TYPE(u32, 32, long int, COPY);
DECL_TYPE(u64, 33, long long int, COPY);
DECL_TYPE(i8,  34, char, COPY);
DECL_TYPE(i16, 35, short int, COPY);
DECL_TYPE(i32, 36, long int, COPY);
DECL_TYPE(i64, 37, long long int, COPY);
DECL_TYPE(f32, 38, float, COPY);
DECL_TYPE(f64, 39, double, COPY);

DECL_TYPE(pointer, 40, void*, COPY);


void mem_size (stackFrame *frame, void *_) {
    int r = sizeof(void*);
    WRITE(r);
}

void mem_null (stackFrame *frame, void *_) {
    void *r = 0;
    WRITE(r);
}

void mem_eq (stackFrame *frame, void *_) {
    READ(void*, a);
    READ(void*, b);
    char r = a == b;
    WRITE(r);
}

void mem_alloc (stackFrame *frame, void *_) {
    READ(int, size);
    void *r = malloc(size);
    WRITE(r);
}


void mem_read (stackFrame *frame, void *_) {
    READ(void*, p);
    int r = *((char*)p);
    WRITE(r);
}

void mem_write (stackFrame *frame, void *_) {
    READ(void*, p);
    READ(int, v);
    *((char*)p) = (char) v;
}

void mem_offset (stackFrame *frame, void *_) {
    READ(void*, p);
    READ(int, v);
    p += v;
    WRITE(p);
}

// Any is represented as just a pointer, so it uses these


#define PRIM_OPS(NAME, TYPE, BASE) \
    void mem_##NAME##_read (stackFrame *frame, void* data) { \
        READ(TYPE*, p); \
        TYPE v = *p; \
        WRITE(v) \
    } \
    void mem_##NAME##_write (stackFrame *frame, void* data) { \
        READ(TYPE*, p); \
        READ(TYPE, v); \
        *p = v; \
    }

PRIM_OPS(u8, char, int);
PRIM_OPS(u16, short int, int);
PRIM_OPS(u32, long int, int);
PRIM_OPS(u64, long long int, int);
PRIM_OPS(i8, char, int);
PRIM_OPS(i16, short int, int);
PRIM_OPS(i32, long int, int);
PRIM_OPS(i64, long long int, int);
PRIM_OPS(f32, float, float);
PRIM_OPS(f64, double, float);

void mem_readptr (stackFrame *frame, void *_) {
    READ(void**, p);
    void *v = *p;
    WRITE(v);
}

void mem_writeptr (stackFrame *frame, void *_) {
    READ(void**, p);
    READ(void*, v);
    *p = v;
}

void mem_allocptr (stackFrame *frame, void *_) {
    READ(void*, a);
    void **p = malloc(sizeof(void*));
    *p = a;
    WRITE(p);
}

nuggetItem getMemModule (nuggetModule *_m, char *name) {
    SCASE(name, "pointer") return (nuggetItem) { kind: nuggetItemType, item: &pointer_t };

    SCASE(name, "size") return FITEM(BuiltinFunctionNew(mem_size, "memory.size", 0, 0, 1, &int_t));
    SCASE(name, "null") return FITEM(BuiltinFunctionNew(mem_null, "memory.null", 0, 0, 1, &pointer_t));
    SCASE(name, "eq") return FITEM(BuiltinFunctionNew(mem_eq, "memory.eq", 0, 2, 1, &pointer_t, &pointer_t, &bool_t));

    SCASE(name, "alloc") return FITEM(BuiltinFunctionNew(mem_alloc, "memory.alloc", 0, 1, 1, &int_t, &pointer_t));
    SCASE(name, "read") return FITEM(BuiltinFunctionNew(mem_read, "memory.read", 0, 1, 1, &pointer_t, &int_t));
    SCASE(name, "write") return FITEM(BuiltinFunctionNew(mem_write, "memory.write", 0, 2, 0, &pointer_t, &int_t));
    SCASE(name, "offset") return FITEM(BuiltinFunctionNew(mem_offset, "memory.offset", 0, 2, 1, &pointer_t, &int_t, &pointer_t));

    SCASE(name, "readany") return FITEM(BuiltinFunctionNew(mem_readptr, "memory.readany", 0, 1, 1, &pointer_t, &any_t));
    SCASE(name, "allocany") return FITEM(BuiltinFunctionNew(mem_allocptr, "memory.allocany", 0, 1, 1, &any_t, &pointer_t));

    SCASE(name, "read\x1dpointer") return FITEM(BuiltinFunctionNew(mem_readptr, "memory.readptr", 0, 1, 1, &pointer_t, &pointer_t));
    SCASE(name, "write\x1dpointer") return FITEM(BuiltinFunctionNew(mem_writeptr, "memory.writeptr", 0, 2, 0, &pointer_t, &pointer_t));

    #define PRIM_MEM_USE(NAME) \
        SCASE(name, #NAME) return (nuggetItem) { kind: nuggetItemType, item: &NAME##_t }; \
        SCASE(name, "read\x1d" #NAME) return FITEM(BuiltinFunctionNew(mem_##NAME##_read, "memory.read" #NAME, 0, 1, 1, &pointer_t, &NAME##_t)); \
        SCASE(name, "write\x1d" #NAME) return FITEM(BuiltinFunctionNew(mem_##NAME##_write, "memory.write" #NAME, 0, 2, 0, &pointer_t, &NAME##_t));

    PRIM_MEM_USE(u8);
    PRIM_MEM_USE(u16);
    PRIM_MEM_USE(u32);
    PRIM_MEM_USE(u64);
    PRIM_MEM_USE(i8);
    PRIM_MEM_USE(i16);
    PRIM_MEM_USE(i32);
    PRIM_MEM_USE(i64);
    PRIM_MEM_USE(f32);
    PRIM_MEM_USE(f64);

    return EMPTY_ITEM;
}

#define PRIM_IMPL(NAME, TYPE, BASE) \
    void prim_##NAME##_get (stackFrame *frame, void* data) { \
        READ(TYPE, v); \
        BASE r = v; \
        WRITE(r) \
    } \
    void prim_##NAME##_new (stackFrame *frame, void* data) { \
        READ(BASE, v); \
        TYPE r = v; \
        WRITE(r) \
    }

PRIM_IMPL(u8, char, int);
PRIM_IMPL(u16, short int, int);
PRIM_IMPL(u32, long int, int);
PRIM_IMPL(u64, long long int, int);
PRIM_IMPL(i8, char, int);
PRIM_IMPL(i16, short int, int);
PRIM_IMPL(i32, long int, int);
PRIM_IMPL(i64, long long int, int);
PRIM_IMPL(f32, float, float);
PRIM_IMPL(f64, double, float);

nuggetItem getPrimModule (nuggetModule *_m, char *name) {
    /*
    SCASE(name, "u8") return (nuggetItem) { kind: nuggetItemType, item: &u8_t };
    SCASE(name, "u16") return (nuggetItem) { kind: nuggetItemType, item: &u16_t };
    SCASE(name, "u32") return (nuggetItem) { kind: nuggetItemType, item: &u32_t };
    SCASE(name, "u64") return (nuggetItem) { kind: nuggetItemType, item: &u64_t };
    SCASE(name, "i8") return (nuggetItem) { kind: nuggetItemType, item: &i8_t };
    SCASE(name, "i16") return (nuggetItem) { kind: nuggetItemType, item: &i16_t };
    SCASE(name, "i32") return (nuggetItem) { kind: nuggetItemType, item: &i32_t };
    SCASE(name, "i64") return (nuggetItem) { kind: nuggetItemType, item: &i64_t };
    SCASE(name, "f32") return (nuggetItem) { kind: nuggetItemType, item: &f32_t };
    SCASE(name, "f64") return (nuggetItem) { kind: nuggetItemType, item: &f64_t };
    */

    // TODO: Flip the name of the functions. Should be get$i32, not i32$get
    #define PRIM_USE(NAME, TYPE, BASE) \
        SCASE(name, #NAME) return (nuggetItem) { kind: nuggetItemType, item: &NAME##_t }; \
        SCASE(name, #NAME "\x1dnew") return FITEM(BuiltinFunctionNew(prim_##NAME##_new, "get_" #NAME, 0, 1, 1, &BASE##_t, &NAME##_t)); \
        SCASE(name, #NAME "\x1dget") return FITEM(BuiltinFunctionNew(prim_##NAME##_get, "new_" #NAME, 0, 1, 1, &NAME##_t, &BASE##_t));

    PRIM_USE(u8, char, int);
    PRIM_USE(u16, short int, int);
    PRIM_USE(u32, long int, int);
    PRIM_USE(u64, long long int, int);
    PRIM_USE(i8, char, int);
    PRIM_USE(i16, short int, int);
    PRIM_USE(i32, long int, int);
    PRIM_USE(i64, long long int, int);
    PRIM_USE(f32, float, flt);
    PRIM_USE(f64, double, flt);

    return EMPTY_ITEM;
}

nuggetModule *newMemModule (nuggetMachine *machine) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = getMemModule;
    mod->build = 0;
    mod->machine = machine;
    mod->name = "auro.machine.memory";
    return mod;
}

nuggetModule *newPrimModule (nuggetMachine *machine) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = getPrimModule;
    mod->build = 0;
    mod->machine = machine;
    mod->name = "auro.machine.primitive";
    return mod;
}
