#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"


#define CODE_CAPACITY 32
#define TYPE_COUNT 16

typedef struct {
    int capacity;
    int count;
    int inCount;
    int outCount;
    int *code; // -1 means there's a function in that index
    nuggetFunction **fns;
    nuggetType *types[TYPE_COUNT * 2];
} ReflectCode;

void code_new (stackFrame *frame, void *_) {
    // READ(ReflectCode*, code);
    ReflectCode *code = malloc(sizeof(ReflectCode));
    code->capacity = CODE_CAPACITY;
    code->count = 0;
    code->inCount = 0;
    code->outCount = 0;
    code->code = malloc(CODE_CAPACITY * sizeof(int));
    code->fns = malloc(CODE_CAPACITY * sizeof(void*));
    WRITE(code);
}

void code_addin (stackFrame *frame, void *_) {
    READ(ReflectCode*, code);
    READ(nuggetItem, item);
    if (item.kind != nuggetItemType) {
        printf("Tried to add a non-type input to a code\n");
        frame->error = 1;
        return;
    }
    code->types[code->inCount++] = item.item;
}

void code_addout (stackFrame *frame, void *_) {
    READ(ReflectCode*, code);
    READ(nuggetItem, item);
    if (item.kind != nuggetItemType) {
        printf("Tried to add a non-type output to a code\n");
        frame->error = 1;
        return;
    }
    code->types[TYPE_COUNT + code->outCount++] = item.item;
}

void code_addint (stackFrame *frame, void *_) {
    READ(ReflectCode*, code);
    READ(int, value);
    code->code[code->count++] = value;
}

void code_addfn (stackFrame *frame, void *_) {
    READ(ReflectCode*, code);
    READ(nuggetItem, item);
    if (item.kind != nuggetItemFunction) {
        printf("Tried to add a non-function to a code\n");
        frame->error = 1;
        return;
    }
    code->fns[code->count] = item.item;
    code->code[code->count++] = nuggetInstCall;
}

int skips[8] = {0, 0, 0, 1, 2, 1, 2, 2};

void code_toitem (stackFrame *frame, void *_) {
    READ(ReflectCode*, code);

    compiledFunction *fn = malloc(sizeof(compiledFunction));
    fn->_fn.kind = nuggetFunctionCompiled;
    fn->_fn.name = "<auro.module.code created>";

    fn->frameSize = 0;
    fn->_fn.inCount = code->inCount;
    fn->_fn.outCount = code->outCount;
    fn->_fn.ins = code->types;
    fn->_fn.outs = code->types + TYPE_COUNT;

    // --- Register assignment phase --- //

    int regCount = fn->_fn.inCount;

    int instCount = 0;
    int *instRegs = calloc(sizeof(int), code->count);

    for (int pc = 0; pc < code->count;) {
        int ipc = pc;
        instCount++;
        switch (code->code[pc++]) {
            case nuggetInstEnd:
                pc += code->outCount;
                break;
            case nuggetInstHlt:
                break;
            case nuggetInstJmp:
                pc += 1;
                break;
            case nuggetInstSet:
            case nuggetInstJif:
            case nuggetInstNif:
                pc += 2;
                break;
            case nuggetInstVar:
                instRegs[ipc] = regCount++;
                break;
            case nuggetInstDup:
                instRegs[ipc] = regCount++;
                pc++;
                break;
            case nuggetInstCall: {
                nuggetFunction *cfn = code->fns[ipc];
                instRegs[ipc] = regCount;
                regCount += cfn->outCount;
                pc += cfn->inCount;
                break;
            }
            default:
                printf("Invalid code %d (%d th)\n", code[ipc], ipc);
                goto error;
        }
    }

    // --- Type resolution phase --- //

    nuggetType **regTypes = calloc(sizeof(void*), regCount);

    for (int i = 0; i < fn->_fn.inCount; i++) {
        regTypes[i] = fn->_fn.ins[i];
    }

    char *instResolved = calloc(1, code->count);
    int resolveCount = 0;

    while (resolveCount < instCount) {
        int anyResolved = 0;

        for (int pc = 0; pc < code->count;) {
            int ipc = pc;
            if (instResolved[ipc]) continue;

            int resolved = 0;
            int inst = code->code[pc++];

            switch (inst) {
                case nuggetInstEnd:
                    pc += code->outCount;
                case nuggetInstHlt:
                case nuggetInstVar:
                case nuggetInstJmp:
                case nuggetInstJif:
                case nuggetInstNif:
                    pc += skips[inst];
                    resolved = 1;
                    break;
                case nuggetInstSet: {
                    int a = code->code[pc++];
                    int b = code->code[pc++];
                    if (regTypes[b]) {
                        if (regTypes[a] && regTypes[a] != regTypes[b]) {
                            printf("Tried to assign %s to a %s, at %s:%d\n",
                                regTypes[b]->name, regTypes[a]->name, fn->_fn.name, pc
                            );
                            goto error;
                        }

                        regTypes[a] = regTypes[b];
                        resolved = 1;
                    }
                    break;
                }
                case nuggetInstDup: {
                    nuggetType *tp = regTypes[code->code[pc++]];
                    if (tp) {
                        regTypes[instRegs[ipc]] = tp;
                        resolved = 1;
                    }
                    break;
                }
                case nuggetInstCall: {
                    nuggetFunction *fn = code->fns[ipc];

                    for (int j = 0; j < fn->outCount; j++) {
                        regTypes[instRegs[ipc] + j] = fn->outs[j];
                    }

                    pc += fn->inCount;
                    resolved = 1;
                    break;
                }
                default:
                    printf("Unsupported instruction (type resolution phase): %d\n", inst);
                    goto error;
            }

            if (resolved) {
                resolveCount++;
                instResolved[ipc] = 1;
                anyResolved = 1;
            }
        }

        if (!anyResolved) {
            printf("Cannot resolve register types\n");
            goto error;
        }
    }

    // --- Register allocation phase --- //

    int *registers = calloc(sizeof(int), regCount);

    fn->frameSize = 0;
    for (int i = 0; i < regCount; i++) {
        registers[i] = fn->frameSize;
        fn->frameSize += regTypes[i]->size;
    }





    int *instOffsets = calloc(sizeof(int), code->count);
    int *jumps = calloc(sizeof(int), code->count);
    int jumpCount = 0;

    int capacity = 32;
    int _pc = 0;

    fn->program = malloc(sizeof(int) * capacity);

    //#define IDEBUG(...) printf(__VA_ARGS__)
    #define IDEBUG(...)

    #define ENSURE(N) if (_pc+(N) < capacity) { capacity += 32; fn->program = realloc(fn->program, sizeof(int) * capacity); }
    #define PUSH(EXPR) { IDEBUG(" %d", EXPR); fn->program[_pc++] = EXPR; }
    #define PUSHI(EXPR) { IDEBUG("\n    %d (%d): " #EXPR, _pc, i); fn->program[_pc++] = EXPR; }

    for (int pc = 0; pc < code->count;) {
        int ipc = pc;
        switch (code->code[pc++]) {
            case nuggetInstEnd: {
                ENSURE(1 + code->outCount);
                PUSHI(InstEnd);
                for (int j = 0; j < code->outCount; j++) {
                    PUSH(registers[code->code[pc++]]);
                }
                break;
            }
            case nuggetInstVar:
                break;
            case nuggetInstDup: {
                int a = code->code[pc++];
                ENSURE(4);
                PUSHI(InstMov);
                PUSH(regTypes[a]->size);
                PUSH(registers[instRegs[ipc]]);
                PUSH(registers[a]);
                break;
            }
            case nuggetInstSet: {
                int a = code->code[pc++], b = code->code[pc++];
                ENSURE(4);
                PUSHI(InstMov);
                PUSH(regTypes[a]->size);
                PUSH(registers[a]);
                PUSH(registers[b]);
                break;
            }
            case nuggetInstJmp: {
                ENSURE(2);
                PUSHI(InstJmp);
                jumps[jumpCount++] = ipc;
                PUSH(code->code[pc++]);
                break;
            }
            case nuggetInstJif: {
                ENSURE(3);
                PUSHI(InstJif);
                jumps[jumpCount++] = ipc;
                PUSH(code->code[pc++]);
                PUSH(registers[code->code[pc++]]);
                break;
            }
            case nuggetInstNif: {
                ENSURE(3);
                PUSHI(InstNif);
                jumps[jumpCount++] = ipc;
                PUSH(code->code[pc++]);
                PUSH(registers[code->code[pc++]]);
                break;
            }
            case nuggetInstCall: {
                nuggetFunction *cfn = code->fns[ipc];

                ENSURE(1 + INT_PER_PTR + cfn->inCount);

                PUSHI(InstCall);
                IDEBUG(" %s", cfn->name);

                // WRITE UNALIGNED
                memcpy(&fn->program[_pc], &cfn, sizeof(void*));
                _pc += INT_PER_PTR;

                for (int j = 0; j < cfn->inCount; j++) {
                    PUSH(registers[code->code[pc++]]);
                }

                if (cfn->outCount > 0) {
                    ENSURE(1);
                    PUSH(registers[instRegs[ipc]]);
                }

                break;
            }
            default:
                // Could be halt. not supported yet
                printf("Unsupported instruction (instruction generation phase): %d (at instruction %d)\n", code->code[ipc], ipc);
                goto error;
        }
    }

    nuggetItem item = FITEM(fn);
    WRITE(item);
    return;

    error:
    printf("An error ocurred compiling an instance of module.code\n");
    frame->error = 1;
    return;
}

nuggetItem getCodeModule (nuggetModule *_m, char *name) {
    SCASE(name, "new") return FITEM(BuiltinFunctionNew(code_new, "code.new", 0, 0, 1, &code_t));
    SCASE(name, "addinput") return FITEM(BuiltinFunctionNew(code_addin, "code.addinput", 0, 2, 0, &code_t, &item_t));
    SCASE(name, "addoutput") return FITEM(BuiltinFunctionNew(code_addout, "code.addoutput", 0, 2, 0, &code_t, &item_t));
    SCASE(name, "addint") return FITEM(BuiltinFunctionNew(code_addint, "code.addint", 0, 2, 0, &code_t, &int_t));
    SCASE(name, "addfn") return FITEM(BuiltinFunctionNew(code_addfn, "code.addfn", 0, 2, 0, &code_t, &item_t));

    SCASE(name, "toitem") return FITEM(BuiltinFunctionNew(code_toitem, "code.toitem", 0, 1, 1, &code_t, &item_t));
    return EMPTY_ITEM;
}

nuggetModule *newCodeModule (nuggetMachine *machine) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = getCodeModule;
    mod->build = 0;
    mod->machine = machine;
    mod->name = "auro.module.code";
    return mod;
}
