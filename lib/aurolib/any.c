#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"

#include "../gc.h"
#include "../any.h"

typedef struct {
    nuggetModule _mod;
    nuggetType *base;

    nuggetFunction *new;
    nuggetFunction *get;
    nuggetFunction *test;
} AnyMod;


void any_new (stackFrame *frame, void* _mod) {
    AnyMod *mod = _mod;
    nuggetType *type = mod->base;
    Any *any = anyNew(type);
    stackFrameReadArg(frame, anyGetValuePtr(any));
    stackFrameWriteOut(frame, &any);
}

void any_get (stackFrame *frame, void *_mod) {
    AnyMod *mod = _mod;
    nuggetType *type = mod->base;

    READ(Any*, any)

    if (any->type->id != type->id) {
        frame->error = 1;
        nuggetPushErrorF(frame->machine, 0, "Tried to get a %s from an any %s", type->name, any->type->name);
        return;
    }

    stackFrameWriteOut(frame, anyGetValuePtr(any));
}

void any_test (stackFrame *frame, void *_mod) {
    AnyMod *mod = _mod;
    nuggetType *type = mod->base;

    READ(Any*, any)
    char r = any->type->id == type->id;
    WRITE(r);
}

void any_drop (nuggetMachine *machine, void *value, void *data) {
    Any *any = value;
    if (any->type->flags & NUGGET_TYPE_FLAG_GC) {
        void **inner = anyGetValuePtr(any);
        gc_decref(*inner);
    }
}



nuggetItem anyItemGetter (nuggetModule *_m, char *name) {
    AnyMod *mod = (AnyMod*) _m;
    SCASE(name, "get") return FITEM(mod->get);
    SCASE(name, "new") return FITEM(mod->new);
    SCASE(name, "test") return FITEM(mod->test);

    return EMPTY_ITEM;
}

nuggetModule *buildAnyModule (nuggetModule *_self, nuggetModule *argument) {
    nuggetItem baseItem = nuggetModuleGetItem(argument, "0");
    if (baseItem.kind != nuggetItemType) {
        printf("No type item '0' in any module argument\n");
        return 0;
    }

    nuggetType *base = baseItem.item;

    AnyMod *mod = malloc(sizeof(AnyMod));
    mod->_mod.getItem = anyItemGetter;
    mod->_mod.build = 0;

    mod->base = base;

    mod->new = BuiltinFunctionNew(any_new, "any.new", mod, 1, 1, base, &any_t);
    mod->get = BuiltinFunctionNew(any_get, "any.get", mod, 1, 1, &any_t, base);
    mod->test = BuiltinFunctionNew(any_test, "any.test", mod, 1, 1, &any_t, &bool_t);

    return (nuggetModule*) mod;
}



nuggetItem anyBaseItemGetter (nuggetModule *_m, char *name) {
    SCASE(name, "any") return (nuggetItem) { kind: nuggetItemType, item: &any_t };
    return EMPTY_ITEM;
}

nuggetModule *newAnyModule (nuggetMachine *machine) {
    if (!any_t.drop) {
        any_t.drop = any_drop;
    }
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = anyBaseItemGetter;
    mod->build = buildAnyModule;
    mod->machine = machine;
    return mod;
}