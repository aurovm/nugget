#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>
#include <stddef.h>
#include <assert.h>

#include "headers.h"

#include "../gc.h"
#include "../fndict.h"
#include "../debug.h"
#include "../any.h"


typedef struct {
    nuggetModule _mod;
    nuggetModule *base;
} NewModuleMod;

typedef struct {
    nuggetModule _mod;
    nuggetType *type;
    nuggetFunction *get;
    nuggetFunction *build;
    char value[];
} CreatedModule;



void mod_get (stackFrame *frame, void *_mod) {
    READ(nuggetModule*, mod);
    READ(char*, name);
    nuggetItem item = nuggetModuleGetItem(mod, name);
    WRITE(item);
}

void mod_build (stackFrame *frame, void *_mod) {
    READ(nuggetModule*, base);
    READ(nuggetModule*, argument);
    if (!base->build) {
        printf("ERROR: Module %s cannot be built\n", base->name);
        frame->error = 1;
        return;
    }
    nuggetModule *result = base->build(base, argument);
    if (!result) {
        printf("ERROR: No module resulted when building %s.\n", base->name);
        frame->error = 1;
        return;
    }

    WRITE(result);
}

void newmod_get (stackFrame *frame, void *_mod) {
    NewModuleMod *mod = _mod;
    nuggetModule *val = mod->base;
    WRITE(val);
}

nuggetItem getNewModuleModule (nuggetModule *_m, char *name) {
    SCASE(name, "") return FITEM(BuiltinFunctionNew(newmod_get, "module.new", _m, 0, 1, &module_t));
    return EMPTY_ITEM;
}

nuggetModule *buildNewModuleModule (nuggetModule *_self, nuggetModule *argument) {
    NewModuleMod *mod = malloc(sizeof(NewModuleMod));
    mod->_mod.getItem = getNewModuleModule;
    mod->_mod.build = 0;

    mod->base = argument;

    return (nuggetModule*) mod;
}

nuggetItem createdModuleGet (nuggetModule *_mod, char *name) {
    CreatedModule *mod = _mod;

    nuggetItem item;

    int size = strlen(name) + 1;
    char *namearg = gc_alloc(&string_t, size);
    memcpy(namearg, name, size);

    // TODO: Check error
    nuggetFunctionArgs *args = nuggetFunctionArgsNew(mod->get);
    nuggetFunctionArgsWriteInput(args, 0, mod->value);
    nuggetFunctionArgsWriteInput(args, 1, &namearg);
    nuggetFunctionCall(_mod->machine, mod->get, args);

    if (_mod->machine->state == machineStateError) {
        printf("ERROR: Created get %s failed.\n", mod->get->name);
        return EMPTY_ITEM;
    }

    nuggetFunctionArgsReadOutput(args, 0, &item);

    return item;
}

nuggetModule *createdModuleBuild (nuggetModule *_mod, nuggetModule *argument) {
    CreatedModule *mod = _mod;
    nuggetMachine *machine = _mod->machine;

    nuggetModule *result;

    assert(argument != 0);

    // TODO: Check error
    nuggetFunctionArgs *args = nuggetFunctionArgsNew(mod->build);
    nuggetFunctionArgsWriteInput(args, 0, mod->value);
    nuggetFunctionArgsWriteInput(args, 1, &argument);
    nuggetFunctionCall(machine, mod->build, args);

    if (nuggetHasError(machine)) {
        pushLoadError(machine, 0, "created module (build)");
        return 0;
    }

    nuggetFunctionArgsReadOutput(args, 0, &result);
    if (!result) {
        pushLoadError(machine, "result module is null", "created module (build)");
        return 0;
    }

    return result;
}

void mod_create (stackFrame *frame, void *_mod) {
    CreatedModule *base = _mod;

    CreatedModule *mod = malloc(sizeof(CreatedModule) + base->type->size);
    mod->_mod.machine = base->_mod.machine;
    mod->_mod.getItem = base->get ? createdModuleGet : 0;
    mod->_mod.build = base->build ? createdModuleBuild : 0;
    mod->_mod.name = "auro.module.created";

    mod->type = base->type;
    mod->get = base->get;
    mod->build = base->build;

    memcpy(mod->value, &frame->data[frame->program[frame->pc++]], mod->type->size);
    if (mod->type->flags && NUGGET_TYPE_FLAG_GC) {
        gc_incref(*(void**)mod->value);
    }

    WRITE(mod);
}

nuggetItem precreatedModuleGet (nuggetModule *_mod, char *name) {
    CreatedModule *mod = _mod;
    SCASE(name, "") return FITEM(BuiltinFunctionNew(mod_create, "module.create", mod, 1, 1, mod->type, &module_t));
    return EMPTY_ITEM;
}

nuggetModule *buildCreateModuleModule (nuggetModule *_self, nuggetModule *argument) {
    nuggetItem typeItem = nuggetModuleGetItem(argument, "type");
    if (typeItem.kind != nuggetItemType) {
        pushErr(_self->machine, "No type item 'type' in module create argument");
        return 0;
    }
    nuggetItem getItem = nuggetModuleGetItem(argument, "get");
    nuggetItem buildItem = nuggetModuleGetItem(argument, "build");

    if (getItem.kind == nuggetItemEmpty && buildItem.kind == nuggetItemEmpty) {
        pushErr(_self->machine, "module create needs either 'get' or 'build' function");
        return 0;
    }

    if (getItem.kind != nuggetItemEmpty && buildItem.kind != nuggetItemEmpty) {
        pushErr(_self->machine, "Only one of either 'get' or 'build' argument for module create");
        return 0;
    }

    nuggetFunction *getFn, *buildFn;

    if (getItem.kind == nuggetItemFunction) {
        getFn = getItem.item;
        buildFn = 0;
    } else if (getItem.kind != nuggetItemEmpty) {
        pushErr(_self->machine, "module create argument 'get' is not a function %d", getItem.kind);
        return 0;
    } else if (buildItem.kind == nuggetItemFunction) {
        buildFn = buildItem.item;
        getFn = 0;
    } else {
        pushErr(_self->machine, "module create argument 'build' is not a function");
        return 0;
    }

    nuggetType *type = typeItem.item;
    nuggetType *tbuf[4];

    if (getFn && !SignatureEq(SignatureFromFunction(getFn), SignatureTmp(tbuf, 2, 1, type, &string_t, &item_t))) {
        pushErr(_self->machine, "invalid signature for 'get' argument for module create");
        return 0;
    }

    if (buildFn && !SignatureEq(SignatureFromFunction(buildFn), SignatureTmp(tbuf, 2, 1, type, &module_t, &module_t))) {
        pushErr(_self->machine, "invalid signature for 'build' argument for module create");
        return 0;
    }

    CreatedModule *mod = malloc(sizeof(CreatedModule));
    mod->_mod.machine = _self->machine;
    mod->_mod.getItem = precreatedModuleGet;
    mod->_mod.build = 0;
    mod->_mod.name = "auro.module.precreated<?>";

    mod->type = type;
    mod->get = getFn;
    mod->build = buildFn;

    return (nuggetModule*) mod;
}

nuggetModule *buildLiftModuleModule (nuggetModule *_mod, nuggetModule *argument) {

    nuggetItem fnItem = nuggetModuleGetItem(argument, "0");
    if (fnItem.kind != nuggetItemFunction) {
        pushErr(_mod->machine, "No function item '0' in module lift argument");
        return 0;
    }
    nuggetFunction *fn = fnItem.item;

    nuggetType *tbuf[2];
    if (!SignatureEq(SignatureFromFunction(fn), SignatureTmp(tbuf, 0, 1, &module_t))) {
        pushErr(_mod->machine, "invalid signature for module lift argument");
        return 0;
    }

    nuggetModule *result;

    // TODO: Check error
    nuggetFunctionArgs *args = nuggetFunctionArgsNew(fn);
    nuggetFunctionCall(_mod->machine, fn, args);

    if (_mod->machine->state == machineStateError) {
        printf("An error ocurred liftng a module from %s\n", fn->name);
        return 0;
    }
    nuggetFunctionArgsReadOutput(args, 0, &result);

    if (result == 0) {
        printf("MODULE LIFT FROM %s IS NULL!!\n", fn->name);
    }

    return result;
}

nuggetItem getModuleModule (nuggetModule *_m, char *name) {
    SCASE(name, "") return (nuggetItem) { kind: nuggetItemType, item: &module_t };
    SCASE(name, "get") return FITEM(BuiltinFunctionNew(mod_get, "module.get", 0, 2, 1, &module_t, &string_t, &item_t));
    SCASE(name, "build") return FITEM(BuiltinFunctionNew(mod_build, "module.build", 0, 2, 1, &module_t, &module_t, &module_t));
    SCASE(name, "new") {
        nuggetModule *mod = malloc(sizeof(nuggetModule));
        mod->getItem = 0;
        mod->build = buildNewModuleModule;
        return (nuggetItem) { kind: nuggetItemModule, item: mod };
    }
    SCASE(name, "create") {
        nuggetModule *mod = malloc(sizeof(nuggetModule));
        mod->machine = _m->machine;
        mod->getItem = 0;
        mod->build = buildCreateModuleModule;
        return (nuggetItem) { kind: nuggetItemModule, item: mod };
    }
    SCASE(name, "lift") {
        nuggetModule *mod = malloc(sizeof(nuggetModule));
        mod->machine = _m->machine;
        mod->getItem = 0;
        mod->build = buildLiftModuleModule;
        return (nuggetItem) { kind: nuggetItemModule, item: mod };
    }
    return EMPTY_ITEM;
}

nuggetModule *newModuleModule (nuggetMachine *machine) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->machine = machine;
    mod->getItem = getModuleModule;
    mod->build = 0;
    mod->name = "auro.module";
    return mod;
}





typedef struct {
    nuggetModule _mod;
    nuggetType *type;
} FnItemMod;

void fitem_new (stackFrame *frame, void *_mod) {
    READ(nuggetFunction*, fn);
    nuggetItem item = (nuggetItem) { kind: nuggetItemFunction, item: fn };
    WRITE(item);
}

nuggetItem getFnItemModule (nuggetModule *_m, char *name) {
    FnItemMod *mod = _m;
    SCASE(name, "") return FITEM(BuiltinFunctionNew(fitem_new, "item.function.new", 0, 1, 1, mod->type, &item_t));
    return EMPTY_ITEM;
}

nuggetModule *buildFnItemModule (nuggetModule *_self, nuggetModule *argument) {
    Signature sig;

    if (SignatureFromModule(argument, &sig)) return 0;

    FnDict *dict = FnDictGlobal();
    nuggetType *type = FnDictGet(dict, sig);   

    FnItemMod *mod = malloc(sizeof(FnItemMod));
    mod->_mod.getItem = getFnItemModule;
    mod->_mod.build = 0;
    mod->type = type;

    return (nuggetModule*) mod;
}



typedef struct {
    nuggetModule _mod;
    nuggetType *type;
} ConstItemMod;

void citem_new (stackFrame *frame, void *_mod) {
    nuggetType *type = ((ConstItemMod*) _mod)->type;
    Constant *cns = calloc(offsetof(Constant, result) + type->size, 1);

    cns->base = 0;
    cns->type = type;
    cns->executed = 1;
    memcpy(&cns->result, &frame->data[frame->program[frame->pc++]], type->size);

    cns->_fn.kind = nuggetFunctionConst;
    cns->_fn.name = "<auro.module.item.constant>";
    cns->_fn.inCount = 0;
    cns->_fn.outCount = 1;
    cns->_fn.outs = &cns->type;

    nuggetItem item = FITEM(cns);
    WRITE(item);
}

nuggetItem getConstItemModule (nuggetModule *_m, char *name) {
    ConstItemMod *mod = _m;
    SCASE(name, "") return FITEM(BuiltinFunctionNew(citem_new, "item.constant.new", mod, 1, 1, mod->type, &item_t));
    return EMPTY_ITEM;
}

nuggetModule *buildConstItemModule (nuggetModule *_self, nuggetModule *argument) {

    nuggetItem baseItem = nuggetModuleGetItem(argument, "0");
    if (baseItem.kind != nuggetItemType) {
        printf("No type item '' in constant item argument\n");
        return 0;
    }

    nuggetType *type = baseItem.item;

    ConstItemMod *mod = malloc(sizeof(ConstItemMod));
    mod->_mod.getItem = getConstItemModule;
    mod->_mod.build = 0;
    mod->type = type;

    return (nuggetModule*) mod;
}




void item_null (stackFrame *frame, void *_mod) {
    nuggetItem item = (nuggetItem) { kind: nuggetItemEmpty, item: 0 };
    WRITE(item);
}



void item_type_new (stackFrame *frame, void *_mod) {
    READ(nuggetType*, t);
    nuggetItem item = (nuggetItem) { kind: nuggetItemType, item: t };
    WRITE(item);
}

void item_type_get (stackFrame *frame, void *_mod) {
    READ(nuggetItem, it);
    if (it.kind != nuggetItemType) {
        frame->error = 1;
        nuggetPushErrorF(frame->machine, 0, "Item %s is not a type", itemString(it));
        return;
    }
    nuggetType *tp = it.item;
    WRITE(tp);
}

void item_type_test (stackFrame *frame, void *_mod) {
    READ(nuggetItem, it);
    char r = it.kind != nuggetItemType;
    WRITE(r);
}



void item_module (stackFrame *frame, void *_mod) {
    READ(nuggetModule*, m);
    nuggetItem item = (nuggetItem) { kind: nuggetItemModule, item: m };
    WRITE(item);
}

void item_isnull (stackFrame *frame, void *_mod) {
    READ(nuggetItem, item);
    char r = item.kind == nuggetItemEmpty;
    WRITE(r);
}

nuggetItem getItemModule (nuggetModule *_m, char *name) {
    SCASE(name, "") return (nuggetItem) { kind: nuggetItemType, item: &item_t };
    SCASE(name, "isnull") return FITEM(BuiltinFunctionNew(item_isnull, "item.isnull", 0, 1, 1, &item_t, &bool_t));
    SCASE(name, "null") return FITEM(BuiltinFunctionNew(item_null, "item.null", 0, 0, 1, &item_t));

    SCASE(name, "type") return FITEM(BuiltinFunctionNew(item_type_new, "item.type.new", 0, 1, 1, &type_t, &item_t));
    SCASE(name, "type\x1dnew") return FITEM(BuiltinFunctionNew(item_type_new, "item.type.new", 0, 1, 1, &type_t, &item_t));
    SCASE(name, "type\x1dget") return FITEM(BuiltinFunctionNew(item_type_get, "item.type.get", 0, 1, 1, &item_t, &type_t));
    SCASE(name, "type\x1dtest") return FITEM(BuiltinFunctionNew(item_type_test, "item.type.test", 0, 1, 1, &item_t, &bool_t));

    SCASE(name, "module") return FITEM(BuiltinFunctionNew(item_module, "item.module", 0, 1, 1, &module_t, &item_t));
    SCASE(name, "function") {
        nuggetModule *mod = malloc(sizeof(nuggetModule));
        mod->getItem = 0;
        mod->build = buildFnItemModule;
        return (nuggetItem) { kind: nuggetItemModule, item: mod };
    }
    SCASE(name, "constant") {
        nuggetModule *mod = malloc(sizeof(nuggetModule));
        mod->getItem = 0;
        mod->build = buildConstItemModule;
        return (nuggetItem) { kind: nuggetItemModule, item: mod };
    }

    return EMPTY_ITEM;
}

nuggetModule *newItemModule (nuggetMachine *machine) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = getItemModule;
    mod->build = 0;
    mod->machine = machine;
    mod->name = "auro.module.item";
    return mod;
}






typedef struct {
    nuggetModule _mod;
    nuggetType *base;
} NewTypeMod;

void type_new (stackFrame *frame, void *_mod) {
    NewTypeMod *mod = _mod;
    WRITE(mod->base);
}

nuggetItem getNewTypeModule (nuggetModule *_m, char *name) {
    SCASE(name, "") return FITEM(BuiltinFunctionNew(type_new, "type.new", _m, 0, 1, &type_t));
    return EMPTY_ITEM;
}

nuggetModule *buildNewTypeModule (nuggetModule *_self, nuggetModule *argument) {
    nuggetItem baseItem = nuggetModuleGetItem(argument, "0");
    // Try again with empty name. This should be deprecated
    if (baseItem.kind == nuggetItemEmpty) {
        baseItem = nuggetModuleGetItem(argument, "");
    }
    if (baseItem.kind != nuggetItemType) {
        printf("No type item '0' in new type argument\n");
        return 0;
    }

    nuggetType *base = baseItem.item;

    NewTypeMod *mod = malloc(sizeof(NewTypeMod));
    mod->_mod.getItem = getNewTypeModule;
    mod->_mod.build = 0;

    mod->base = base;

    return (nuggetModule*) mod;
}

void type_eq (stackFrame *frame, void *_mod) {
    READ(nuggetType*, a);
    READ(nuggetType*, b);
    char result = a->id == b->id;
    WRITE(result);
}

nuggetItem getTypeModule (nuggetModule *_m, char *name) {
    SCASE(name, "") return (nuggetItem) { kind: nuggetItemType, item: &type_t };
    SCASE(name, "eq") return FITEM(BuiltinFunctionNew(type_eq, "type.eq", 0, 2, 1, &type_t, &type_t, &bool_t));
    SCASE(name, "new") {
        nuggetModule *mod = malloc(sizeof(nuggetModule));
        mod->getItem = 0;
        mod->build = buildNewTypeModule;
        return (nuggetItem) { kind: nuggetItemModule, item: mod };
    }

    return EMPTY_ITEM;
}

nuggetModule *newTypeModule (nuggetMachine *machine) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = getTypeModule;
    mod->build = 0;
    mod->machine = machine;
    mod->name = "auro.type";
    return mod;
}



// This is non standard

void item_debug (stackFrame *frame, void *_mod) {
    READ(nuggetItem, item);
    char *repr = itemString(item);
    int len = strlen(repr) + 1;
    char *result = malloc(len);
    memcpy(result, repr, len);
    WRITE(result);
}

void any_debug (stackFrame *frame, void *_mod) {
    READ(Any*, val);
    char *repr = nuggetValueString(0, anyGetValuePtr(val), anyGetType(val));
    void *value = nuggetStringAlloc(repr);
    WRITE(value);
}

void shell_debug (stackFrame *frame, void *_mod) {
    startShell(frame->machine);
}

void debug_log (stackFrame *frame, void *_mod) {
    READ(char, flag);
    MonitorSetFlagValue(&frame->machine->monitor, MONITOR_LOG, flag);
}

int uid = 0;
void debug_uid (stackFrame *frame, void *_mod) {
    uid++;
    WRITE(uid);
}

nuggetItem getDebugModule (nuggetModule *_m, char *name) {
    SCASE(name, "any") return FITEM(BuiltinFunctionNew(any_debug, "nugget.debug.any", 0, 1, 1, &any_t, &string_t));
    SCASE(name, "item") return FITEM(BuiltinFunctionNew(item_debug, "nugget.debug.item", 0, 1, 1, &item_t, &string_t));
    SCASE(name, "shell") return FITEM(BuiltinFunctionNew(shell_debug, "nugget.debug.shell", 0, 0, 0));
    SCASE(name, "log") return FITEM(BuiltinFunctionNew(debug_log, "nugget.debug.log", 0, 1, 0, &bool_t));

    SCASE(name, "uid") return FITEM(BuiltinFunctionNew(debug_uid, "nugget.debug.log", 0, 0, 1, &int_t));
    return EMPTY_ITEM;
}

nuggetModule *newDebugModule (nuggetMachine *machine) {
    nuggetModule *mod = malloc(sizeof(nuggetModule));
    mod->getItem = getDebugModule;
    mod->build = 0;
    mod->machine = machine;
    mod->name = "nugget.debug";
    return mod;
}