#include <string.h>
#include <stdio.h>

#include "headers.h"

static inline int checkString (nuggetMeta node, char *s) {
    return node.kind == nuggetMetaString && strcmp(node.data.string, s) == 0;
}

void printNode (nuggetMeta node) {
    if (node.kind == nuggetMetaInt) {
        printf("%d", node.data.n);
    } else if (node.kind == nuggetMetaString) {
        printf("%s", node.data.string);
    } else if (node.kind == nuggetMetaList) {
        printf("(");
        for (int i = 0; i < node.data.list.count; ++i) {
            if (i > 0) printf(",");
            printNode(node.data.list.nodes[i]);
        }
        printf(")");
    } else {
        printf("[[ INVALID ]]");
    }
}

nuggetMeta *sourceMapFindRoot (nuggetMeta *_root) {
    if (!_root) return 0;
    nuggetMeta root = *_root;

    if (root.kind != nuggetMetaList) return 0;

    for (int i = 0; i < root.data.list.count; ++i) {
        nuggetMeta node = root.data.list.nodes[i];
        if (node.kind != nuggetMetaList) continue;
        if (node.data.list.count < 1) continue;

        nuggetMeta first = node.data.list.nodes[0];
        if (checkString(first, "source map")) {
            return &root.data.list.nodes[i];
        }
    }

    return 0;
}

char *sourceMapFile (nuggetMeta *_root) {
    if (!_root) return 0;
    nuggetMeta root = *_root;

    // Skip first node "source map"
    for (int i = 1; i < root.data.list.count; ++i) {
        nuggetMeta node = root.data.list.nodes[i];

        if (node.kind != nuggetMetaList) continue;
        if (node.data.list.count < 2) continue;

        nuggetMeta first = node.data.list.nodes[0];
        if (!checkString(first, "file")) continue;

        nuggetMeta second = node.data.list.nodes[1];
        if (second.kind != nuggetMetaString) continue;
        return second.data.string;
    }

    return 0;
}

nuggetMeta *sourceMapFindItem (nuggetMeta *_root, char* kind, int index) {
    if (!_root) return 0;
    nuggetMeta root = *_root;

    if (root.kind != nuggetMetaList) return 0;

    // Skip first node "source map"
    for (int i = 1; i < root.data.list.count; ++i) {
        nuggetMeta node = root.data.list.nodes[i];

        if (node.kind != nuggetMetaList) continue;
        if (node.data.list.count < 2) continue;

        nuggetMeta first = node.data.list.nodes[0];
        if (!checkString(first, kind)) continue;

        nuggetMeta second = node.data.list.nodes[1];
        if (second.kind == nuggetMetaInt && second.data.n == index) {
            return &root.data.list.nodes[i];
        }
    }

    return 0;
}

nuggetMeta *sourceMapFindFunction (nuggetMeta *_root, int index) {
    return sourceMapFindItem(_root, "function", index);
}

nuggetMeta *sourceMapFindType (nuggetMeta *_root, int index) {
    return sourceMapFindItem(_root, "type", index);
}

char *sourceMapItemName (nuggetMeta *_root) {
    if (!_root) return 0;
    nuggetMeta root = *_root;

    if (root.kind != nuggetMetaList) return 0;

    // Skip "function" and index nodes
    for (int i = 2; i < root.data.list.count; ++i) {
        nuggetMeta node = root.data.list.nodes[i];
        if (node.kind != nuggetMetaList) continue;
        if (node.data.list.count < 2) continue;

        nuggetMeta first = node.data.list.nodes[0];
        if (first.kind != nuggetMetaString) continue;
        if (strcmp(first.data.string, "name") != 0) continue;

        nuggetMeta second = node.data.list.nodes[1];
        if (second.kind != nuggetMetaString) continue;
        return second.data.string;
    }

    return 0;
}

char *sourceMapFunctionName (nuggetMeta *_root) {
    return sourceMapItemName(_root);
}

int sourceMapFunctionFindLine (nuggetMeta *_root, int index) {
    if (!_root) return -1;
    nuggetMeta root = *_root;
    if (root.kind != nuggetMetaList) return -1;

    // Line of the function declaration
    int fline = -1;

    // Find the code node
    // Skip "function" and index nodes
    for (int i = 2; i < root.data.list.count; ++i) {
        nuggetMeta node = root.data.list.nodes[i];
        if (node.kind != nuggetMetaList) continue;
        if (node.data.list.count < 1) continue;


        nuggetMeta first = node.data.list.nodes[0];

        if (checkString(first, "line") && node.data.list.count >= 2) {
            nuggetMeta second = node.data.list.nodes[1];
            if (second.kind != nuggetMetaInt) continue;
            fline = second.data.n;
            continue;
        }

        if (first.kind != nuggetMetaString) continue;
        if (strcmp(first.data.string, "code") != 0) continue;

        int highest = -1;
        int line;

        for (int i = 1; i < node.data.list.count; ++i) {
            nuggetMeta inode = node.data.list.nodes[i];
            if (inode.kind != nuggetMetaList) continue;
            if (inode.data.list.count < 2) continue;

            nuggetMeta first = inode.data.list.nodes[0];
            nuggetMeta second = inode.data.list.nodes[1];
            if (first.kind != nuggetMetaInt) continue;
            if (second.kind != nuggetMetaInt) continue;

            int ix = first.data.n;

            if (ix > highest && ix <= index) {
                highest = ix;
                line = second.data.n;
            }
        }

        if (highest >= 0) {
            return line;
        }
    }

    return fline;
}