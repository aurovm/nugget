
// TODO: Should be in nugget.h

typedef void* nuggetValPtr;

int nuggetValPtrSize (nuggetType *type, nuggetValPtr value);
void nuggetValPtrDrop (nuggetType *type, nuggetValPtr value);
void nuggetValPtrReadData (nuggetType *type, nuggetValPtr value, void *dst);

#define nuggetValPtrOverwrite nuggetValPtrCopy
int nuggetValPtrCopy (nuggetType *type, nuggetValPtr dst, nuggetValPtr src);