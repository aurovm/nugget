#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#include "headers.h"

#define KIND_DICT 0
#define KIND_GET 1
#define KIND_BUILD 2

typedef struct {
    char* name;
    nuggetItem item;
} CustomItem;

typedef struct {
    nuggetModule _mod;

    int kind;
    void *data;

    void *callback;
    CustomItem *items;

    int itemCapacity;
    int itemCount;
} CustomModule;

nuggetItem CustomModuleGetItem (nuggetModule *_m, char *name) {
    CustomModule *mod = (CustomModule*)_m;

    switch (mod->kind) {
        case KIND_DICT: {
            for (int i = 0; i < mod->itemCount; ++i) {
                if (strcmp(mod->items[i].name, name) == 0) {
                    return mod->items[i].item;
                }
            }
            break;
        }
        case KIND_GET: {
            return ((nuggetModuleGetter) mod->callback)(0, name, mod->data);
        }
    }

    return (nuggetItem) { kind: nuggetItemEmpty, item: 0 };
}

nuggetModule *CustomModuleBuild (nuggetModule *_m, nuggetModule *argument) {
    CustomModule *mod = (CustomModule*)_m;

    if (mod->kind != KIND_BUILD) {
        printf("Error: Custom module %s cannot be built\n", mod->_mod.name);
    }

    return ((nuggetModuleBuilder) mod->callback)(0, argument, mod->data);
}


nuggetModule *nuggetModuleCreate (char *name, void *data) {
    CustomModule *mod = malloc(sizeof(CustomModule));

    mod->_mod.name = name;
    mod->_mod.getItem = CustomModuleGetItem;
    mod->_mod.build = CustomModuleBuild;
    // TODO: Accept a machine parameter
    mod->_mod.machine = 0;

    mod->kind = KIND_DICT;

    mod->data = data;
    mod->itemCapacity = 16;
    mod->items = malloc(sizeof(CustomItem) * mod->itemCapacity);
    mod->itemCount = 0;

    return (nuggetModule*) mod;
}

nuggetModule *nuggetModuleCreateGet (char *name, nuggetModuleGetter cb, void *data) {
    CustomModule *mod = malloc(sizeof(CustomModule));

    mod->_mod.name = name;
    mod->_mod.getItem = CustomModuleGetItem;
    mod->_mod.build = CustomModuleBuild;
    // TODO: Accept a machine parameter
    mod->_mod.machine = 0;

    mod->kind = KIND_GET;
    mod->data = data;
    mod->callback = cb;

    return (nuggetModule*) mod;
}

nuggetModule *nuggetModuleCreateBuild (char *name, nuggetModuleBuilder cb, void *data) {
    CustomModule *mod = malloc(sizeof(CustomModule));

    mod->_mod.name = name;
    mod->_mod.getItem = CustomModuleGetItem;
    mod->_mod.build = CustomModuleBuild;
    // TODO: Accept a machine parameter
    mod->_mod.machine = 0;

    mod->kind = KIND_BUILD;
    mod->data = data;
    mod->callback = cb;

    return (nuggetModule*) mod;
}

void nuggetModuleAddItem (nuggetModule *_m, char *name, nuggetItem item) {
    CustomModule *mod = (CustomModule*)_m;

    if (mod->itemCount >= mod->itemCapacity) {
        mod->itemCapacity += 16;
        mod->items = realloc(mod->items, sizeof(CustomItem) * mod->itemCapacity);
    }

    mod->items[mod->itemCount++] = (CustomItem) { name: name, item: item };
}

void nuggetModuleAddFunction (nuggetModule *_m, char *name, nuggetFunction *fn) {
    nuggetModuleAddItem(_m, name, (nuggetItem) { kind: nuggetItemFunction, item: fn });
}

void nuggetModuleAddType (nuggetModule *_m, char *name, nuggetType *tp) {
    nuggetModuleAddItem(_m, name, (nuggetItem) { kind: nuggetItemType, item: tp });
}


extern nuggetFunction *nuggetFunctionCreate (void (*impl)(nuggetFunctionArgs *params, void *data), char *name, void *data) {
    CustomFunction *fn = malloc(sizeof(CustomFunction));

    fn->_fn.kind = nuggetFunctionCustom;
    fn->_fn.name = name;
    fn->_fn.inCount = 0;
    fn->_fn.outCount = 0;
    fn->_fn.ins = malloc(sizeof(void*) * 16);
    fn->_fn.outs = malloc(sizeof(void*) * 1);

    fn->impl = impl;
    fn->data = data;
    fn->inCapacity = 16;
    fn->outCapacity = 1;

    return fn;
}

char *nuggetFunctionGetName (nuggetFunction *f) { return f->name; }


void nuggetFunctionAddInput (nuggetFunction *_f, nuggetType *type) {
    CustomFunction *fn = (CustomFunction*) _f;

    if (fn->_fn.inCount >= fn->inCapacity) {
        fn->inCapacity += 8;
        fn->_fn.ins = realloc(fn->_fn.ins, sizeof(void*) * fn->inCapacity);
    }

    fn->_fn.ins[fn->_fn.inCount++] = type;
}

void nuggetFunctionAddOutput (nuggetFunction *_f, nuggetType *type) {
    CustomFunction *fn = (CustomFunction*) _f;

    if (fn->_fn.inCount >= fn->outCapacity) {
        fn->outCapacity += 8;
        fn->_fn.outs = realloc(fn->_fn.outs, sizeof(void*) * fn->outCapacity);
    }

    fn->_fn.outs[fn->_fn.outCount++] = type;
}
