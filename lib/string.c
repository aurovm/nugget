#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "headers.h"
#include "gc.h"

nuggetString *nuggetStringAlloc (char *content) {
	void *value = gc_alloc(&string_t, strlen(content) + 1);
	strcpy(value, content);
	return value;
}