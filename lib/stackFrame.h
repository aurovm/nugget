
void writeRef (stackFrame *frame, void *newval);
#define writeReg stackFrameWriteReg
int writeVal (void *dst, void *src, nuggetType *type);
void dropVal (void *ptr, nuggetType *type);

int stackFrameGetRegFlag (stackFrame *frame, int index);
void stackFrameSetRegFlag (stackFrame *frame, int index, int val);

int stackFrameWriteReg (stackFrame *frame, int addr, void *srcvalptr);
int stackFrameReadReg (stackFrame *frame, int addr, void *dstvalptr);

#define readArg stackFrameReadArg
void stackFrameReadArg (stackFrame *frame, void *dst);

#define writeOut stackFrameWriteOut
void stackFrameWriteOut (stackFrame *frame, void *src);

int stackFrameTransferReg (
	stackFrame *dstFrame, int dstAddr,
	stackFrame *srcFrame, int srcAddr
);
