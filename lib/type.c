#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"

// from nugget.h

nuggetType *nuggetTypeNew(char* name, int size) {
    nuggetType *tp = calloc(sizeof(nuggetType), 1);
    tp->id = nextTypeId();
    tp->name = name;
    tp->size = size;
    return tp;
}

// TODO: Remove this one
nuggetType *nuggetTypeCreate (char *name, int size, void *data) {
    nuggetType *type = calloc(sizeof(nuggetType), 1);
    type->id = nextTypeId();
    type->size = size;
    type->name = name;
    type->data = data;
    return type;
}

int nuggetTypeGetId(nuggetType *tp) { return tp->id; }
char *nuggetTypeGetName(nuggetType *tp) { return tp->name; }
int nuggetTypeGetSize(nuggetType *tp) { return tp->size; }
int nuggetTypeEq(nuggetType *a, nuggetType *b) {
    if (!a || !b) return 0;
    if (a->id == b->id) { return 1; }

    // Record comparison
    if (a->flags & b->flags & NUGGET_TYPE_FLAG_RECORD) {
        return recordTypeEquals(a, b);
    }

    // Null comparison
    if (a->flags & b->flags & NUGGET_TYPE_FLAG_NULL) {
        return nullTypeEquals(a, b);
    }

    if (a->family && a->family == b->family) {
        return a->family->eq(a, b, a->family->userData);
    }

    return 0;
}

// end nugget.h
