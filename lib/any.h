
typedef struct {
    nuggetType *type;
} Any;

Any *anyNew (nuggetType *type);
nuggetType *anyGetType (Any *any);
void *anyGetValuePtr (Any *any);
