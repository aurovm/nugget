#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "headers.h"
#include "gc.h"
#include "value.h"
#include "stackFrame.h"

void writeRef (stackFrame *frame, void *newval) {
    int addr = frame->program[frame->pc++];

    void *oldval;
    memcpy(&oldval, &frame->data[addr], sizeof(void*));

    if (stackFrameGetRegFlag(frame, addr)) {
        gc_decref(oldval);
    }

    memcpy(&frame->data[addr], &newval, sizeof(void*));

    stackFrameSetRegFlag(frame, addr, 1);
}

int stackFrameWriteReg (stackFrame *frame, int addr, void *src) {
    // TODO: Should decref here?
    nuggetType *type = frame->types[addr];
    int written = writeVal(&frame->data[addr], src, type);
    stackFrameSetRegFlag(frame, addr, 1);
    return written;
}

int stackFrameReadReg (stackFrame *frame, int addr, void *dst) {
    nuggetType *type = frame->types[addr];
    int iscopy = type->flags & NUGGET_TYPE_FLAG_COPY;
    if (iscopy || stackFrameGetRegFlag(frame, addr)) {
        return writeVal(dst, &frame->data[addr], type);
    } else {
        nuggetPushErrorF(
            frame->machine,
            42 + 12 + strlen(type->name),
            "Uninitialized value (%s), at stack address %d",
            type->name, addr
        );
        return 0;
    }
}

int stackFrameTransferReg (stackFrame *dstFrame, int dstAddr, stackFrame *srcFrame, int srcAddr) {
    void *dst = &dstFrame->data[dstAddr];
    int written = stackFrameReadReg(srcFrame, srcAddr, dst);
    if (written) stackFrameSetRegFlag(dstFrame, dstAddr, 1);
    return written;
}

void stackFrameReadArg (stackFrame *frame, void *dst) {
    int addr = frame->program[frame->pc++];
    stackFrameReadReg(frame, addr, dst);
}

void stackFrameWriteOut (stackFrame *frame, void *src) {
    int addr = frame->program[frame->pc++];
    stackFrameWriteReg(frame, addr, src);
}


int writeVal (void *dst, void *src, nuggetType *type) {
    return nuggetValPtrCopy(type, dst, src);
}

void dropVal (void *ptr, nuggetType *type) {
    nuggetValPtrDrop(type, ptr);
}



int stackFrameGetRegFlag (stackFrame *frame, int index) {
    int addr = frame->program[index];

    int groupSize = sizeof(int) * 4;
    int groupIndex = index / groupSize;
    int localIndex = index - groupIndex;

    int group = frame->dataset[groupIndex];
    int mask = 1 << localIndex;
    return group & mask && 1;
}

void stackFrameSetRegFlag (stackFrame *frame, int index, int val) {
    int addr = frame->program[index];

    int groupSize = sizeof(int) * 4;
    int groupIndex = index / groupSize;
    int localIndex = index - groupIndex;

    int group = frame->dataset[groupIndex];
    int mask = 1 << localIndex;
    if (val) {
        group |= mask;
    } else {
        group &= ~mask;
    }

    frame->dataset[groupIndex] = group;
}
