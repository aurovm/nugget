#include <stdlib.h>
#include <stddef.h>

#include "headers.h"

inline nuggetBuffer *nuggetBufferAlloc(int size) {
    nuggetBuffer *buf = calloc(offsetof(nuggetBuffer, data) + size, 1);
    buf->size = size;
    return buf;
}

inline int nuggetBufferGetSize(nuggetBuffer *buf) {
    return buf->size;
}

inline char* nuggetBufferGetData(nuggetBuffer *buf) {
    return buf->data;
}

inline char nuggetBufferGet(nuggetBuffer *buf, int index) {
    return buf->data[index];
}

inline void nuggetBufferSet(nuggetBuffer *buf, int index, char value) {
    buf->data[index] = value;
}
