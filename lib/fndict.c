#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"
#include "fndict.h"
#include "debug.h"


#define OUT_CAP 8
#define IN_CAP 24

void SignaturePrint (Signature a) {
    printf("(");
    for (int i = 0; i < a.inCount; ++i) {
        if (i > 0) printf(", ");
        char *name = a.ins[i]->name;
        if (!name) printf("#%d", a.ins[i]->id);
        else printf("%s", name);
    }
    printf(") -> (");

    for (int i = 0; i < a.outCount; ++i) {
        if (i > 0) printf(", ");
        char *name = a.outs[i]->name;
        if (!name) printf("#%d", a.outs[i]->id);
        else printf("%s", name);
    }

    printf(")\n");
}

int SignatureEq (Signature a, Signature b) {
    if (a.inCount == b.inCount && a.outCount && b.outCount) {
        for (int i = 0; i < a.inCount; ++i) {
            if (a.ins[i]->id != b.ins[i]->id) {
                return 0;
            }
        }

        for (int i = 0; i < a.outCount; ++i) {
            if (a.outs[i]->id != b.outs[i]->id) {
                return 0;
            }
        }

        return 1;
    }
    return 0;
}

int SignatureFromModule (nuggetModule *mod, Signature *dest) {
    nuggetType *_types[IN_CAP + OUT_CAP];

    int inCap = IN_CAP;
    int outCap = OUT_CAP;

    int inCount = 0;
    int outCount = 0;

    nuggetType **ins = _types;
    nuggetType **outs = _types + inCap;

    while (1) {
        if (inCount >= inCap) {
            printf("TODO: Increase signature capacity\n");
            goto cleanup;
        }

        char sindex[6];
        sprintf(sindex, "in%d", inCount);
        nuggetItem baseItem = nuggetModuleGetItem(mod, sindex);
        if (baseItem.kind == nuggetItemEmpty) { break; }
        if (baseItem.kind != nuggetItemType) {
            printf("argument '%s' is not a type\n", sindex);
            goto cleanup;
        }

        ins[inCount++] = baseItem.item;
    }

    while (1) {
        if (outCount >= outCap) {
            printf("TODO: Increase signature capacity\n");
            goto cleanup;
        }

        char sindex[7];
        sprintf(sindex, "out%d", outCount);
        nuggetItem baseItem = nuggetModuleGetItem(mod, sindex);
        if (baseItem.kind == nuggetItemEmpty) { break; }
        if (baseItem.kind != nuggetItemType) {
            printf("argument '%s' is not a type\n", sindex);
            goto cleanup;
        }

        outs[outCount++] = baseItem.item;
    }

    nuggetType *alloc = malloc(sizeof(void*) * (inCount + outCount));
    int inSize = sizeof(void*) * inCount;
    int outSize = sizeof(void*) * outCount;
    memcpy(alloc, ins, inSize);
    memcpy(alloc + inCount, outs, outSize);

    *dest = (Signature) {
        .inCount = inCount,
        .outCount = outCount,
        .ins = alloc,
        .outs = alloc + inCount,
    };
    return 0;


    cleanup:
    return 1;
}

Signature SignatureFromFunction (nuggetFunction *fn) {
    return (Signature) {
        .inCount = fn->inCount,
        .outCount = fn->outCount,
        .ins = fn->ins,
        .outs = fn->outs,
    };
}

Signature SignatureTmp (nuggetType **tbuf, int inCount, int outCount, ...) {
    int tcount = inCount + outCount;

    va_list args;
    va_start( args, outCount );
    for (int i = 0; i < tcount; i++) {
        tbuf[i] = va_arg(args, nuggetType*);
    }
    va_end( args );

    return (Signature) {
        .inCount = inCount,
        .outCount = outCount,

        .ins = &tbuf[0],
        .outs = &tbuf[inCount],
    };
}




typedef struct FnDictNode {
    Signature sig;
    nuggetType *type;
    struct FnDictNode *next;
} FnDictNode;

struct FnDict {
    FnDictNode *first;
};

FnDict *FnDictNew () {
    FnDict *dict = malloc(sizeof(FnDict));
    dict->first = 0;
    return dict;
}

FnDict _FnDictGlobal = (FnDict){.first=0};

FnDict *FnDictGlobal () { return &_FnDictGlobal; }

nuggetType *FnDictGet (FnDict *dict, Signature sig) {
    FnDictNode **next = &dict->first;
    while (*next) {
        if (SignatureEq(sig, (*next)->sig)) {
            return (*next)->type;
        }
        next = &(*next)->next;
    }

    nuggetType *type = malloc(sizeof(nuggetType));
    type->id = nextTypeId();
    type->size = sizeof(void*);
    type->name = malloc(13);
    sprintf(type->name, "function_%d", type->id);

    *next = malloc(sizeof(FnDictNode));
    **next = (FnDictNode) {
        .sig = sig,
        .type = type,
        .next = 0,
    };

    return type;
}