#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdarg.h>

#include "headers.h"
#include "sourcemap.h"
#include "debug.h"

void nuggetPushError (nuggetMachine *machine, char *msg) {
    machine->state = machineStateError;

    ErrorFrame *frame = calloc(sizeof(ErrorFrame), 1);
    frame->kind = ERROR_FRAME_EXEC;
    frame->msg = msg;
    frame->stackFrame = machine->frame;
    frame->prev = machine->err;

    //printf("Error %p -> %p\n", frame->prev, frame);

    machine->err = frame;
}

void nuggetPushErrorF (nuggetMachine *machine, int n, const char *format, ...) {
    int size = (n > 0) ? n : strlen(format) + 256;
    char *msg = malloc(size);

    va_list args;
    va_start( args, format );
    vsprintf( msg, format, args );
    va_end( args );

    nuggetPushError(machine, msg);
}

void printErrorFrame (ErrorFrame *err) {
    if (!err) return;

    // Print first the first frames
    printErrorFrame(err->prev);

    if (err->msg) {
        printf("ERROR: %s\n", err->msg);
    }

    if (err->kind == ERROR_FRAME_EXEC) {

        stackFrame *frame = err->stackFrame;
        while (frame) {
            printf("at ");
            printFrameLocation(frame);
            frame = frame->parent;
        }
    } else if (err->kind == ERROR_FRAME_LOAD) {
        printf("loading %s\n", err->loadLoc);
    } else {
        printf("* unrecognized location (error frame kind %d)\n", err->kind);
    }
}

void nuggetPrintError (nuggetMachine *machine) {
    if (!machine->err) return;
    printErrorFrame(machine->err);
}

void pushLoadError (nuggetMachine *machine, char *msg, char *loc) {
    nuggetPushError(machine, msg);
    machine->err->kind = ERROR_FRAME_LOAD;
    machine->err->loadLoc = loc;
}

int nuggetHasError (nuggetMachine *machine) {
    return machine->err && 1;
}
