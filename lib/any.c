#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdarg.h>

#include "headers.h"
#include "any.h"
#include "gc.h"

Any *anyNew (nuggetType *type) {
    Any *any = gc_alloc(&any_t, sizeof(Any) * 2 + type->size);
    any->type = type;
    return any;
}

nuggetType *anyGetType (Any *any) {
    return any->type;
}

void *anyGetValuePtr (Any *any) {
    return any+1;
}
