#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include "headers.h"
#include "gc.h"
//#include "type.h"


//#define DEBUG(...) printf(__VA_ARGS__)
#define DEBUG(...)


#define MAGIC 0xB0CADA50
#define MAGIC_FREED 0xACABAD00

static int idCount = 0;
static int totalAllocated = 0;
static int peakAllocated = 0;

const int maxMemory = 1024 * 1024 * 200; // 200MB

typedef struct Value {
    nuggetType *type;
    int magic;
    int id;
    int count;
    int size;
} Value;

extern int gc_isValid (void *gcvalue) {
    if (!gcvalue) return 0;
    Value *val = gcvalue; val--;
    return val->magic == MAGIC;
}

extern void *gc_alloc (nuggetType *type, int size) {
    size += sizeof(Value[2]);

    if (totalAllocated + size > maxMemory) {
        printf("ERROR: Max memory allocated\n");
        abort();
    }

    Value *val = calloc(size, 1);
    val->type = type;
    val->magic = MAGIC;
    val->id = ++idCount;
    val->count = 1;
    val->size = size;

    totalAllocated += size;

    DEBUG("[gc] alloc %s #%d (%d bytes)\n", type->name, val->id, size);
    return val+1;
}

extern void gc_incref (void *gcvalue) {
    Value *val = gcvalue; val--;
    assert(gcvalue && val->magic == MAGIC);
    val->count++;
    DEBUG("[gc] ref+ #%d → %d\n", val->id, val->count);
}

extern void gc_decref (void *gcvalue) {
    Value *val = gcvalue; val--;
    assert(gcvalue && val->magic == MAGIC);
    val->count--;
    DEBUG("[gc] ref- #%d → %d\n", val->id, val->count);
    if (val->count <= 0) {
        DEBUG("[gc] free #%d\n", val->id);
        if (val->magic == MAGIC_FREED) {
            printf("ERROR: Double free\n");
            abort();
            //exit(1);
        }
        val->magic = MAGIC_FREED;
        if (val->type->drop) {
            val->type->drop(0, gcvalue, val->type->data);
        }
        totalAllocated -= val->size;
        //free(val);
    }
}

extern int gc_totalAllocated () {
    return totalAllocated;
}
