#ifndef __NUGGET_COL_ARRAY__
#define __NUGGET_COL_ARRAY__

typedef struct {
	int count;
	int capacity;
	void *data;
}  col_Array;

//#define DEFINE_ARRAY(ARRAY_NAME, INNER_TYPE) typedef col_Array* ARRAY_NAME;

#define DEFINE_ARRAY(ARRAY_NAME, INNER_TYPE) \
	typedef col_Array* ARRAY_NAME; \
	typedef INNER_TYPE ARRAY_NAME##Base; \
	ARRAY_NAME ARRAY_NAME##New (int capacity) {\
		col_Array *this = malloc(sizeof(col_Array));\
		this->data = malloc(capacity * sizeof(INNER_TYPE));\
		this->capacity = capacity; this->count = 0;\
		return this;}\
	int ARRAY_NAME##Push (ARRAY_NAME this, INNER_TYPE value) {\
		if (this->count >= this->capacity) {\
			this->capacity = this->capacity * 3 / 2;\
			this->data = realloc(this->data, this->capacity * sizeof(INNER_TYPE));\
		} ((INNER_TYPE*)(this->data))[this->count++] = value;\
		return this->count;}\
	int ARRAY_NAME##Count (ARRAY_NAME this) {\
		return this->count;}\
	void ARRAY_NAME##Set (ARRAY_NAME this, int index, INNER_TYPE value) {\
		((INNER_TYPE*)(this->data))[index] = value;}\
	INNER_TYPE ARRAY_NAME##Get (ARRAY_NAME this, int index) {\
		return ((INNER_TYPE*)(this->data))[index];}\
	INNER_TYPE ARRAY_NAME##Remove (ARRAY_NAME this, int index) {\
		INNER_TYPE* data = this->data;\
		INNER_TYPE oldValue = data[index];\
		data[index] = data[--this->count];\
		return oldValue;}\


#define DEFINE_ARRAY_SEARCH(ARRAY_NAME, VAR_ITEM, VAR_SEARCH, SEARCH_EXPRESSION) \
	int ARRAY_NAME##FindIndex (ARRAY_NAME this, ARRAY_NAME##Base VAR_SEARCH) {\
		for (int i = 0; i < ARRAY_NAME##Count(this); i++) {\
			ARRAY_NAME##Base VAR_ITEM = ARRAY_NAME##Get(this, i);\
			int matches = SEARCH_EXPRESSION;\
			if (matches) return i;\
		} return -1;}\

#endif