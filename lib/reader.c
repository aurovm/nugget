#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "headers.h"

#define SIGNATURE "Auro 0.6"

typedef struct Reader {
    char buffer[512];
    int size;
    int pos;
    FILE* file;
} Reader;


void refill (Reader *file) {
    file->size = fread(file->buffer, 1, 512, file->file);
    if (file->size == 0) {
        file->size = -1;
    }
    file->pos = 0;
}

int rbyte (Reader *file) {
    if (file->pos >= file->size) {
        refill(file);
    }
    if (file->size < 0) { return 0; }
    return file->buffer[file->pos++];
}

int rnum (Reader *file) {
    int n = 0;
    int b = rbyte(file);
    while ((b & 0x80) > 0) {
        n = (n << 7) + (b & 0x7f);
        b = rbyte(file);
    }
    return (n << 7) + b;
}

int rbytes (Reader *file, int size, char* buf) {
    char *p = buf;

    while (1) {
        int left = file->size - file->pos;
        if (left < size) {
            memcpy(p, file->buffer + file->pos, left);
            size -= left;
            p += left;
            refill(file);

            if (file->size < 0) {
                return 0;
            }
        } else {
            memcpy(p, file->buffer + file->pos, size);
            file->pos += size;
            return 1;
        }
    }
}

char* rstr (Reader *file) {
    int size = rnum(file);
    if (file->size < 0) { return 0; }

    char *result = malloc(size + 1);
    if (rbytes(file, size, result)) {
        result[size] = 0;
        return result;
    } else {
        free(result);
        return 0;
    }
}

nuggetMeta readMetaNode (Reader *file) {
    int val = rnum(file);

    nuggetMeta node;

    if (val & 1) {
        node.kind = nuggetMetaInt;
        node.data.n = val >> 1;
    } else if (val & 2) {
        int count = val >> 2;

        node.kind = nuggetMetaString;
        node.data.string = malloc(count + 1);
        rbytes(file, count, node.data.string);
        node.data.string[count] = 0;
    } else {
        int count = val >> 2;

        node.kind = nuggetMetaList;
        node.data.list.count = count;
        node.data.list.nodes = malloc(sizeof(nuggetMeta) * count);
        for (int i = 0; i < count; ++i) {
            node.data.list.nodes[i] = readMetaNode(file);
        }
    }

    return node;
}

nuggetModuleDesc *nuggetReadFile (char *path) {

    Reader file;
    file.file = fopen(path, "rb");

    #define CHECK { if (file.size < 0) goto eof; }

    if (file.file == 0) {
        setErr("Can't open file"); goto close;
    }

    refill(&file);
    if (file.size < 0) goto eof;

    if (strcmp(file.buffer, SIGNATURE) != 0) {
        setErr("Invalid signature"); goto close;
    }
    file.pos += strlen(SIGNATURE) + 1;

    nuggetModuleDesc mod;

    mod.moduleCount = rnum(&file);
    mod.modules = calloc(sizeof(nuggetModuleItem), mod.moduleCount);

    for (int i = 0; i < mod.moduleCount; i++) {
        nuggetModuleItem item;
        item.kind = rbyte(&file);

        switch (item.kind) {
            case nuggetModuleItemImport:
                item.data.import = rstr(&file);
                break;
            case nuggetModuleItemDefine:
                int count = rnum(&file);
                item.data.define.count = count;
                item.data.define.items = calloc(sizeof(nuggetDefineItemDesc), count);
                for (int j = 0; j < count; j++) {
                    item.data.define.items[j] = (nuggetDefineItemDesc) {
                        kind: rbyte(&file),
                        index: rnum(&file),
                        name: rstr(&file),
                    };
                }
                break;
            case nuggetModuleItemBuild:
                item.data.build.base = rnum(&file);
                item.data.build.argument = rnum(&file);
                break;
            case nuggetModuleItemUse:
                item.data.use.base = rnum(&file);
                item.data.use.name = rstr(&file);
                break;
            case nuggetModuleItemContext:
                break;
            default:
                setErr("Unknown module kind %d", item.kind);
                goto close;
        }

        CHECK;

        mod.modules[i] = item;
    }

    mod.typeCount = rnum(&file);
    mod.types = calloc(sizeof(nuggetTypeItem), mod.typeCount);

    for (int i = 0; i < mod.typeCount; i++) {
        int kind = rnum(&file);

        if (kind == 0) {
            mod.types[i] = (nuggetTypeItem) { module: 0, name: 0 };
        } else {
            mod.types[i] = (nuggetTypeItem) { module: kind - 1, name: rstr(&file) };
        }

        CHECK;
    }

    mod.functionCount = rnum(&file);
    mod.functions = calloc(sizeof(nuggetFunctionItem), mod.functionCount);

    for (int i = 0; i < mod.functionCount; i++) {
        int kind = rnum(&file);
        nuggetFunctionItem item;

        item.inCount = rnum(&file);
        item.ins = calloc(sizeof(int), item.inCount);
        for (int j = 0; j < item.inCount; j++) {
            item.ins[j] = rnum(&file);
        }

        item.outCount = rnum(&file);
        item.outs = calloc(sizeof(int), item.outCount);
        for (int j = 0; j < item.outCount; j++) {
            item.outs[j] = rnum(&file);
        }

        switch (kind) {
            case 0:
                item.kind = nuggetFunctionEmpty;
                break;
            case 1:
                item.kind = nuggetFunctionCode;
                item.data.code.count = 0;
                break;
            default:
                item.kind = nuggetFunctionImport;
                item.data.import.module = kind - 2;
                item.data.import.name = rstr(&file);
        }

        CHECK;

        mod.functions[i] = item;
    }

    int fcount = mod.functionCount;
    int ccount = rnum(&file);
    mod.functionCount += ccount;
    mod.functions = realloc(mod.functions, sizeof(nuggetFunctionItem) * mod.functionCount);

    for (int i = 0; i < ccount; i++) {
        int kind = rnum(&file);

        nuggetFunctionItem item;
        item.inCount = 0;
        item.outCount = 1;
        item.outs = malloc(sizeof(int));
        item.outs[0] = -1;

        switch (kind) {
            case 1:
                item.kind = nuggetFunctionInt;
                item.data.num = rnum(&file);
                break;
            case 2:
                item.kind = nuggetFunctionBin;

                int size = rnum(&file);
                char* bytes = calloc(1, size);
                rbytes(&file, size, bytes);

                item.data.bin.size = size;
                item.data.bin.bytes = bytes;
                break;
            default:
                if (kind < 16) {
                    char *msg = malloc(256);
                    mod.functionCount -= ccount - i;
                    sprintf(msg, "Unknown constant kind: %d, at %s Constant[%d]", kind, path, fcount+i);
                    setErr(msg);
                    goto close;
                }

                item.kind = nuggetFunctionConstCall;
                int index = kind - 16;
                item.data.call.fn = index;

                int pcount;
                if (index < fcount) {
                    pcount = mod.functions[index].inCount;
                } else {
                    pcount = 0;
                }

                item.data.call.paramCount = pcount;
                item.data.call.params = calloc(pcount, sizeof(int));
                for (int j = 0; j < pcount; j++) {
                    item.data.call.params[j] = rnum(&file);
                }
        }

        CHECK;

        mod.functions[fcount + i] = item;
    }

    for (int i = 0; i < fcount; i++) {
        nuggetFunctionItem *item = &mod.functions[i];
        if (item->kind == nuggetFunctionCode) {
            int count = rnum(&file);
            nuggetInstDesc *code = calloc(sizeof(nuggetInstDesc), count);
            for (int j = 0; j < count; j++) {
                nuggetInstDesc inst;

                inst.kind = rnum(&file);
                switch (inst.kind) {
                    case nuggetInstHlt:
                    case nuggetInstVar:
                        break;
                    case nuggetInstDup:
                    case nuggetInstJmp:
                        inst.data.args[0] = rnum(&file);
                        break;
                    case nuggetInstSet:
                    case nuggetInstJif:
                    case nuggetInstNif:
                        inst.data.args[0] = rnum(&file);
                        inst.data.args[1] = rnum(&file);
                        break;
                    case nuggetInstEnd: {
                        int count = item->outCount;
                        int *args = calloc(sizeof(int), count);
                        for (int k = 0; k < count; k++) {
                            args[k] = rnum(&file);
                        }
                        inst.data.call.argCount = count;
                        inst.data.call.args = args;
                        break;
                    }
                    default: {
                        if (inst.kind < 16) {
                            char *msg = malloc(256);
                            sprintf(msg, "Unknown instruction: %d, at %s Function[%d]:%d", inst.kind, path, i, j);
                            setErr(msg);
                            goto close;
                        }


                        int index = inst.kind - 16;
                        if (index >= mod.functionCount) {
                            nuggetPrintModule(&mod);
                            char *msg = malloc(256);
                            sprintf(msg, "Invalid function index: %d, at %s Function[%d]:%d", index, path, i, j);
                            setErr(msg);
                            goto close;
                        }


                        int count = mod.functions[index].inCount;
                        int *args = calloc(sizeof(int), count);
                        for (int k = 0; k < count; k++) {
                            args[k] = rnum(&file);
                        }

                        inst.kind = nuggetInstCall;
                        inst.data.call.fn = index;
                        inst.data.call.argCount = count;
                        inst.data.call.args = args;
                    }
                }

                code[j] = inst;
            }
            CHECK;

            item->data.code.count = count;
            item->data.code.code = code;
        }
    }

    mod.meta = readMetaNode(&file);
    CHECK;

    #undef CHECK
    fclose(file.file);

    nuggetModuleDesc *rmod = malloc(sizeof(nuggetModuleDesc));
    *rmod = mod;

    return rmod;

    eof:
    setErr("Unexpected end of file");

    close:
    //nuggetPrintModule(&mod);
    fclose(file.file);
    return 0;
}