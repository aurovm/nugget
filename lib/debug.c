#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include "headers.h"
#include "sourcemap.h"

#include "load.h"
#include "any.h"
#include "monitor.h"

char tmp_s[256];
char *kindName[4] = {"null", "module", "type", "function"};

char *itemString (nuggetItem item) {
    if (item.kind == nuggetItemEmpty) return "<null item>";
    if (item.kind == nuggetItemError) return "<error item>";

    if (item.item == 0) {
        sprintf(tmp_s, "<null %s>", kindName[item.kind]);
        return tmp_s;
    }

    char *name =
        item.kind == nuggetItemModule   ? ((nuggetModule*)   item.item)->name :
        item.kind == nuggetItemType     ? ((nuggetType*)     item.item)->name :
        item.kind == nuggetItemFunction ? ((nuggetFunction*) item.item)->name :
        -1;

    if (name == -1) return "<invalid item>";
    sprintf(tmp_s, "<%s %s>", kindName[item.kind], name);
    return tmp_s;
}

// TODO: Deprecated
// This function is only used in one place,
// The real implementation is nuggetValueString.
char *anyString (void *_val) {
    Any *val = _val;
    return nuggetValueString(0, anyGetValuePtr(val), anyGetType(val));
}


void printFrameLocation (stackFrame *frame) {
    nuggetFunction *fn = frame->fn;

    if (fn->kind == nuggetFunctionCompiled) {

        compiledFunction *cfn = (compiledFunction*) fn;

        int index = -1;
        // TODO: Binary search
        for (int i = 0; i < cfn->codeCount; ++i) {
            if (cfn->mapping[i] == frame->ipc) {
                index = i;
                break;
            }
        }

        if (index < 0) {
            printf("Could not find instruction mapping in compiled function%s\n", fn->name);
            printf("  PC: %d\n", frame->ipc);
            printf("  Mapping: {");
            for (int i = 0; i < cfn->codeCount; ++i) {
                if (i > 0) printf(", ");
                printf("%d: %d", cfn->mapping[i], i);
            }
            printf("}\n");
        }

        // returns -1 if it can't find it
        int line = sourceMapFunctionFindLine(cfn->sourceMap, index);

        if (line >= 0) {
            // TODO: get file from function
            char *file = sourceMapFile(cfn->module->sourceMap);
            char *fname = file ? sourceMapFunctionName(cfn->sourceMap) : 0;

            if (file && fname) {
                printf("%s  %s:%d\n", fname, file, line);
            } else {
                printf("%s:%d\n", fn->name, line);
            }
        } else {
            printf("%s Instruction %d\n", fn->name, index);
        }
    } else {
        printf("%s\n", fn->name);
    }
}

char* nuggetValueString (nuggetMachine *machine, void *value, nuggetType *type) {
    char *data = value;

    int size = 1;
    if (type->name) {
        size += strlen(type->name);
    } else {
        size += 5;
    }

    size += 2*type->size + 1;
    char *text = malloc(size);

    if (type->name) {
        strcpy(text, type->name);
    } else {
        sprintf(text, "#%d", type->id);
    }

    char *p = text + strlen(text);
    *(p++) = ' ';
    for (int i = 0; i < type->size; i++) {
        sprintf(p, "%02X", data[i]);
        p += 2;
    }
    *p = 0;
    return text;
}


int nuggetEnableLog (nuggetMachine *machine, int flag) {
    if (flag) {
        MonitorSetFlag(&machine->monitor, MONITOR_LOG);
    } else {
        MonitorUnsetFlag(&machine->monitor, MONITOR_LOG);
    }
}
int nuggetEnableProfiling (nuggetMachine *machine, int flag) {
    MonitorSetFlagValue(&machine->monitor, MONITOR_PROFILE, flag);
}

void _fmtInt (char **buf, int n) {
    if (!n) return;

    int d = n % 10;
    _fmtInt(buf, n / 10);
    **buf = '0' + d;
    *buf += 1;
}

void fmtInt (char *buf, int n) {
    if (n < 0) {
        *(buf++) = '-';
        n = -n;
    }
    if (n) _fmtInt(&buf, n);
    else *(buf++) = '0';
    *buf = 0;
}

void _writeS (int fd, char* msg) {
    write(fd, msg, strlen(msg));
}

void _writeN (int fd, char* buf, int n) {
    fmtInt(buf, n);
    _writeS(fd, buf);
}

extern void nuggetReport (nuggetMachine *machine, int fd) {
    // This function must be safe to use in an interrupt handler,
    // i.e. don't use malloc and check every pointer access (TODO)

    char intbuf[16];

    _writeS(fd, "Nugget Frames:\n");

    stackFrame *frame = machine->frame;
    while (frame) {
        nuggetFunction *fn = frame->fn;

        _writeS(fd, "  F");
        _writeN(fd, intbuf, frame->id);
        if (fn && fn->name) {
            _writeS(fd, " ");
            _writeS(fd, fn->name);
        }
        _writeS(fd, " ipc: ");
        _writeN(fd, intbuf, frame->ipc);

        // Try find source line
        if (fn && fn->kind == nuggetFunctionCompiled) {
            compiledFunction *cfn = (compiledFunction*) fn;

            int index = -1;
            // TODO: Binary search
            for (int i = 0; i < cfn->codeCount; ++i) {
                if (cfn->mapping[i] == frame->ipc) {
                    index = i;
                    break;
                }
            }

            if (index >= 0) {
                // returns -1 if it can't find it
                int line = sourceMapFunctionFindLine(cfn->sourceMap, index);

                if (line >= 0) {
                    // TODO: get file from function
                    char *file = sourceMapFile(cfn->module->sourceMap);
                    char *fname = file ? sourceMapFunctionName(cfn->sourceMap) : 0;

                    if (file && fname) {
                        _writeS(fd, "\n    ");
                        _writeS(fd, file);
                        _writeS(fd, " : line ");
                        _writeN(fd, intbuf, line);
                    }
                } else {
                    _writeS(fd, "inst: ");
                    _writeN(fd, intbuf, index);
                }
            }
        }
        _writeS(fd, "\n");
        frame = frame->parent;
    }
}

void printFunctionProfile (void *fptr, void *acc) {
    FILE *file = acc;

    nuggetFunction *f = fptr;

    if (f->callCount < 1) return;

    // All in ms
    float time = (float)f->time / CLOCKS_PER_SEC * 1000.0;
    float selfTime = (float)(f->time - f->innerTime) / CLOCKS_PER_SEC * 1000.0;

    fprintf(file, "%s,%d,%.2f,%.2f\n", f->name, f->callCount, time, selfTime);
}
extern void nuggetPrintProfileReport (nuggetMachine *machine, char *path) {
    FILE* file = fopen(path, "w");

    fprintf(file, "name,calls,time (ms),self (ms)\n");
    ArenaReduce(&machine->functionArena, printFunctionProfile, file);
}

int nuggetMonitorNamespace (nuggetMachine *machine, char *prefix) {
    MonitorAddPrefix(&machine->monitor, prefix);
}