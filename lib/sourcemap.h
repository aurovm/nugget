
nuggetMeta *sourceMapFindRoot (nuggetMeta *root);
nuggetMeta *sourceMapFindFunction (nuggetMeta *root, int index);
nuggetMeta *sourceMapFindType (nuggetMeta *root, int index);
char *sourceMapFile (nuggetMeta *root);

char *sourceMapItemName (nuggetMeta *root);
char *sourceMapFunctionName (nuggetMeta *root);
int sourceMapFunctionFindLine (nuggetMeta *root, int index);