#include <stdlib.h>
#include <string.h>

#include "hashmap.h"

#define BITS 9

typedef struct HashmapNode HashmapNode;

struct HashmapNode {
    char *key;
    void *value;
    HashmapNode *next;
};

struct Hashmap {
    HashmapHash hash;
    HashmapEqual eq;
    HashmapNode *heads[1 << BITS];
};

int HashmapStringHash (char *str) {
    int len = strlen(str);
    int hash = 0;
    for (int i = 0; i < len; ++i) {
        hash =(hash * 31) + str[i];
    }
    return hash;
}

int HashmapStringEq (char *a, char *b) {
    return strcmp(a, b) == 0;
}

extern Hashmap *HashmapNew (HashmapHash hash, HashmapEqual eq) {
    Hashmap *map = calloc(sizeof(Hashmap), 1);
    map->hash = hash;
    map->eq = eq;
    return map;
}

extern Hashmap *HashmapStringNew () {
    return HashmapNew((HashmapHash) HashmapStringHash, (HashmapEqual) HashmapStringEq);
}

extern void **HashmapGetPtr (Hashmap *map, void* key) {
    int hash = map->hash(key) & ((1 << BITS) - 1);

    HashmapNode **nodeptr = &map->heads[hash];
    while (1) {
        if (*nodeptr == 0) {
            *nodeptr = calloc(sizeof(HashmapNode), 1);
            (*nodeptr)->key = key;
            return &(*nodeptr)->value;
        }

        if (map->eq((*nodeptr)->key, key)) {
            return &(*nodeptr)->value;
        }

        nodeptr = &(*nodeptr)->next;
    }
}