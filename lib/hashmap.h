#ifndef __HASHMAP_H__
#define __HASHMAP_H__

typedef int (*HashmapHash)(void* a);
typedef int (*HashmapEqual)(void* a, void* b);

typedef struct Hashmap Hashmap;
extern Hashmap *HashmapStringNew ();
extern Hashmap *HashmapNew (HashmapHash hash, HashmapEqual eq);
extern void **HashmapGetPtr (Hashmap *map, void* key);

#endif