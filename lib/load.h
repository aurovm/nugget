
#define STACK_SIZE 64

typedef struct itemEntry {
    char* name;
    nuggetDefineItemDesc desc;
    nuggetItem item;
} itemEntry;

typedef struct contextInfo {
    int activeCount;
    int active[STACK_SIZE];
    int itemCount;
    int *items;
    int *recursions;
    int totalRecursion;
} contextInfo;

// TODO: Rename to Context
typedef struct loadedModule {
    // The first field so that it can be casted to this type
    nuggetModule _mod;

    nuggetMachine* machine;
    nuggetModuleDesc* desc;
    nuggetMeta *sourceMap;

    loadedModule* parent;

    // List with the context of all the items: modules first, then types, then functions.
    contextInfo *contextInfo;


    int contextIndex;
    nuggetModule *argument;

    int moduleCount;
    nuggetModule **modules;

    int functionCount;
    nuggetFunction **functions;

    int typeCount;
    nuggetType **types;

    int itemCount;
    itemEntry *items;
} loadedModule;