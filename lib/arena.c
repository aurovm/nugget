
#include <stdlib.h>

#include "arena.h"

DEFINE_ARRAY(ArenaEntryList, void*)

void ArenaInit (Arena *arena) {
	arena->entries = ArenaEntryListNew(128);
}

void *ArenaAlloc (Arena *arena, size_t size) {
	void *ptr = calloc(size, 1);
	ArenaEntryListPush(arena->entries, ptr);
	return ptr;
}

void ArenaReduce (Arena *arena, void (*fn)(void *value, void *acc), void *acc) {
	for (int i = 0; i < ArenaEntryListCount(arena->entries); ++i) {
		void *entry = ArenaEntryListGet(arena->entries, i);
		fn(entry, acc);
	}
}
