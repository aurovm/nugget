#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>

#include "headers.h"
#include "sourcemap.h"
#include "debug.h"
#include "monitor.h"

#define LINE_SIZE 512

#define RESET "\x1b[0m"
#define TITLE "\x1b[1m"

typedef struct {
    enum {
        OBJECT_KIND_MODULE,
        OBJECT_KIND_TYPE,
        OBJECT_KIND_FUNCTION,
        OBJECT_KIND_FRAME,
    } kind;
} Object;

typedef struct Shell {
    nuggetMachine *machine;
    int exit;
    int persist;
} *Shell;


Shell currentShell = 0;
void (*prevSigInt) ();

void shellSignalHandler (int signo) {
    if (signo == SIGINT) {
        printf("INTERRUPT\n");
        prevSigInt();
    } else {
        printf("SIGNAL HANDLER %d\n", signo);
    }
}


void printHelp () {
    printf(
        "h, help\tprint help\n"
        "exit\texit the Nugget Debugging Shell\n"
        "\n"
        "bt\tprint the backtrace\n"
        "n\tgo to next (nugget) instruction\n"
        "c\tcontinue execution, returning to the shell on breakpoints or errors\n"
    );
}

void printBacktrace (Shell shell) {
    stackFrame *frame = shell->machine->frame;
    while (frame) {
        printFrameLocation(frame);
        frame = frame->parent;
    }
}

void skipWhitespace (char **cmd) {
    while (1) {
        char c = **cmd;
        if (c == ' ' || c == '\t') *cmd++;
        else return;
    }
}

int check (char **_cmd, const char *pattern) {
    char *cmd = *_cmd;
    while (*pattern != 0) {
        if (*cmd != *pattern) return 0;
        cmd++; pattern++;
    }
    if (*cmd == ' ' || *cmd == '\n' || *cmd == '\0') {
        // Skip word and return
        *_cmd = cmd;
        skipWhitespace(cmd);
        return 1;
    }
    return 0;
}

void readline(Shell shell) {
    char line[LINE_SIZE];

    printf("> ");

    // This will also read the newline
    if (!fgets(line, LINE_SIZE, stdin)) {
        printf("ERROR READING LINE\n");
        shell->exit = 1;
    }

    char *cmd = line;
    skipWhitespace(&cmd);

    #define CASE(PAT) if (check(&cmd, PAT))

    if (!strcmp(line, "\n")) {
        // ignore empty line
    } else CASE("exit") {
        shell->exit = 1;
    } else CASE("help") {
        printHelp();
    } else CASE("bt") {
        printBacktrace(shell);
    } else CASE("next") {
        //shell->machine->monitor->shell = shell;
        //shell->exit = 1;
    } else if (!strcmp(line, "log on\n")) {
        MonitorSetFlag(&shell->machine->monitor, MONITOR_LOG);
    } else if (!strcmp(line, "log off\n")) {
        MonitorUnsetFlag(&shell->machine->monitor, MONITOR_LOG);
    } else {
        // newline is already in line
        printf("Unknown command: %s", line);
    }
}

void startShell (nuggetMachine *machine) {
    printf( "\n== " TITLE "Nugget Debugging Shell" RESET " ==\n");

    Shell shell = malloc(sizeof(struct Shell));
    shell->machine = machine;
    shell->exit = 0;
    shell->persist = 0;

    printf("  ");
    printFrameLocation(machine->frame);

    // Install signal handlers
    // prevSigInt = signal(SIGINT, shellSignalHandler);

    while (!shell->exit) {
        readline(shell);
    }

    if (!shell->persist) {
        free(shell);
    }

    // signal(SIGINT, prevSigInt);
}

void nuggetStartShell (nuggetMachine *machine) { startShell(machine); }
