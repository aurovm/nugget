
#ifndef __NUGGET__HEADERS__
#define __NUGGET__HEADERS__

#include "../nugget.h"
#include "hashmap.h"
#include "monitor.h"
#include "type.h"
#include "arena.h"

#define STRUCT(S) typedef struct S S
#define INT_PER_PTR (sizeof(void*) / sizeof(int))
// How to do this in a constant?
// if (INT_PER_PTR < 1) INT_PER_PTR = 1;

#define RECAST(VAR, TYPE) void* __recast_tmp_##VAR = VAR; TYPE *VAR = __recast_tmp_##VAR;
#define TODO(...) { printf("TODO: " __VA_ARGS__); abort(); }


void setErr (const char* err, ...);
int nextTypeId ();

#define EXACT_MATCH 1
#define INEXACT_MATCH 2

extern int nameMatch (char *query, char *name);

STRUCT(Inst);
STRUCT(stackFrame);
STRUCT(ErrorFrame);
STRUCT(loadedModule);

STRUCT(compiledFunction);
STRUCT(BuiltinFunction);
STRUCT(Constant);
STRUCT(CustomFunction);

STRUCT(BuiltinTypeDictionary);


struct stackFrame {
    stackFrame *parent;
    nuggetMachine *machine;
    nuggetFunction *fn;

    int id;
    int startClock;
    int innerElapsed;

    // 1 means an error ocurred, 0 means normal state
    int error;

    char *resultAddr;
    // Indicates this is the first frame in the stack, the machine should stop execution after popping it
    // otherwise, the machine will keep executing the instructions in the parent frame
    int head;

    // TODO: Should be only pc. It should be read to the stack, used locally, and then updated in the frame
    int ipc;
    int pc;
    int size;

    int *program;
    nuggetType **types;

    int *dataset;
    char data[];
};

typedef enum machineState {
    machineStateStopped,
    machineStateRunning,
    machineStatePaused,

    machineStateError=-1,
} machineState;

struct ErrorFrame {
    ErrorFrame *prev;
    char *msg;
    char *loadLoc;
    stackFrame *stackFrame;

    enum { ERROR_FRAME_EXEC, ERROR_FRAME_LOAD, ERROR_FRAME_LOAD_LOC } kind;
};

struct nuggetMachine {
    stackFrame* frame;

    char state;

    nuggetModuleLoader loader;
    Hashmap *moduleMap;

    ErrorFrame *err;
    Monitor monitor;

    Arena functionArena;

    char *error;

    nuggetFlag flags;

    // user data
    void *data;
};

struct nuggetFunction {
    enum {
        nuggetFunctionCompiled,
        nuggetFunctionExternal,
        nuggetFunctionConst,
        nuggetFunctionBuiltin,
        nuggetFunctionCustom,
        nuggetFunctionApply,
    } kind;

    enum {
        NUGGET_FUNCTION_MONITOR=1,
    } flags;

    int callCount;  // Number of times this function has been called
    int time;       // Time spent in this function
    int innerTime;  // Time spent in functions called by this function

    int inCount;
    int outCount;
    nuggetType **ins;
    nuggetType **outs;
    char *name;
};

struct nuggetModule {
    char *name;
    nuggetMachine *machine;
    nuggetItem (*getItem) (nuggetModule*, char*);
    nuggetModule *(*build) (nuggetModule* base, nuggetModule* argument);
};


typedef enum Instruction {
    InstCall,
    InstMov,
    InstEnd,

    InstJmp,
    InstJif,
    InstNif,

    // ops
} Instruction;

struct compiledFunction {
    nuggetFunction _fn;

    loadedModule *module;

    int frameSize;
    int codeCount; // instruction count of the original code
    int programSize; // size (in integers) of program
    int *program;
    nuggetType **types; // Types in the stack frame

    // the original instruction at index i is located at program[mapping[i]]
    int *mapping;
    nuggetMeta *sourceMap;
};

struct BuiltinFunction {
    nuggetFunction _fn;
    void (*impl)(stackFrame*, void*);
    void *data;
};

struct Constant {
    nuggetFunction _fn;

    nuggetFunction *base;

    // This is just so that _fn.outs doesn't need allocation
    nuggetType *type;

    // The size of this array is known from base->inCount
    nuggetFunction **args;

    int executed;
    char result[];
};

struct CustomFunction {
    nuggetFunction _fn;
    void (*impl)(nuggetFunctionArgs*, void*);
    void *data;

    int inCapacity;
    int outCapacity;
};

struct nuggetFunctionArgs {
    nuggetFunction *fn;
    int insize;
    char data[];
};

// nugget.c

extern void pushErr (nuggetMachine *machine, const char* format, ...);

// error.c

extern void pushLoadError (nuggetMachine *machine, char *msg, char *loc);

// builtins.c

#define EMPTY_ITEM ((nuggetItem) { nuggetItemEmpty, 0 })
#define ERROR_ITEM ((nuggetItem) { nuggetItemError, 0 })

struct nuggetBuffer {
    int size;
    char data[];
};

extern nuggetType buffer_t;
extern nuggetType string_t;
extern nuggetType int_t;
extern nuggetType flt_t;
extern nuggetType bool_t;
extern nuggetType any_t;

extern nuggetType module_t;
extern nuggetType item_t;
extern nuggetType code_t;
extern nuggetType type_t;
extern nuggetType pointer_t;

nuggetFunction *BuiltinFunctionNew (void (*impl)(stackFrame*, void*), char *name, void* data, int inCount, int outCount, ...);

typedef struct {
    nuggetType *type;
    nuggetModule *module;
} InstanceDictEntry;

typedef struct {
    int count;
    int capacity;
    InstanceDictEntry *entries;
} InstanceDict;

InstanceDict InstanceDictNew ();
nuggetModule *InstanceDictGet (InstanceDict *dict, nuggetType *type);
void InstanceDictSet (InstanceDict *dict, nuggetType *type, nuggetModule *instance);

typedef struct {
    nuggetModule _mod;
    InstanceDict dict;
} DictModule;

// array.c
extern nuggetModule *newArrayModule (nuggetMachine *machine);

// aurolib/null.c
extern nuggetModule *newNullModule (nuggetMachine *machine);
int nullTypeEquals (nuggetType *a, nuggetType *b);

// shell.c
extern nuggetModule *newShellModule (nuggetMachine *machine);

// aurolib/record.c
extern nuggetModule *newRecordModule (nuggetMachine *machine);
int recordTypeEquals (nuggetType *a, nuggetType *b);

// any.c
extern nuggetModule *newAnyModule (nuggetMachine *machine);

// reflect.c
extern nuggetModule *newModuleModule (nuggetMachine *machine);
extern nuggetModule *newItemModule (nuggetMachine *machine);
extern nuggetModule *newCodeModule (nuggetMachine *machine);
extern nuggetModule *newTypeModule (nuggetMachine *machine);
extern nuggetModule *newFunctionModule (nuggetMachine *machine);
extern nuggetModule *newDebugModule (nuggetMachine *machine);

// aurolib/machine.c
extern nuggetModule *newMemModule (nuggetMachine *machine);
extern nuggetModule *newPrimModule (nuggetMachine *machine);


#endif