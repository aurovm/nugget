
extern void *gc_alloc (nuggetType *type, int size);
extern int gc_isValid (void *gcvalue);
extern void gc_incref (void *gcvalue);
extern void gc_decref (void *gcvalue);

extern int gc_totalAllocated ();