    
#define DEFINE_METHOD(RET, NAME, ...) \
    typedef RET (*nuggetTypeMethod##NAME) (nuggetMachine *machine, void *valptr, void *data)

DEFINE_METHOD(void, Drop);
DEFINE_METHOD(void, Clone);

#undef DEFINE_METHOD

typedef int (*nuggetTypeFamilyMethodEq) (nuggetType *a, nuggetType *b, void *userData);

typedef struct {
    nuggetTypeFamilyMethodEq eq;
    void *userData;
} nuggetTypeFamily;

// Declared in nugget.h
struct nuggetType {
    int id;
    int size;
    enum {
        NUGGET_TYPE_FLAG_UNNAMED = 1,
        NUGGET_TYPE_FLAG_SIZED = 2,
        NUGGET_TYPE_FLAG_POINTER = 4,
        NUGGET_TYPE_FLAG_GC = 8,
        NUGGET_TYPE_FLAG_COPY = 16,
        NUGGET_TYPE_FLAG_RECORD = 32,
        NUGGET_TYPE_FLAG_NULL = 64,
    } flags;
    char *name;
    void *data;
    nuggetTypeMethodDrop drop;
    nuggetTypeMethodClone clone;

    // The family allows equality to be implemented among different
    // instanced types in the family.
    nuggetTypeFamily *family;
};

// Declared in nugget.h
// nuggetTypeEq