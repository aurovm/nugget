
LIB_SRC = lib/*.c lib/aurolib/*.c
BIN_SRC = bin/*.c

build: nugget libnugget.so
.PHONY: install uninstall

# --std=c99: Use the C99 version of the C language
# -g: debugging symbols, allows to see function locations in stack traces
# -O3: turn on optimizations

# TODO: Compiling with -O3 makes nugget crash

libnugget.so: nugget.h lib/*.h $(LIB_SRC)
	# -fPIC: ?
	gcc --std=c99 -g -shared -fPIC -o $@ $(LIB_SRC)
# 	gcc --std=c99 -O3 -g -shared -fPIC -o $@ $(LIB_SRC)

nugget: libnugget.so $(BIN_SRC)
	# -L./: Makes libnugget.so (in this directory) visible to the compiler
	# -lm -lffi -lnugget: Use the c-math, ffi and nugget libraries
	gcc --std=c99 -g -L./ -lm -lffi -lnugget -o $@ $(BIN_SRC)
# 	gcc --std=c99 -O3 -g -L./ -lm -lffi -lnugget -o $@ $(BIN_SRC)

install: nugget libnugget.so
	install -m 755 nugget /usr/bin
	install -m 755 libnugget.so /usr/lib

uninstall:
	rm /usr/bin/nugget
	rm /usr/lib/libnugget.so
