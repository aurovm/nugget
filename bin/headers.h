#include "../nugget.h"

typedef struct Loader Loader;

typedef struct {
    Loader *loader;
    int argc;
    char **argv;
} Context;

nuggetModule *createFfiModule (nuggetMachine *machine);
nuggetModule *createFfiFnModule (nuggetMachine *machine);
nuggetModule *createSysModule (nuggetMachine *machine);
nuggetModule *createIoModule (nuggetMachine *machine);

Loader *LoaderNew ();
void LoaderPushDir (Loader *this, char* dir);
char* LoaderGetFile (Loader *this, char* modname);