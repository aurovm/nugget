#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <setjmp.h>
#include <unistd.h>

#include "headers.h"

jmp_buf point;
void *previousSignal;
nuggetMachine *mainMachine = 0;

void signalHandler(int signo) {
    longjmp(point, 1);
}

nuggetModule *moduleLoader (nuggetMachine *machine, char* name) {
    Context *ctx = nuggetMachineGetData(machine);

    if (strcmp(name, "auro\x1fsystem") == 0) {
        return createSysModule(machine);
    }

    if (strcmp(name, "auro\x1fio") == 0) {
        return createIoModule(machine);
    }

    if (strcmp(name, "auro\x1f" "ffi") == 0) {
        return createFfiModule(machine);
    }

    if (strcmp(name, "auro\x1f" "ffi\x1f" "function") == 0) {
        return createFfiFnModule(machine);
    }

    nuggetModule *builtin = nuggetModuleBuiltin(machine, name);
    if (builtin != 0) { return builtin; }

    char *path = LoaderGetFile(ctx->loader, name);

    if (path == 0) {
        printf("ERROR: Module '%s' not found in directories.\n", name);
        return 0;
    }

    nuggetModuleDesc* desc = nuggetReadFile(path);
    if (desc == 0) {
        printf("An error ocurred while loading auro file %s:\n%s\n", path, nuggetError());
        return 0;
    }

    char *modname = malloc(strlen(name) + 1);
    strcpy(modname, name);
    for (int i = 0; i < strlen(modname); ++i) {
        if (modname[i] == '\x1f') {
            modname[i] = '.';
        }
    }
    nuggetModule *mod = nuggetModuleDescLoad(desc, machine, modname);

    return mod;
}

void printHelp (char* cmd) {
    printf("Usage: %s [options] <module> [arguments]\n", cmd);
    printf("\t--dir <path>\tAdds a search path for auro modules\n");
    printf("\t--log\t\tLogs every instruction and function call to stdout\n");
    printf("\t--profile <filename>\n\t\tMeasures time of execution for all function,\n\t\tand writes the csv report in <filename>\n");
}

void *prevInt = 0;
void handleInt (int signo) {
    // Restore after first interrupt
    signal(SIGINT, prevInt);

    if (mainMachine) {
        nuggetStartShell(mainMachine);
    } else if (prevInt) {
        ((void (*)(int signo)) prevInt)(signo);
    }
}

void *prevSegv = 0;
void handleSegv (int signo) {
    // Restore after first interrupt
    signal(SIGSEGV, prevSegv);

    if (mainMachine) {
        nuggetReport(mainMachine, STDERR_FILENO);
    }

    ((void (*)(int signo)) prevSegv)(signo);
}

int main (int argc, char **argv) {

    // Setup signal handlers

    prevInt = signal(SIGINT, &handleInt);
    prevSegv = signal(SIGSEGV, &handleSegv);

    /*
    previousSignal = signal(SIGSEGV, &signalHandler);
    if (setjmp(point) != 0) {
        signal(SIGSEGV, previousSignal);
        // Handle a segmentation fault
        if (mainMachine) {
            nuggetPushError(mainMachine, "SEGMENTATION FAULT");
            nuggetPrintError(mainMachine);
        } else {
            printf("UNKNOWN SEGMENTATION FAULT\n");
        }
        abort();
    }
    */

    Context *ctx = malloc(sizeof(Context));

    Loader *loader = LoaderNew();
    ctx->loader = loader;
    ctx->argc = 0;

    char *mainModName = 0;
    #define FLAG_LOG 1
    #define FLAG_PROFILE 2
    int flags = 0;
    char *profileFile = 0;

    int monitorCount = 0;
    char *monitorNamespaces[8];

    for (int i = 1; i < argc; ++i) {
        if (strcmp(argv[i], "--dir") == 0) {
            if (i+1 == argc) {
                printf("ERROR: Incomplete --dir argument.\n");
                printHelp(argv[0]);
                return 1;
            }
            LoaderPushDir(loader, argv[++i]);
        } else if (strcmp(argv[i], "--log") == 0) {
            flags = flags | FLAG_LOG;
        } else if (strcmp(argv[i], "--profile") == 0) {
            flags = flags | FLAG_PROFILE;
            profileFile = argv[++i];
        } else if (strcmp(argv[i], "--monitor-namespace") == 0) {
            monitorNamespaces[monitorCount++] = argv[++i];
        } else {
            mainModName = argv[i];
            ctx->argc = argc - i;
            ctx->argv = argv + i;
            break;
        }
    }

    LoaderPushDir(loader, ".");

    char *HOME = getenv("HOME");
    if (HOME != 0) {
        char *_modpath = "/.auro/modules";
        char *modpath = malloc(strlen(HOME) + strlen(_modpath) + 1);
        strcpy(modpath, HOME);
        strcat(modpath, _modpath);
        LoaderPushDir(loader, modpath);
    }

    if (mainModName == 0) {
        printf("ERROR: No main module specified.\n", mainModName);
        printHelp(argv[0]);
        return 1;
    }

    nuggetMachine *machine = nuggetMachineNew(moduleLoader, ctx);
    mainMachine = machine;

    if (flags & FLAG_LOG) {
        nuggetEnableLog(machine, 1);
    }
    if (flags & FLAG_PROFILE) {
        nuggetEnableProfiling(machine, 1);
    }
    for (int i = 0; i < monitorCount; ++i) {
        nuggetMonitorNamespace(machine, monitorNamespaces[i]);
    }

    nuggetModule *mainmod = nuggetMachineGetModule(machine, mainModName);
    if (mainmod == 0) {
        printf("An error ocurred while getting main module:\n%s\n", nuggetError());
        nuggetMachinePrintErrors(machine);
        return 1;
    }

    nuggetItem mainitem = nuggetModuleGetItem(mainmod, "main");

    if (mainitem.kind == nuggetItemError) {
        nuggetPrintError(machine);
        return 1;
    }

    if (mainitem.kind == nuggetItemEmpty) {
        printf("Module has no item 'main'.\n");
        return 1;
    }

    if (mainitem.kind != nuggetItemFunction) {
        printf("Module item 'main' is not a function.\n");
        return 1;
    }

    nuggetFunction *mainfn = mainitem.item;

    nuggetFunctionArgs *args = nuggetFunctionArgsNew(mainfn);
    nuggetFunctionCall(machine, mainfn, args);

    //int space = nuggetAllocatedMemory(machine);
    //printf("Allocated: %d bytes\n", space);

    if (flags & FLAG_PROFILE) {
        nuggetPrintProfileReport(machine, profileFile);
    }

    if (nuggetHasError(machine)) {
        nuggetPrintError(machine);
        return 1;
    }

    return 0;
}