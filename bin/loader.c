
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <unistd.h>

#include "headers.h"


struct Loader {
    int dirCount;
    char **dirs;
    int dirCapacity;
};

Loader *LoaderNew () {
    Loader *this = malloc(sizeof(Loader));
    this->dirCount = 0;
    this->dirCapacity = 64;
    this->dirs = malloc(sizeof(char*) * this->dirCapacity);
    return this;
}

void LoaderPushDir (Loader *this, char* dir) {
    if (this->dirCount >= this->dirCapacity) {
        this->dirCapacity += 64;
        this->dirs = realloc(this->dirs, sizeof(char*) * this->dirCapacity);
    }
    this->dirs[this->dirCount++] = dir;
}

char* LoaderGetFile (Loader *this, char* modname) {
    int modlen = strlen(modname);
    char *localpath = malloc(modlen+2);
    strcpy(localpath+1, modname);
    localpath[0] = '/';

    for (char *ch = localpath; *ch != 0; ch++) {
        if (*ch == '\x1f') {
            *ch = '.';
        }
    }

    for (int i = 0; i < this->dirCount; i++) {
        char *dir = this->dirs[i];
        char *fullpath = malloc(strlen(dir) + modlen + 2);

        strcpy(fullpath, dir);
        strcat(fullpath, localpath);

        if (access(fullpath, F_OK) == 0) {
            free(localpath);
            return fullpath;
        }

        free(fullpath);
    }

    free(localpath);
    return 0;
}
