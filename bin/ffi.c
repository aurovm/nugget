#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <dlfcn.h> 
#include <ffi.h>

#include "headers.h"

#define TYPE_COUNT 11

enum primitives {
    prim_u8,
    prim_u16,
    prim_u32,
    prim_u64,
    prim_i8,
    prim_i16,
    prim_i32,
    prim_i64,
    prim_ptr,
    prim_f32,
    prim_f64,
};


typedef struct {
    nuggetMachine *machine;
    char *name;
    void *lib;

    nuggetType *types[TYPE_COUNT];
} Data;

typedef struct {
    Data *data;
    char *name;
    void *handle;

    int inCount;
    int outCount;

    nuggetType *outType;
    nuggetType *inTypes[31];

    ffi_type *ffi_ret;
    ffi_type *ffi_args[31];
} FData;

ffi_type *get_ffi_type (nuggetType **types, nuggetType *tp) {

    ffi_type *ffi_types[TYPE_COUNT] = {
        &ffi_type_uint8,
        &ffi_type_uint16,
        &ffi_type_uint32,
        &ffi_type_uint64,
        &ffi_type_sint8,
        &ffi_type_sint16,
        &ffi_type_sint32,
        &ffi_type_sint64,
        &ffi_type_pointer,
        &ffi_type_float,
        &ffi_type_double,
    };

    for (int i = 0; i < TYPE_COUNT; ++i) {
        if (tp == types[i]) {
            return ffi_types[i];
        }
    }

    return 0;
}

int get_type_size (nuggetType **types, nuggetType *tp) {
    // TODO: use sizeof the base types
    const int sizes[TYPE_COUNT] = {1, 2, 4, 8, 1, 2, 4, 8, 8, 4, 8};

    for (int i = 0; i < TYPE_COUNT; ++i) {
        if (tp == types[i]) {
            return sizes[i];
        }
    }

    return 0;
}

void fnCall (nuggetFunctionArgs *args, void *_data) {
    FData *fdata = _data;
    ffi_cif cif;

    // Initialize the cif
    if (ffi_prep_cif(&cif, FFI_DEFAULT_ABI, fdata->inCount, fdata->ffi_ret, fdata->ffi_args) == FFI_OK) {

        // Calculate positions of each argument in the buffer
        // The first value in the buffer is the result
        int size = get_type_size(fdata->data->types, fdata->outType);
        int offsets[TYPE_COUNT];
        for (int i = 0; i < fdata->inCount; ++i) {
            int isize = get_type_size(fdata->data->types, fdata->inTypes[i]);
            // Align offset with the size of the argument:
            //   smallest multiple of isize equal or greater than size
            int pos = ((size + isize - 1) / isize) * isize;
            offsets[i] = pos;
            size = pos + isize;
        }

        // Allocate and populate the argument buffer
        char *buffer = malloc(size);
        void *values[TYPE_COUNT];
        for (int i = 0; i < fdata->inCount; ++i) {
            void *p = buffer + offsets[i];
            nuggetFunctionArgsReadInput(args, i, p);
            values[i] = p;
        }

        ffi_call(&cif, fdata->handle, buffer, values);

        if (fdata->outType) {
            nuggetFunctionArgsWriteOutput(args, 0, buffer);
        }

        free(buffer);
    } else {
        printf("ERROR: Could not prepare ffi function '%s'\n", fdata->name);
        exit(1);
        // TODO: Error handling
    }
}

nuggetModule *getBuilder (nuggetMachine *_machine, nuggetModule *argument, void *_data) {
    Data *data = _data;
    nuggetMachine *machine = data->machine;


    nuggetItem nameitem = nuggetModuleGetItem(argument, "name");

    if (nameitem.kind == nuggetItemError) {
        printf("An error ocurred while getting item 'name':\n%s\n", nuggetError());
        //nuggetMachinePrintErrors(machine);
        return 0;
    }

    if (nameitem.kind == nuggetItemEmpty) {
        printf("Argument for auro.ffi.import has no item 'name'.\n");
        return 0;
    }

    if (nameitem.kind != nuggetItemFunction) {
        printf("Argument 'name' for ffi import is not a function.\n");
        return 0;
    }



    nuggetFunction *namefn = nameitem.item;

    nuggetFunctionArgs *args = nuggetFunctionArgsNew(namefn);
    nuggetFunctionCall(machine, namefn, args);

    char *fname; nuggetFunctionArgsReadOutput(args, 0, &fname);

    void *handle = dlsym(data->lib, fname);
    if (handle == 0) {
        printf("No symbol '%s' in ffi library %s", fname, data->name);
        return 0;
    }



    FData *fdata = malloc(sizeof(FData));
    fdata->data = data;
    fdata->name = fname;
    fdata->handle = handle;
    fdata->outType = 0;



    int inCount = 0;
    while (1) {
        char iname[6];
        sprintf(iname, "in%d", inCount);

        nuggetItem titem = nuggetModuleGetItem(argument, iname);
        if (titem.kind != nuggetItemType) break;

        nuggetType *ntp = titem.item;

        ffi_type *ftp = get_ffi_type(data->types, ntp);
        if (!ftp) {
            printf("ffi.get argument '%s' is not a type usable for ffi\n", iname);
            return 0;
        }

        fdata->ffi_args[inCount] = ftp;
        fdata->inTypes[inCount++] = ntp;
    }
    fdata->inCount = inCount;


    nuggetItem outitem = nuggetModuleGetItem(argument, "out");
    if (outitem.kind == nuggetItemType) {
        nuggetType *ntp = outitem.item;

        ffi_type *ftp = get_ffi_type(data->types, ntp);
        if (!ftp) {
            printf("ffi.get argument 'out' is not a type usable for ffi\n");
            return 0;
        }

        fdata->outType = ntp;
        fdata->ffi_ret = ftp;
    } else {
        fdata->ffi_ret = &ffi_type_void;
    }


    nuggetModule *mod = nuggetModuleCreate("ffi.import.get", data);

    nuggetFunction *aufn = nuggetFunctionCreate(fnCall, fname, fdata);
    for (int i = 0; i < fdata->inCount; ++i) nuggetFunctionAddInput(aufn, fdata->inTypes[i]);
    if (fdata->outType) nuggetFunctionAddOutput(aufn, fdata->outType);

    nuggetModuleAddFunction(mod, "", aufn);

    return mod;
}

void setupTypes (nuggetMachine *machine, nuggetType **types) {
    nuggetModule *primmod = nuggetMachineGetModule(machine, "auro\x1fmachine\x1fprimitive");
    nuggetModule *memmod = nuggetMachineGetModule(machine, "auro\x1fmachine\x1fmemory");

    // matched the primitive enum
    char *names[8] = {"u8","u16","u32","u64","i8","i16","i32","i64"};

    for (int i = 0; i < 8; i++) {
        nuggetItem item = nuggetModuleGetItem(primmod, names[i]);
        types[i] = item.item;
    }

    types[prim_f32] = nuggetModuleGetItem(primmod, "f32").item;
    types[prim_f64] = nuggetModuleGetItem(primmod, "f64").item;

    types[prim_ptr] = nuggetModuleGetItem(memmod, "pointer").item;
}

nuggetModule *importBuilder (nuggetMachine *_machine, nuggetModule *argument, void *_data) {
    nuggetMachine *machine = _data;



    nuggetItem nameitem = nuggetModuleGetItem(argument, "0");

    if (nameitem.kind == nuggetItemError) {
        printf("An error ocurred while getting item '0':\n%s\n", nuggetError());
        //nuggetMachinePrintErrors(machine);
        return 0;
    }

    if (nameitem.kind == nuggetItemEmpty) {
        printf("Argument for auro.ffi.import has no item '0'.\n");
        return 0;
    }

    if (nameitem.kind != nuggetItemFunction) {
        printf("Argument '0' for ffi import is not a function.\n");
        return 0;
    }

    nuggetFunction *namefn = nameitem.item;

    nuggetFunctionArgs *args = nuggetFunctionArgsNew(namefn);
    nuggetFunctionCall(machine, namefn, args);

    char *libname; nuggetFunctionArgsReadOutput(args, 0, &libname);



    int len = strlen(libname);
    int prefix = memcmp(libname, "lib", 3) == 0;
    int local = memcmp(libname, "./", 2) == 0;
    int absolute = memcmp(libname, "/", 1) == 0;
    int ext = strcmp(libname + len - 3, ".so") == 0;

    if (!prefix && !(local || absolute) || !ext) {
        char *orig = libname;
        int newlen = len + 1;

        int addprefix = !prefix && !local && !absolute;

        if (!ext) newlen += 3;
        if (addprefix) newlen += 3;

        libname = malloc(newlen);
        libname[0] = 0;

        if (addprefix) strcat(libname, "lib");
        strcat(libname, orig);
        if (!ext) strcat(libname, ".so");
    }



    void *handle = dlopen(libname, RTLD_LAZY);
    if (handle == 0) {
        printf("Could not load library: %s\n", libname);
        return 0;
    }

    Data *data = malloc(sizeof(Data));
    data->machine = machine;
    data->name = libname;
    data->lib = handle;

    setupTypes(machine, data->types);

    char *modname = malloc(18 + strlen(libname));
    sprintf(modname, "auro.ffi.import(%s)", libname);

    nuggetModule *mod = nuggetModuleCreate(modname, data);

    nuggetModule *getMod = nuggetModuleCreateBuild("auro.ffi.import.get", getBuilder, data);
    nuggetModuleAddItem(mod, "get", (nuggetItem){ .kind = nuggetItemModule, .item = getMod });

    return mod;
}

nuggetModule *createFfiModule (nuggetMachine *machine) {
    nuggetModule *mod = nuggetModuleCreate("auro.ffi", 0);

    nuggetModule *importMod = nuggetModuleCreateBuild("auro.ffi.import", importBuilder, machine);
    nuggetModuleAddItem(mod, "import", (nuggetItem){ .kind = nuggetItemModule, .item = importMod });

    return mod;
}

typedef struct {
    nuggetMachine *machine;
    nuggetType *types[TYPE_COUNT];
} FBData;

typedef struct {
    // ffi related info
    ffi_cif cif;
    ffi_closure *closure;
    void *bound_f;

    // nugget related info
    FBData *data;
    nuggetFunction *fn;
    int inCount;

    ffi_type *ffi_ret;
    ffi_type *ffi_args[TYPE_COUNT];

    nuggetType *outType;
    nuggetType *inTypes[TYPE_COUNT];
} FBIData;

/* Acts like puts with the file given at time of enclosure. */
void ffi_binding(ffi_cif *cif, void *ffi_ret, void* ffi_args[], void *user_data) {
    FBIData *fdata = (FBIData*) user_data;

    nuggetFunctionArgs *args = nuggetFunctionArgsNew(fdata->fn);

    for (int i = 0; i < fdata->inCount; ++i) {
        nuggetFunctionArgsWriteInput(args, i, ffi_args[i]);
    }

    nuggetMachine *machine = fdata->data->machine;
    nuggetFunctionCall(machine, fdata->fn, args);

    if (fdata->outType) {
        nuggetFunctionArgsReadOutput(args, 0, ffi_ret);
    }

    nuggetFunctionArgsFree(args);
}

void ffi_get_fn_pointer (nuggetFunctionArgs *args, void *_data) {
    FBIData *fdata = (FBIData*) _data;
    void *p = fdata->bound_f;
    nuggetFunctionArgsWriteOutput(args, 0, &p);
}

nuggetModule *fnBuilder (nuggetMachine *_machine, nuggetModule *argument, void *_data) {
    FBData *data = _data;
    nuggetMachine *machine = data->machine;


    nuggetItem baseitem = nuggetModuleGetItem(argument, "function");
    if (baseitem.kind != nuggetItemFunction) {
        printf("Argument 'function' for auro.ffi.function is not a function\n");
        return 0;
    }


    FBIData *fdata = malloc(sizeof(FBIData));
    fdata->data = data;
    fdata->fn = baseitem.item;


    int inCount = 0;
    while (1) {
        char iname[6];
        sprintf(iname, "in%d", inCount);

        nuggetItem titem = nuggetModuleGetItem(argument, iname);
        if (titem.kind != nuggetItemType) break;

        nuggetType *ntp = titem.item;

        ffi_type *ftp = get_ffi_type(data->types, ntp);
        if (!ftp) {
            printf("ffi.get argument '%s' is not a type usable for ffi\n", iname);
            return 0;
        }

        fdata->ffi_args[inCount] = ftp;
        fdata->inTypes[inCount++] = ntp;
    }
    fdata->inCount = inCount;


    nuggetItem outitem = nuggetModuleGetItem(argument, "out");
    if (outitem.kind == nuggetItemType) {
        nuggetType *ntp = outitem.item;

        ffi_type *ftp = get_ffi_type(data->types, ntp);
        if (!ftp) {
            printf("ffi.get argument 'out' is not a type usable for ffi\n");
            return 0;
        }

        fdata->outType = ntp;
        fdata->ffi_ret = ftp;
    } else {
        fdata->ffi_ret = &ffi_type_void;
    }




    // Allocate closure and bound_f
    void *_before = fdata->bound_f;
    fdata->closure = ffi_closure_alloc(sizeof(ffi_closure), &fdata->bound_f);

    if (!fdata->closure) {
        printf("ERROR: ffi_closure_alloc failed\n");
        return 0;
    }

    int result;

    // Initialize the cif
    result = ffi_prep_cif(&fdata->cif, FFI_DEFAULT_ABI, fdata->inCount, fdata->ffi_ret, fdata->ffi_args);
    if (result != FFI_OK) {
        printf("ERROR: ffi_prep_cif failed: %d\n", result);
        return 0;
    }

    // Initialize the closure, passing fdata as the USER_DATA
    result = ffi_prep_closure_loc(fdata->closure, &fdata->cif, ffi_binding, fdata, fdata->bound_f);
    if (result != FFI_OK) {
        printf("ERROR: ffi_prep_closure_loc failed: %d\n", result);
        return 0;        
    }

    // TODO: Deallocate both closure, and bound_f
    // ffi_closure_free(fdata->closure);


    nuggetModule *mod = nuggetModuleCreate("auro.ffi.function", data);

    nuggetFunction *aufn = nuggetFunctionCreate(ffi_get_fn_pointer, "auro.ffi.function (get pointer)", fdata);
    nuggetFunctionAddOutput(aufn, data->types[prim_ptr]);
    nuggetModuleAddFunction(mod, "", aufn);

    return mod;
}

nuggetModule *createFfiFnModule (nuggetMachine *machine) {
    FBData *data = malloc(sizeof(FBData));
    data->machine = machine;
    setupTypes(machine, data->types);

    nuggetModule *mod = nuggetModuleCreateBuild("auro.ffi.function", fnBuilder, data);

    return mod;
}
