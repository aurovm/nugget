// Enables popen and pclose
#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "headers.h"

void _println (nuggetFunctionArgs *args, void *data) {
    char *msg;
    nuggetFunctionArgsReadInput(args, 0, &msg);
    printf("%s\n", msg);
}

void _argc (nuggetFunctionArgs *args, void *data) {
    nuggetFunctionArgsWriteOutput(args, 0, &((Context*)data)->argc);
}

void _argv (nuggetFunctionArgs *args, void *data) {
    int ix;
    nuggetFunctionArgsReadInput(args, 0, &ix);
    nuggetString *value = nuggetStringAlloc(((Context*)data)->argv[ix]);
    nuggetFunctionArgsWriteOutput(args, 0, &value);
}

void _exit (nuggetFunctionArgs *args, void *data) {
    int code;
    nuggetFunctionArgsReadInput(args, 0, &code);
    exit(code);
}

void _exec (nuggetFunctionArgs *args, void *data) {
    char *cmd;
    nuggetFunctionArgsReadInput(args, 0, &cmd);

    FILE *fp = popen(cmd, "r");

    int capacity = 512;
    int redd = 0;
    char *output = malloc(capacity);

    while (!feof(fp)) {
        if (redd >= capacity) {
            capacity = capacity + capacity / 2;
            output = realloc(output, capacity);
        }
        redd += fread(output+redd, 1, capacity - redd, fp);
    }

    output = realloc(output, redd+1);
    output[redd] = 0;

    nuggetString *outstr = nuggetStringAlloc(output);
    free(output);

    int code = WEXITSTATUS(pclose(fp));

    nuggetFunctionArgsWriteOutput(args, 0, &code);
    nuggetFunctionArgsWriteOutput(args, 1, &outstr);
}

void _error (nuggetFunctionArgs *args, void *data) {
    char *msg;
    nuggetFunctionArgsReadInput(args, 0, &msg);
    nuggetMachine *machine = (nuggetMachine*) data;
    nuggetPushError(machine, msg);
}


void io_r (nuggetFunctionArgs *args, void *data) {
    char *mode = "r";
    nuggetFunctionArgsWriteOutput(args, 0, &mode);
}

void io_w (nuggetFunctionArgs *args, void *data) {
    char *mode = "w";
    nuggetFunctionArgsWriteOutput(args, 0, &mode);
}

void io_a (nuggetFunctionArgs *args, void *data) {
    char *mode = "a";
    nuggetFunctionArgsWriteOutput(args, 0, &mode);
}

void io_open (nuggetFunctionArgs *args, void *data) {
    char *path; nuggetFunctionArgsReadInput(args, 0, &path);
    char *mode; nuggetFunctionArgsReadInput(args, 1, &mode);

    FILE *file = fopen(path, mode);
    if (!file) { printf("Could not open %s", path); exit(1); }
    
    nuggetFunctionArgsWriteOutput(args, 0, &file);
}

void io_close (nuggetFunctionArgs *args, void *data) {
    FILE *file; nuggetFunctionArgsReadInput(args, 0, &file);
    fclose(file);
}

void io_read (nuggetFunctionArgs *args, void *data) {
    FILE *file; nuggetFunctionArgsReadInput(args, 0, &file);
    int count; nuggetFunctionArgsReadInput(args, 1, &count);

    char *bytes = malloc(count);
    int redd = fread(bytes, 1, count, file);

    nuggetBuffer *buf = nuggetBufferAlloc(redd);
    memcpy(nuggetBufferGetData(buf), bytes, redd);
    
    nuggetFunctionArgsWriteOutput(args, 0, &buf);
}

void io_write (nuggetFunctionArgs *args, void *data) {
    FILE *file; nuggetFunctionArgsReadInput(args, 0, &file);
    nuggetBuffer *buf; nuggetFunctionArgsReadInput(args, 1, &buf);

    int size = nuggetBufferGetSize(buf);
    int written = fwrite(nuggetBufferGetData(buf), 1, size, file);

    if (written != size) { printf("Could not write file"); exit(1); }
}

void io_eof (nuggetFunctionArgs *args, void *data) {
    FILE *file; nuggetFunctionArgsReadInput(args, 0, &file);
    char eof = feof(file);
    nuggetFunctionArgsWriteOutput(args, 0, &eof);
}

nuggetModule *createSysModule (nuggetMachine *machine) {
    Context *ctx = nuggetMachineGetData(machine);

    nuggetModule *mod = nuggetModuleCreate("auro.system", 0);

    nuggetFunction *println = nuggetFunctionCreate(_println, "auro.system.println", 0);
    nuggetFunctionAddInput(println, nuggetTypeString());
    nuggetModuleAddFunction(mod, "println", println);

    nuggetFunction *argc = nuggetFunctionCreate(_argc, "auro.system.argc", ctx);
    nuggetFunctionAddOutput(argc, nuggetTypeInt());
    nuggetModuleAddFunction(mod, "argc", argc);

    nuggetFunction *argv = nuggetFunctionCreate(_argv, "auro.system.argv", ctx);
    nuggetFunctionAddInput(argv, nuggetTypeInt());
    nuggetFunctionAddOutput(argv, nuggetTypeString());
    nuggetModuleAddFunction(mod, "argv", argv);

    nuggetFunction *exit = nuggetFunctionCreate(_exit, "auro.system.exit", ctx);
    nuggetFunctionAddInput(exit, nuggetTypeInt());
    nuggetModuleAddFunction(mod, "exit", exit);

    nuggetFunction *exec = nuggetFunctionCreate(_exec, "auro.system.exec", ctx);
    nuggetFunctionAddInput(exec, nuggetTypeString());
    nuggetFunctionAddOutput(exec, nuggetTypeInt());
    nuggetFunctionAddOutput(exec, nuggetTypeString());
    nuggetModuleAddFunction(mod, "exec", exec);

    nuggetFunction *error = nuggetFunctionCreate(_error, "auro.system.error", machine);
    nuggetFunctionAddInput(error, nuggetTypeString());
    nuggetModuleAddFunction(mod, "error", error);

    return mod;
}

nuggetModule *createIoModule (nuggetMachine *machine) {
    Context *ctx = nuggetMachineGetData(machine);

    nuggetModule *mod = nuggetModuleCreate("auro.io", 0);

    nuggetType *file_t = nuggetTypeCreate("auro.io.file", sizeof(void*), 0);
    nuggetType *mode_t = nuggetTypeCreate("auro.io.mode", sizeof(void*), 0);

    nuggetModuleAddType(mod, "file", file_t);
    nuggetModuleAddType(mod, "mode", mode_t);

    nuggetFunction *r = nuggetFunctionCreate(io_r, "auro.system.r", ctx);
    nuggetFunctionAddOutput(r, mode_t);
    nuggetModuleAddFunction(mod, "r", r);

    nuggetFunction *w = nuggetFunctionCreate(io_w, "auro.system.w", ctx);
    nuggetFunctionAddOutput(w, mode_t);
    nuggetModuleAddFunction(mod, "w", w);

    nuggetFunction *a = nuggetFunctionCreate(io_a, "auro.system.a", ctx);
    nuggetFunctionAddOutput(a, mode_t);
    nuggetModuleAddFunction(mod, "a", a);

    nuggetFunction *open = nuggetFunctionCreate(io_open, "auro.system.open", ctx);
    nuggetFunctionAddInput(open, nuggetTypeString());
    nuggetFunctionAddInput(open, mode_t);
    nuggetFunctionAddOutput(open, file_t);
    nuggetModuleAddFunction(mod, "open", open);

    nuggetFunction *close = nuggetFunctionCreate(io_close, "auro.system.close", ctx);
    nuggetFunctionAddInput(close, file_t);
    nuggetModuleAddFunction(mod, "close", close);

    nuggetFunction *read = nuggetFunctionCreate(io_read, "auro.system.read", ctx);
    nuggetFunctionAddInput(read, file_t);
    nuggetFunctionAddInput(read, nuggetTypeInt());
    nuggetFunctionAddOutput(read, nuggetTypeBuffer());
    nuggetModuleAddFunction(mod, "read", read);

    nuggetFunction *write = nuggetFunctionCreate(io_write, "auro.system.write", ctx);
    nuggetFunctionAddInput(write, file_t);
    nuggetFunctionAddInput(write, nuggetTypeBuffer());
    nuggetModuleAddFunction(mod, "write", write);

    nuggetFunction *eof = nuggetFunctionCreate(io_eof, "auro.system.eof", ctx);
    nuggetFunctionAddInput(eof, file_t);
    nuggetFunctionAddOutput(eof, nuggetTypeBool());
    nuggetModuleAddFunction(mod, "eof", eof);

    return mod;
}
