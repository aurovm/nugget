# Auro Nugget

This is an implementation of the [Auro virtual machine specification](https://gitlab.com/aurovm/spec), written in C and designed with embedding in mind. It also replaces the original [Nim implementation](https://gitlab.com/aurovm/aurovm) as the reference implementation.

# TODO

- Garbage Collection

# Future

- Redesign api (simpler, maybe more lua-like)
- Reflection api (nugget internals)
- Extension api for JIT or type checkers
- Profiling
- Optimize internal instruction set