
#ifndef __NUGGET_H__
#define __NUGGET_H__

#ifdef __cplusplus
extern "C" {
#endif

#define NUGGET_STRUCT(S) typedef struct S S


// ==== Item type definitions ==== //

NUGGET_STRUCT(nuggetModuleDesc);

NUGGET_STRUCT(nuggetModuleItem);
NUGGET_STRUCT(nuggetTypeItem);
NUGGET_STRUCT(nuggetFunctionItem);
NUGGET_STRUCT(nuggetInstDesc);

NUGGET_STRUCT(nuggetMeta);

struct nuggetMeta {
    enum {
        nuggetMetaInt,
        nuggetMetaList,
        nuggetMetaString,
    } kind;

    union {
        int n;
        char *string;
        struct {
            int count;
            nuggetMeta* nodes;
        } list;
    } data;
};

struct nuggetModuleDesc {
    int moduleCount;
    int typeCount;
    int functionCount;

    nuggetModuleItem* modules;
    nuggetTypeItem* types;
    nuggetFunctionItem* functions;

    nuggetMeta meta;
};

struct nuggetTypeItem {
    int module;
    char* name;
};

typedef struct nuggetDefineItemDesc {
    enum {
        nuggetDefineModuleItem,
        nuggetDefineTypeItem,
        nuggetDefineFunctionItem,
    } kind;
    int index;
    char* name;
} nuggetDefineItemDesc;

struct nuggetModuleItem {
    enum {
        nuggetModuleItemEmpty,
        nuggetModuleItemImport,
        nuggetModuleItemDefine,
        nuggetModuleItemUse,
        nuggetModuleItemBuild,
        nuggetModuleItemContext,
    } kind;

    union {
        char* import;
        struct {
            int count;
            nuggetDefineItemDesc *items;
        } define;
        struct {
            int base;
            int argument;
        } build;
        struct {
            int base;
            char* name;
        } use;
    } data;
};

struct nuggetFunctionItem {
    enum {
        nuggetFunctionEmpty,
        nuggetFunctionCode,
        nuggetFunctionImport,

        nuggetFunctionInt=8,
        nuggetFunctionBin,
        nuggetFunctionConstCall=16,
    } kind;

    int inCount;
    int outCount;

    int* ins;
    int* outs;

    union {
        struct {
            int count;
            nuggetInstDesc* code;
        } code;
        struct {
            int module;
            char* name;
        } import;

        int num;
        struct {
            int size;
            char* bytes;
        } bin;
        struct {
            int fn;
            int paramCount;
            int* params;
        } call;
    } data;
};

struct nuggetInstDesc {
    enum {
        nuggetInstEnd,
        nuggetInstHlt,
        nuggetInstVar,
        nuggetInstDup,
        nuggetInstSet,
        nuggetInstJmp,
        nuggetInstJif,
        nuggetInstNif,
        nuggetInstCall=16,
    } kind;

    union {
        int args[2];
        struct {
            int fn;
            int argCount;
            int *args;
        } call;
    } data;
};




// ==== Machine functions ==== //

NUGGET_STRUCT(nuggetMachine);
NUGGET_STRUCT(nuggetModule);
NUGGET_STRUCT(nuggetType);
NUGGET_STRUCT(nuggetFunction);
NUGGET_STRUCT(nuggetItem);

NUGGET_STRUCT(nuggetBuffer);
NUGGET_STRUCT(nuggetString);

struct nuggetItem {
    enum {
        nuggetItemEmpty,
        nuggetItemModule,
        nuggetItemType,
        nuggetItemFunction,

        nuggetItemError = -1,
    } kind;
    void *item;
};

typedef nuggetModule* (*nuggetModuleLoader) (nuggetMachine *machine, char* name);

extern nuggetMachine *nuggetMachineNew (nuggetModuleLoader loader, void *data);
extern nuggetModule *nuggetMachineGetModule (nuggetMachine *machine, char *name);
extern void *nuggetMachineGetData (nuggetMachine *machine);
extern void nuggetMachinePrintErrors (nuggetMachine *machine);

typedef int nuggetFlag;
#define NUGGET_FLAG_SHELL_ON_ERROR 1

/*typedef struct {
    void (*) itemPreload (int kind, int index, void *data);
} nuggetModuleDescLoadOptions;*/

extern nuggetModule *nuggetModuleDescLoad (nuggetModuleDesc *module, nuggetMachine *machine, char *name);
extern nuggetModule *nuggetModuleBuiltin (nuggetMachine *machine, char *name);
extern nuggetItem nuggetModuleGetItem (nuggetModule* module, char *name);

extern nuggetFunction *nuggetCompiledModuleGetFunction (nuggetModule *module, int index);

extern nuggetType *nuggetTypeNew(char* name, int size);
extern int nuggetTypeGetId(nuggetType *tp);
extern char *nuggetTypeGetName(nuggetType *tp);
extern int nuggetTypeGetSize(nuggetType *tp);
extern int nuggetTypeEq(nuggetType *a, nuggetType *b);


// Builtin items

extern nuggetType *nuggetTypeInt();
extern nuggetType *nuggetTypeBool();
extern nuggetType *nuggetTypeFloat();
extern nuggetType *nuggetTypeString();
extern nuggetType *nuggetTypeBuffer();

extern nuggetBuffer *nuggetBufferAlloc(int size);
extern int nuggetBufferGetSize(nuggetBuffer *buf);
extern char* nuggetBufferGetData(nuggetBuffer *buf);
extern char nuggetBufferGet(nuggetBuffer *buf, int index);
extern void nuggetBufferSet(nuggetBuffer *buf, int index, char value);

// Allocates a nugget string from a c string
extern nuggetString *nuggetStringAlloc(char *init);



// Custom items

typedef nuggetItem (*nuggetModuleGetter) (nuggetMachine *machine, char *name, void *data);
typedef nuggetModule* (*nuggetModuleBuilder) (nuggetMachine *machine, nuggetModule *argument, void *data);


extern nuggetModule *nuggetModuleCreate (char *name, void *data);
extern void nuggetModuleAddItem (nuggetModule *mod, char *name, nuggetItem item);
extern void nuggetModuleAddType (nuggetModule *mod, char *name, nuggetType *tp);
extern void nuggetModuleAddFunction (nuggetModule *mod, char *name, nuggetFunction *fn);
// extern void nuggetModuleSetGetter (nuggetItem (*fn)(char *name, void *data));
// extern void nuggetModuleSetBuilder (nuggetModule* (*fn)(nuggetModule *argument, void *data));

extern nuggetModule *nuggetModuleCreateGet (char *name, nuggetModuleGetter cb, void *data);
extern nuggetModule *nuggetModuleCreateBuild (char *name, nuggetModuleBuilder cb, void *data);

NUGGET_STRUCT(nuggetFunctionArgs);

extern nuggetFunction *nuggetFunctionCreate (void (*fn)(nuggetFunctionArgs *args, void *data), char *name, void *data);
extern void nuggetFunctionAddInput (nuggetFunction *function, nuggetType *type);
extern void nuggetFunctionAddOutput (nuggetFunction *function, nuggetType *type);

extern char *nuggetFunctionGetName (nuggetFunction *function);

extern nuggetType *nuggetFunctionGetInput (nuggetFunction *function, int index);
extern int nuggetFunctionGetInputCount (nuggetFunction *function);

extern nuggetType *nuggetFunctionGetOutput (nuggetFunction *function, int index);
extern int nuggetFunctionGetOutputCount (nuggetFunction *function);

extern void nuggetFunctionCall (nuggetMachine *machine, nuggetFunction* fn, nuggetFunctionArgs *args);

// lib/args.c

extern nuggetFunctionArgs *nuggetFunctionArgsNew (nuggetFunction *fn);
extern void nuggetFunctionArgsFree (nuggetFunctionArgs *args);
extern void nuggetFunctionArgsReadInput (nuggetFunctionArgs *args, int i, void *retaddr);
extern void nuggetFunctionArgsWriteInput (nuggetFunctionArgs *args, int i, void *data);
extern void nuggetFunctionArgsReadOutput (nuggetFunctionArgs *args, int i, void *retaddr);
extern void nuggetFunctionArgsWriteOutput (nuggetFunctionArgs *args, int i, void *data);

extern nuggetType *nuggetTypeCreate (char *name, int size, void *data);


// Error Handling (error.c)
extern void nuggetPushError (nuggetMachine *machine, char* msg);
extern void nuggetPushErrorF (nuggetMachine *machine, int size, const char* format, ...);
extern void nuggetPrintError (nuggetMachine *machine);
extern int nuggetHasError (nuggetMachine *machine);


// Debugging

extern char *nuggetError();
extern void nuggetPrintModule (nuggetModuleDesc *module);
extern void nuggetStartShell (nuggetMachine *machine);

extern void nuggetReport (nuggetMachine *machine, int fd);
extern int nuggetAllocatedMemory (nuggetMachine *machine);

// Monitors any module whose namespace has the prefix
extern int nuggetMonitorNamespace (nuggetMachine *machine, char *prefix);

// 1 for logging/profiling. 0 for no logging/profiling
extern int nuggetEnableLog (nuggetMachine *machine, int v);
extern int nuggetEnableProfiling (nuggetMachine *machine, int v);

extern void nuggetPrintProfileReport (nuggetMachine *machine, char *filename);

extern char* nuggetValueString (nuggetMachine *machine, void *value, nuggetType *type);



// ==== Loading functions ==== //

extern nuggetModuleDesc *nuggetReadFile (char* path);

#undef NUGGET_STRUCT

#ifdef __cplusplus
}
#endif

#endif