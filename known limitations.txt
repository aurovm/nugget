
- Module item names can't be longer than 255 characters
- Module items can't have more than 32 segments in their name
- Because of context computation, there can't be more than 64 nested contexts, nor more than 64 simultaneously recursive items
- Recursive record and shell types return an incomplete type the second time they are requested. If any code actually executes before the type is fully described, the machine will crash
- Nugget segfaults when compiled with -O3